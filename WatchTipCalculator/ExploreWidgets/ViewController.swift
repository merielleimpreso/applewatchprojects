//
//  ViewController.swift
//  ExploreWidgets
//
//  Created by Lito Ang on 12/3/14.
//  Copyright (c) 2014 Aeus. All rights reserved.
//

import UIKit
import WatchConnectivity


class UIPageViewControllerWithOverlayIndicator: UIPageViewController {
    override func viewDidLayoutSubviews() {
        for subView in self.view.subviews {
            if subView is UIScrollView {
                subView.frame = self.view.bounds
            } else if subView is UIPageControl {
                subView.frame.origin = CGPointMake(0, self.view.frame.size.height-40)
                self.view.bringSubviewToFront(subView)
            }
        }
        super.viewDidLayoutSubviews()
    }
}

class ViewController: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate, WCSessionDelegate {
    
    
    @IBOutlet weak var viewLockedOverlay: UIView!
    @IBOutlet weak var bgGradient: UIImageView!
    
    @IBOutlet weak var numpad: UIView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    @IBOutlet weak var btnDecimal: UIButton!
    
    @IBOutlet weak var btnRestore: UIButton!
    
    private var pageViewController: UIPageViewController?
    private var currentPageController: TCPageContentController?
    private var index = 0
    private var billAmount:NSString = "0"
    private var tipPercent:NSString = "20"
    private var splitCount:NSString = "1"

    private var payAmount:Float = 0

    let formatNumber = NSNumberFormatter()
    var iapHelper:IAPHelper = IAPHelper()
    var separator:String = "."
    
    var session : WCSession!
    
    private let contentImages = ["tc_bg_bill",
        "tc_bg_tip",
        "tc_bg_split"]
    
    func log(message:String) {
//         print(message)
    }
    
    func sendContextToWatch(key:String, value:String) {
        do {
            let appData: [String: String] = [key : value]
//            appData[key] = value
            try WCSession.defaultSession().updateApplicationContext(appData)
            log("ios - app context sent")
        } catch {
            log("ios - error occurred while sending context to watch")
        }
    }
    
    func storeSharedInfo(key:String, value:String) {
        
        let appGroupID = "group.com.aeustech.watchtipcalculator"
        let defaults = NSUserDefaults(suiteName: appGroupID)
        
        defaults!.setObject(value, forKey: key)

        defaults!.synchronize()
        
        log("stored latest shared info")
        
    }
    
    
    func retrieveSharedInfo(key:String) -> String {
        
        let appGroupID = "group.com.aeustech.watchtipcalculator"
        let defaults = NSUserDefaults(suiteName: appGroupID)
        
        defaults!.synchronize()
        
        if let value = defaults!.objectForKey(key) as? String {
            return value
        } else {
            return ""
        }
        
    }
    
    func jumpToPage() {
        var pageIndex = currentPageController!.pageIndex < contentImages.count-1 ? currentPageController!.pageIndex+1 : currentPageController!.pageIndex

        index++
        if(index > contentImages.count-1) {
            index = 0
            pageIndex = 0
        }
        
        let unlocked = retrieveSharedInfo("split_unlocked")
        
        if index == 2 && unlocked != "yes" {
            viewLockedOverlay.hidden = false
            numpad.alpha = 0.2
        } else {
            viewLockedOverlay.hidden = true
            numpad.alpha = 1
        }

        log("pageIndex = \(pageIndex)")

        
        let nextController = getItemController(pageIndex)!
        currentPageController = nextController
        
        if pageIndex == 1 {
            bgGradient.image = UIImage(named:"bg_tip_gradient")

            let controller = currentPageController as! TCTipPageContentController
            let nextControllers: NSArray = [controller]
            pageViewController!.setViewControllers(nextControllers as? [UIViewController], direction: UIPageViewControllerNavigationDirection.Forward, animated: true, completion: nil)

            controller.btn_tip_pct.setTitle("0", forState: UIControlState.Normal)

            calcTip(tipPercent as String)
        } else {
            bgGradient.image = UIImage(named:"bg_split_gradient")

            let controller = currentPageController as! TCSplitPageContentController
            let nextControllers: NSArray = [controller]
            pageViewController!.setViewControllers(nextControllers as? [UIViewController], direction: UIPageViewControllerNavigationDirection.Forward, animated: true, completion: nil)

            controller.btn_split_num.setTitle("1", forState: UIControlState.Normal)

            calcSplit(splitCount as String)
        }

    }
    
    func childMessage(message:String) {
        log("child message = \(message)")
        
        if message == "jump_to_tip" {
            jumpToPage()
        } else if message == "jump_to_split" {
            jumpToPage()
        }
    }
    
    func calcTip(btnTitle:String) {
        let controller = currentPageController as! TCTipPageContentController
        
        var amountText:NSString = controller.btn_tip_pct.titleForState(UIControlState.Normal)!
        amountText = amountText == "0" ? "" : amountText
        amountText = (amountText as String) + "" + btnTitle
        
        if (amountText as NSString).length > 4 {
            return
        }
        
        controller.btn_tip_pct.setTitle(amountText as String, forState: UIControlState.Normal)
        
        let bill = (billAmount as NSString).floatValue
        let tipPct = (amountText as NSString).floatValue
        payAmount = bill + (bill * (tipPct/100))
        tipPercent = amountText
        
        controller.lbl_tip_amt.text = formatNumber.stringFromNumber(bill * (tipPct/100)) //"\(tipPct)"
        controller.lbl_pay_amt.text = formatNumber.stringFromNumber(payAmount) //"\(pay)"
    }
    
    func calcSplit(btnTitle:String) {
        let controller = currentPageController as! TCSplitPageContentController
        
        var amountText:NSString = controller.btn_split_num.titleForState(UIControlState.Normal)!
        amountText = amountText == "1" ? "" : amountText
        amountText = (amountText as String) + "" + btnTitle
        
        if (amountText as NSString).length > 4 || amountText == "0" {
            return
        }
        
        controller.btn_split_num.setTitle(amountText as String, forState: UIControlState.Normal)
        
        let splitNum = (amountText as NSString).floatValue
        let splitPay = payAmount / splitNum
        
        splitCount = amountText
        
        //                controller.lbl_tip_amt.text = formatNumber.stringFromNumber(bill * (tipPct/100)) //"\(tipPct)"
        controller.lbl_share_amount.setTitle(formatNumber.stringFromNumber(splitPay), forState: UIControlState.Normal)
    }
    
    func iapCallBack(message:String) {
        
        if message == "success" {
            viewLockedOverlay.hidden = true

            storeSharedInfo("split_unlocked", value: "yes")
            sendContextToWatch("split_unlocked", value: "yes")

            numpad.alpha = 1
            activityIndicator.stopAnimating()

        } else if message == "failure" {
            numpad.alpha = 0.2
            activityIndicator.stopAnimating()
        }
        
    }
    
    
    @IBAction func btnRestoreAction(sender: AnyObject) {
        log("restore")
        
        activityIndicator.hidden = false
        activityIndicator.startAnimating()
        iapHelper.restorePurchases()
    }
    
    
    @IBAction func btnUnlockAction(sender: AnyObject) {
        log("ios - unlock")
        
        activityIndicator.hidden = false
        activityIndicator.startAnimating()
        iapHelper.fetchProducts()
        
    }
    
    
    @IBAction func buttonPressed(sender: AnyObject) {
        let btnTitle = (sender as! UIButton).currentTitle!
//        log("button = \(btnTitle)")
        
        if btnTitle == "C" {
            
            if currentPageController!.pageIndex == 0 {
                let controller = currentPageController as! TCBillPageContentController
                controller.btn_bill_amt.setTitle("0", forState: UIControlState.Normal)
                
            } else if currentPageController!.pageIndex == 1 {
                let controller = currentPageController as! TCTipPageContentController
                controller.btn_tip_pct.setTitle("0", forState: UIControlState.Normal)
                calcTip("0")

                
            } else if currentPageController!.pageIndex == 2 {
                let controller = currentPageController as! TCSplitPageContentController
                controller.btn_split_num.setTitle("1", forState: UIControlState.Normal)
                calcSplit("1")
//                controller.lbl_share_amount.setTitle("0.00", forState: UIControlState.Normal)
            }
            
        } else if btnTitle == separator {
            
            if currentPageController!.pageIndex == 0 {
                let controller = currentPageController as! TCBillPageContentController
                var buttonText = controller.btn_bill_amt.titleForState(UIControlState.Normal)

                if !(buttonText!.rangeOfString(separator) != nil) {
                    buttonText = buttonText! + separator
                    controller.btn_bill_amt.setTitle(buttonText, forState: UIControlState.Normal)
                    billAmount = buttonText!
                }
                
                
            } else if currentPageController!.pageIndex == 1 {

                let controller = currentPageController as! TCTipPageContentController
                var buttonText = controller.btn_tip_pct.titleForState(UIControlState.Normal)
                if !(buttonText?.rangeOfString(separator) != nil) {
                    buttonText = buttonText! + separator
                    controller.btn_tip_pct.setTitle(buttonText, forState: UIControlState.Normal)
                    tipPercent = buttonText!
                }
                
            } else if currentPageController!.pageIndex == 2 {

                let controller = currentPageController as! TCSplitPageContentController
                var buttonText = controller.btn_split_num.titleForState(UIControlState.Normal)
                if !(buttonText?.rangeOfString(separator) != nil) {
                    buttonText = buttonText! + separator
                    controller.btn_split_num.setTitle(buttonText, forState: UIControlState.Normal)
                    splitCount = buttonText!
                }
            }
        
        } else {
            
            if currentPageController!.pageIndex == 0 {
                
                
                let controller = currentPageController as! TCBillPageContentController
                
                var amountText = controller.btn_bill_amt.titleForState(UIControlState.Normal)
                amountText = amountText == "0" ? "" : amountText
                amountText = amountText! + "" + btnTitle
                
                if (amountText! as NSString).length > 9 {
                    return
                }
                
                controller.btn_bill_amt.setTitle(amountText, forState: UIControlState.Normal)
                
                billAmount = amountText!
                
            } else if currentPageController!.pageIndex == 1 {
                calcTip(btnTitle)
                
                
            } else if currentPageController!.pageIndex == 2 {
                calcSplit(btnTitle)
                
            }
            

        }
        
        
//        var defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
//
//        var unlock = false
//        
//        if btnTitle == "Unlock" {
//            unlock = true
//        }
//        
//        defaults.setObject(unlock, forKey: "unlocked_split")
//
//        defaults.synchronize()
        
    }

    
    func pageViewController(pageViewController: UIPageViewController,
        didFinishAnimating finished: Bool,
        previousViewControllers _previousViewControllers: [UIViewController],
        transitionCompleted completed: Bool) {
            if completed {
                currentPageController = pageViewController.viewControllers?.first as? TCPageContentController
                log("transition to view controllers = \(currentPageController!.pageIndex)")
                
                index = currentPageController!.pageIndex
                
                let unlocked = retrieveSharedInfo("split_unlocked")
                
                if index == 2 && unlocked != "yes" {
                    viewLockedOverlay.hidden = false
                    numpad.alpha = 0.2

                } else {
                    viewLockedOverlay.hidden = true
                    numpad.alpha = 1

                }
                
                if currentPageController!.pageIndex == 0 {
                    bgGradient.image = UIImage(named:"bg_bill_gradient")

                    let controller = currentPageController as! TCBillPageContentController
                    controller.btn_bill_amt.setTitle(billAmount as String, forState: UIControlState.Normal)
                    
                } else if currentPageController!.pageIndex == 1 {
                    bgGradient.image = UIImage(named:"bg_tip_gradient")

                    let controller = currentPageController as! TCTipPageContentController
                    controller.btn_tip_pct.setTitle("0", forState: UIControlState.Normal)
                    calcTip(tipPercent as String)
                    
                } else if currentPageController!.pageIndex == 2 {
                    bgGradient.image = UIImage(named:"bg_split_gradient")

                    let controller = currentPageController as! TCSplitPageContentController
                    controller.btn_split_num.setTitle("1", forState: UIControlState.Normal)
                    splitCount = splitCount == "" ? "1" : splitCount
                    calcSplit(splitCount as String)
                }

            }
    }

    private func createPageViewController() {
        
        let pageController = self.storyboard!.instantiateViewControllerWithIdentifier("TCPageController") as! UIPageViewController
        pageController.dataSource = self
        pageController.delegate = self
        
        if contentImages.count > 0 {
            let firstController = getItemController(0)!
            let startingViewControllers: NSArray = [firstController]
            pageController.setViewControllers(startingViewControllers as? [UIViewController], direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil)
        }
        
        pageViewController = pageController
        addChildViewController(pageViewController!)
        pageViewController!.view.frame = CGRectMake(0, 0, pageViewController!.view.frame.width, 300)
        self.view.addSubview(pageViewController!.view)
        pageViewController!.didMoveToParentViewController(self)
        currentPageController = pageViewController!.viewControllers?.first as? TCPageContentController
    }
    
    private func setupPageControl() {
        let appearance = UIPageControl.appearance()
        appearance.pageIndicatorTintColor = UIColor.grayColor()
        appearance.currentPageIndicatorTintColor = UIColor.whiteColor()
        appearance.backgroundColor = UIColor.clearColor()
//        appearance.frame = CGRectMake(appearance.frame.origin.x, appearance.frame.origin.y-450, appearance.frame.size.width, appearance.frame.size.height)
        
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as! TCPageContentController
        
        if itemController.pageIndex > 0 {
            return getItemController(itemController.pageIndex-1)
        }
        
        return nil
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as! TCPageContentController
        
        if itemController.pageIndex+1 < contentImages.count {
            return getItemController(itemController.pageIndex+1)
        }
        
        return nil
    }
    
    private func getItemController(itemIndex: Int) -> TCPageContentController? {
        
//        if itemIndex < contentImages.count {
        if itemIndex == 0 {
            let pageItemController = self.storyboard!.instantiateViewControllerWithIdentifier("TCBillPageContentController") as! TCBillPageContentController
            pageItemController.pageIndex = itemIndex
            pageItemController.imageName = contentImages[itemIndex]
            pageItemController.mainController = self
//            pageItemController.btn_bill_amt.setTitle(billAmount as String, forState: UIControlState.Normal)
            
            return pageItemController
            
        } else if itemIndex == 1 {
            let pageItemController = self.storyboard!.instantiateViewControllerWithIdentifier("TCTipPageContentController") as! TCTipPageContentController
            pageItemController.pageIndex = itemIndex
            pageItemController.imageName = contentImages[itemIndex]
            pageItemController.mainController = self
            
//            pageItemController.btn_tip_pct.setTitle("0", forState: UIControlState.Normal)
//            calcTip(tipPercent as String)

            return pageItemController
            
        } else if itemIndex == 2 {
            let pageItemController = self.storyboard!.instantiateViewControllerWithIdentifier("TCSplitPageContentController") as! TCSplitPageContentController
            pageItemController.pageIndex = itemIndex
            pageItemController.imageName = contentImages[itemIndex]
            pageItemController.mainController = self
            
//            pageItemController.btn_split_num.setTitle("1", forState: UIControlState.Normal)
//            splitCount = splitCount == "" ? "1" : splitCount
//            calcSplit(splitCount as String)

            return pageItemController
            
        } else {
            let pageItemController = self.storyboard!.instantiateViewControllerWithIdentifier("TCPageContentController") as! TCPageContentController
            pageItemController.pageIndex = itemIndex
            pageItemController.imageName = contentImages[itemIndex]
            pageItemController.mainController = self

            return pageItemController
        }
        
//        return nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
//        var locale: AnyObject = NSLocale.preferredLanguages()[0]
//        log("phone locale = \(locale)")

        let locale: AnyObject = NSLocale.currentLocale()
        separator = locale.objectForKey(NSLocaleDecimalSeparator) as! String
        
//        println("watch locale decimal separator = " + separator)
        btnDecimal.setTitle(separator, forState: UIControlState.Normal)
        
        formatNumber.numberStyle = NSNumberFormatterStyle.CurrencyStyle
//        formatNumber.locale = NSLocale(localeIdentifier: "en_US")
//        btnBill.setTitle(formatNumber.stringFromNumber(0))
        
        createPageViewController()
        setupPageControl()
        
        iapHelper.initIAP(self)
        
        if (WCSession.isSupported()) {
            session = WCSession.defaultSession()
            session.delegate = self
            session.activateSession()
        }
        
        // TEST - uncomment for testing
//        storeSharedInfo("split_unlocked", value: "yes")
//        sendContextToWatch("split_unlocked", value: "yes")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Page Indicator
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return contentImages.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return index
    }


}

