//
//  ActiveViewController.swift
//  WearHydrateMe
//
//  Created by Merielle Impreso on 6/4/15.
//  Copyright (c) 2015 Merielle Impreso. All rights reserved.
//

import UIKit
import DataKit

class ActiveViewController: UIViewController {
    @IBOutlet weak var picker: UIDatePicker!
    var type: String = ""
    var parentController: AlarmViewController?
    var currentTime: NSDate?
    
    func saveChangeSettings() {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "HH:mm"
        let stringFromDate = formatter.stringFromDate(self.picker.date)
        let stringFromDateArr = stringFromDate.componentsSeparatedByString(":")
        let hour = Int(stringFromDateArr[0])
        let min = Int(stringFromDateArr[1])
        let miridiem = hour < 12 ? "AM" : "PM"
        
        if (self.type == "ActiveFrom") {
            DataAccess.sharedInstance.updateActiveFromHour(hour!)
            DataAccess.sharedInstance.updateActiveFromMinute(min!)
            DataAccess.sharedInstance.updateActiveFromMiridiem(miridiem)
        } else {
            DataAccess.sharedInstance.updateActiveToHour(hour!)
            DataAccess.sharedInstance.updateActiveToMinute(min!)
            DataAccess.sharedInstance.updateActiveToMiridiem(miridiem)
        }
        
        DataAccess.sharedInstance.updateAlarmOn(false)
        DataAccess.sharedInstance.cancelNotification()
        
        self.navigationController?.popToRootViewControllerAnimated(true)
    }

    @IBAction func saveButtonClicked(sender: AnyObject) {
        let alarm = DataAccess.sharedInstance.getAlarmOn()
        if (alarm) {
            		
        } else {
            self.saveChangeSettings()
        }
    }
    
    @IBAction func cancelButtonClicked(sender: AnyObject) {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        picker.setDate(currentTime!, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
