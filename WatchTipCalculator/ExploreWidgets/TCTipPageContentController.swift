//
//  TCTipPageContentController.swift
//  WatchTipCalculator
//
//  Created by dev on 4/21/15.
//  Copyright (c) 2015 Aeus. All rights reserved.
//

import UIKit

class TCTipPageContentController : TCPageContentController {
    @IBOutlet weak var btn_tip_pct: UIButton!
    @IBOutlet weak var lbl_tip_amt: UILabel!
    @IBOutlet weak var lbl_pay_amt: UILabel!
    
    let formatNumber = NSNumberFormatter()
    
    @IBAction func buttonAction(sender: AnyObject) {
        mainController.childMessage("jump_to_split")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}
