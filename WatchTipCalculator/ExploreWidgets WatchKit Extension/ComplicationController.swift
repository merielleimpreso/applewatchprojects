//
//  ComplicationController.swift
//  ProtoWatchOSTwo WatchKit Extension
//
//  Created by dev on 10/3/15.
//  Copyright © 2015 Aeus Tech, Inc. All rights reserved.
//

import ClockKit


class ComplicationController: NSObject, CLKComplicationDataSource {
    
    let formatNumber = NSNumberFormatter()

    // MARK: - Timeline Configuration
    
    func getSupportedTimeTravelDirectionsForComplication(complication: CLKComplication, withHandler handler: (CLKComplicationTimeTravelDirections) -> Void) {
        handler([.Forward, .Backward])
    }
    
    func getTimelineStartDateForComplication(complication: CLKComplication, withHandler handler: (NSDate?) -> Void) {
        handler(nil)
    }
    
    func getTimelineEndDateForComplication(complication: CLKComplication, withHandler handler: (NSDate?) -> Void) {
        handler(nil)
    }
    
    func getPrivacyBehaviorForComplication(complication: CLKComplication, withHandler handler: (CLKComplicationPrivacyBehavior) -> Void) {
        handler(.ShowOnLockScreen)
    }
    
    // MARK: - Timeline Population
    
    func getCurrentTimelineEntryForComplication(complication: CLKComplication, withHandler handler: ((CLKComplicationTimelineEntry?) -> Void)) {
        let appGroupID = "group.com.aeustech.watchtipcalculator"
        let defaults = NSUserDefaults(suiteName: appGroupID)
        
        var tipPercent = defaults!.stringForKey("tip_percent")
        tipPercent = tipPercent != nil || tipPercent != "" ? tipPercent : "0"
        
        var totalAmount = defaults!.stringForKey("total_amount")
        totalAmount = totalAmount != nil || totalAmount != "" ? totalAmount : "0"
        
        var billAmount = defaults!.stringForKey("bill_amount")
        billAmount = billAmount != nil || billAmount != "" ? billAmount : "0"
        
        var tipAmount = defaults!.stringForKey("tip_amount")
        tipAmount = tipAmount != nil || tipAmount != "" ? tipAmount : "0"
        
        formatNumber.numberStyle = NSNumberFormatterStyle.CurrencyStyle

        
        if complication.family == .CircularSmall {
            
//            print("complication tip: \(tipPercent)")

            let template = CLKComplicationTemplateCircularSmallRingText()
            template.textProvider = CLKSimpleTextProvider(text: tipPercent!)
            template.fillFraction = Float(tipPercent!)! / 50.0
            template.ringStyle = CLKComplicationRingStyle.Closed
            
//            let template = CLKComplicationTemplateCircularSmallStackText()
//            template.line1TextProvider = CLKSimpleTextProvider(text: "TIP")
//            template.line2TextProvider = CLKSimpleTextProvider(text: tipPercent! + "%")
            
            let timelineEntry = CLKComplicationTimelineEntry(date: NSDate(), complicationTemplate: template)
            handler(timelineEntry)
            
        } else if complication.family == .ModularSmall {
                
            //            print("complication tip: \(tipPercent)")
            
            let template = CLKComplicationTemplateModularSmallRingText()
            template.textProvider = CLKSimpleTextProvider(text: tipPercent!)
            template.fillFraction = Float(tipPercent!)! / 50.0
            template.ringStyle = CLKComplicationRingStyle.Closed
            
            //            let template = CLKComplicationTemplateCircularSmallStackText()
            //            template.line1TextProvider = CLKSimpleTextProvider(text: "TIP")
            //            template.line2TextProvider = CLKSimpleTextProvider(text: tipPercent! + "%")
            
            let timelineEntry = CLKComplicationTimelineEntry(date: NSDate(), complicationTemplate: template)
            handler(timelineEntry)
            
        } else if complication.family == .ModularLarge {
            
            let template = CLKComplicationTemplateModularLargeStandardBody()
            template.headerTextProvider = CLKSimpleTextProvider(text: "Total: " + formatNumber.stringFromNumber(Double(totalAmount!)!)!)
            template.body1TextProvider = CLKSimpleTextProvider(text: "Bill: " + formatNumber.stringFromNumber(Double(billAmount!)!)!)
            template.body2TextProvider = CLKSimpleTextProvider(text: "Tip: " + formatNumber.stringFromNumber(Double(tipAmount!)!)!)
            
            let timelineEntry = CLKComplicationTimelineEntry(date: NSDate(), complicationTemplate: template)
            handler(timelineEntry)
        
        } else if complication.family == .UtilitarianSmall {
            
            let formattedSplitAmount = formatNumber.stringFromNumber(Double(totalAmount!)!)!
            
            let template = CLKComplicationTemplateUtilitarianSmallFlat()
            template.textProvider = CLKSimpleTextProvider(text: formattedSplitAmount)
            
            let timelineEntry = CLKComplicationTimelineEntry(date: NSDate(), complicationTemplate: template)
            handler(timelineEntry)
            
        } else if complication.family == .UtilitarianLarge {
            
            let formattedSplitAmount = formatNumber.stringFromNumber(Double(totalAmount!)!)!
            
            let template = CLKComplicationTemplateUtilitarianLargeFlat()
            template.textProvider = CLKSimpleTextProvider(text: "LAST BILL: " + formattedSplitAmount)
            
            let timelineEntry = CLKComplicationTimelineEntry(date: NSDate(), complicationTemplate: template)
            handler(timelineEntry)
        
        } else {
            handler(nil)
        }
    }
    
    func getTimelineEntriesForComplication(complication: CLKComplication, beforeDate date: NSDate, limit: Int, withHandler handler: (([CLKComplicationTimelineEntry]?) -> Void)) {
        // Call the handler with the timeline entries prior to the given date
        handler(nil)
    }
    
    func getTimelineEntriesForComplication(complication: CLKComplication, afterDate date: NSDate, limit: Int, withHandler handler: (([CLKComplicationTimelineEntry]?) -> Void)) {
        // Call the handler with the timeline entries after to the given date
        handler(nil)
    }
    
    // MARK: - Update Scheduling
    
    func getNextRequestedUpdateDateWithHandler(handler: (NSDate?) -> Void) {
        // Call the handler with the date when you would next like to be given the opportunity to update your complication content
        // Update hourly
        handler(NSDate(timeIntervalSinceNow: 60*60))
    }
    
    // MARK: - Placeholder Templates
    
    func getPlaceholderTemplateForComplication(complication: CLKComplication, withHandler handler: (CLKComplicationTemplate?) -> Void) {
        // This method will be called once per supported complication, and the results will be cached
        
        var template: CLKComplicationTemplate? = nil
        switch complication.family {
            
        case .ModularSmall:
            let modularTemplate = CLKComplicationTemplateModularSmallRingText()
            modularTemplate.textProvider = CLKSimpleTextProvider(text: "15")
            modularTemplate.fillFraction = 15.0 / 50.0
            modularTemplate.ringStyle = CLKComplicationRingStyle.Closed
            
            template = modularTemplate
        
        case .ModularLarge:
            let modularTemplate = CLKComplicationTemplateModularLargeStandardBody()
            modularTemplate.headerTextProvider = CLKSimpleTextProvider(text: "Total: $0.00")
            modularTemplate.body1TextProvider = CLKSimpleTextProvider(text: "Bill: $0.00")
            modularTemplate.body2TextProvider = CLKSimpleTextProvider(text: "Tip: $0.00")
            
            template = modularTemplate
            
        case .UtilitarianSmall:
            let modularTemplate = CLKComplicationTemplateUtilitarianSmallFlat()
            modularTemplate.textProvider = CLKSimpleTextProvider(text: formatNumber.stringFromNumber(0.0)!)

            template = modularTemplate
            
        case .UtilitarianLarge:
            let modularTemplate = CLKComplicationTemplateUtilitarianLargeFlat()
            modularTemplate.textProvider = CLKSimpleTextProvider(text: formatNumber.stringFromNumber(0.0)!)
            
            template = modularTemplate
            
        case .CircularSmall:
            
            let modularTemplate = CLKComplicationTemplateCircularSmallRingText()
            modularTemplate.textProvider = CLKSimpleTextProvider(text: "15")
            modularTemplate.fillFraction = 15.0 / 50.0
            modularTemplate.ringStyle = CLKComplicationRingStyle.Closed
//
//            let modularTemplate = CLKComplicationTemplateCircularSmallStackText()
//            modularTemplate.line1TextProvider = CLKSimpleTextProvider(text: "TIP")
//            modularTemplate.line2TextProvider = CLKSimpleTextProvider(text: "15")
            
            template = modularTemplate
        }
        
        handler(template)
        
    }
    
}
