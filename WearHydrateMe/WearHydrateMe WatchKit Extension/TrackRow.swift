//
//  TrackRowController.swift
//  WearHydrateMe
//
//  Created by Merielle Impreso on 5/25/15.
//  Copyright (c) 2015 Merielle Impreso. All rights reserved.
//

import WatchKit
import Foundation
import DataKit

class TrackRow: NSObject {
    @IBOutlet weak var dateLabel: WKInterfaceLabel!
    @IBOutlet weak var trackLabel: WKInterfaceLabel!

    var records : [Record]?
    var goal : Goal?
    var date : NSDate?
}
