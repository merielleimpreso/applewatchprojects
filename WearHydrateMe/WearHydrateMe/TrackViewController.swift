//
//  TrackViewController.swift
//  WearHydrateMe
//
//  Created by Merielle Impreso on 5/27/15.
//  Copyright (c) 2015 Merielle Impreso. All rights reserved.
//

import UIKit
import DataKit

class TrackViewController: UITableViewController {
    var daysToBeDisplayed = 7
    
    var allRecords: [Record] = []
    var allGoals: [Goal] = []
    var allTracks: [Track] = []
    
    var iapHelper:IAPHelper = IAPHelper()
    var activityIndicator = UIActivityIndicatorView()
    var overlayView = UIView()
    
    @IBOutlet weak var unlockButton: UIBarButtonItem!
    
    @IBAction func showActionSheet(sender: AnyObject) {
        let optionMenu = UIAlertController(title: nil, message: "Show 30-Day Track", preferredStyle: .ActionSheet)
        
        let buyAction = UIAlertAction(title: "Buy", style: .Default, handler: {
            (alert: UIAlertAction) -> Void in
            print("Buy")
            
            self.overlayView.hidden = false
            self.activityIndicator.hidden = false
            self.activityIndicator.startAnimating()
            
            UIApplication.sharedApplication().networkActivityIndicatorVisible = true
            self.iapHelper.fetchProducts()
        })
        let restoreAction = UIAlertAction(title: "Restore", style: .Default, handler: {
            (alert: UIAlertAction) -> Void in
            
            print("Restore")
            
            self.overlayView.hidden = false
            self.activityIndicator.hidden = false
            self.activityIndicator.startAnimating()
            
            UIApplication.sharedApplication().networkActivityIndicatorVisible = true
            
            self.iapHelper.restorePurchases()
            
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction) -> Void in
            print("Cancelled")
        })
        
        optionMenu.addAction(buyAction)
        optionMenu.addAction(restoreAction)
        optionMenu.addAction(cancelAction)
        
        self.presentViewController(optionMenu, animated: true, completion: nil)
    }
    
    func iapCallBack(message:String) {
        
        if message == "success" {
            self.daysToBeDisplayed = 30
            DataAccess.sharedInstance.updateUnlock30Days(true)
            self.tableView.reloadData()
            
        } else if message == "failure" {
        
        }
        
        activityIndicator.stopAnimating()
        activityIndicator.hidden = true
        self.overlayView.hidden = true
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    }
    
    
    
    func getAllTracks() -> [Track] {
        var allTracks: [Track] = []
        for daysAgo in 0...daysToBeDisplayed-1 {
            let format = NSDateFormatter.dateFormatFromTemplate("MMM-dd", options: 0, locale: NSLocale(localeIdentifier: "en-US"))
            let formatter = NSDateFormatter()
            formatter.dateFormat = format
            
            let date = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: -daysAgo, toDate: NSDate(), options: [])!
            let records = getRecordsWithDate(date)
            let goal = getGoalWithDate(date)
            let dateString = formatter.stringFromDate(date)
            
            let track = Track(records: records, goal: goal, date: date)
            allTracks.append(track)
        }
        return allTracks
    }
    
    func getRecordsWithDate(specificDate: NSDate) -> [Record] {
        var records: [Record] = []
        let formatter = NSDateFormatter()
        formatter.dateStyle = .ShortStyle
        
        let selectedDate = formatter.stringFromDate(specificDate)
        for record in allRecords {
            let date = formatter.stringFromDate(record.timeStamp)
            if (date == selectedDate) {
                records.append(record)
            }
        }
        return records
    }
    
    func getGoalWithDate(specificDate: NSDate) -> Goal? {
        let formatter = NSDateFormatter()
        formatter.dateStyle = .ShortStyle
        
        let selectedDate = formatter.stringFromDate(specificDate)
        for g in allGoals {
            let date = formatter.stringFromDate(g.date)
            if (date == selectedDate) {
                return g
            }
        }
        return nil
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.iapHelper.initIAP(self)
        
        overlayView = UIView(frame: UIScreen.mainScreen().bounds)
        overlayView.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.4)
        overlayView.center = self.tableView.center
        overlayView.hidden = true
        
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
        activityIndicator.hidden = true
        activityIndicator.center = overlayView.center
        overlayView.addSubview(activityIndicator)
        self.tableView.addSubview(overlayView)
        
        let unlocked = DataAccess.sharedInstance.getUnlock30Days()
        if unlocked {
            self.daysToBeDisplayed = 30
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        allRecords = DataAccess.sharedInstance.getAllRecords()
        allGoals = DataAccess.sharedInstance.getAllGoals()
        allTracks = getAllTracks()
        
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allTracks.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let format = NSDateFormatter.dateFormatFromTemplate("MMM-dd", options: 0, locale: NSLocale(localeIdentifier: "en-US"))
        let formatter = NSDateFormatter()
        formatter.dateFormat = format
        
        
        let track = allTracks[indexPath.row]
        let records = getRecordsWithDate(track.date!)
        let goal = getGoalWithDate(track.date!)
        let dateString = formatter.stringFromDate(track.date!)

        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) 
        cell.textLabel?.text = dateString
        
        if (goal == nil) {
            cell.detailTextLabel?.text = "No Record"
            cell.accessoryType = UITableViewCellAccessoryType.None
            cell.detailTextLabel?.textColor = UIColor(red: 190/255, green: 190/255, blue: 190/255, alpha: 0.95)
            cell.userInteractionEnabled = false
        } else {
            cell.detailTextLabel?.text = "\(records.count)/\(goal!.number)"
            cell.detailTextLabel?.textColor = UIColor(red: 109/255, green: 208/255, blue: 247/255, alpha: 1)
            
            if records.count == 0 {
                cell.accessoryType = UITableViewCellAccessoryType.None
                cell.userInteractionEnabled = false
            } else {
                cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
                cell.userInteractionEnabled = true
            }
        }
        
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let vc = segue.destinationViewController as! TrackDetailViewController
        let index = tableView.indexPathForSelectedRow?.row
        let track = allTracks[index!]
        vc.track = track
        
        var r = track.records!.first!
        let format = NSDateFormatter.dateFormatFromTemplate("dd MMM yyyy, EEE", options: 0, locale: NSLocale(localeIdentifier: "en-US"))
        let formatter = NSDateFormatter()
        formatter.dateFormat = format
        let date = formatter.stringFromDate(r.timeStamp)
        vc.title = date
    }
}
