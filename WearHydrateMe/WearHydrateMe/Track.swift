//
//  Track.swift
//  WearHydrateMe
//
//  Created by Merielle Impreso on 5/26/15.
//  Copyright (c) 2015 Merielle Impreso. All rights reserved.
//

import Foundation
import DataKit

class Track {
    var records : [Record]?
    var goal : Goal?
    var date : NSDate?
    
    init(records: [Record]?, goal: Goal?, date: NSDate?) {
        self.records = records
        self.goal = goal
        self.date = date
    }
}