//
//  ViewController.swift
//  InactivityCoach
//
//  Created by Merielle Impreso on 7/15/15.
//  Copyright (c) 2015 Merielle Impreso. All rights reserved.
//

import UIKit
import CoreMotion

class MainViewController: UIViewController, SOMotionDetectorDelegate {
    @IBOutlet weak var activateButton: UIButton!
    @IBOutlet weak var coachImage: UIImageView!
    @IBOutlet weak var statsLabel: UILabel!
    @IBOutlet weak var progressTextImage: UIImageView!
    
    var pageViewController: UIPageViewController?
    let coachNames = InactivityCoach.sharedInstance.getCoachNames()
    var isMoving: Bool?
    
    let motionMgr = CMMotionManager()
    let pedometer = CMPedometer()
    let activityMgr = CMMotionActivityManager()
    
    var progressView: ProgressView!
    var dayProgressView: DayProgressView!
    
    
    var coachState: String = kCoachStateNormal {
        didSet {
            animateCoachImage()
            InactivityCoach.sharedInstance.setSharedInfo(kCoachState, value: coachState)
        }
    }
    
    var progress: Double = 0 {
        didSet {
            
            let sessionGoal = kPedometerAvailable ?
                goalStepsBetweenSessions :
            kGoalStepsBetweenNotifications
            
            if progress >= sessionGoal {
                if coachState == kCoachStateAngry {
                    coachState = kCoachStateHappy
                }
            } else {
                coachState = kCoachStateAngry
            }
            
            if let view = progressView {
                if progress <= sessionGoal {
                    view.progress = progress
                }
            }
            
            let progressPercent = self.progress / sessionGoal
            InactivityCoach.sharedInstance.setSharedInfo(kSessionProgress, value: progressPercent)
        }
    }
    
    var dailyProgress: Double = 0 {
        didSet {
            if let view = dayProgressView {
                let dailyGoal = kPedometerAvailable ? kGoalStepsPerDay : Record.getGoalStepsForToday()
                
                if dailyProgress <= dailyGoal {
                    view.progress = dailyProgress
                } else {
                    view.progress = dailyGoal
                }
            }
            var recordToday = Record.getRecordToday()
            recordToday.progress = dailyProgress
            recordToday = Record.updateRecordToday(recordToday)
            
            let dayProgressPercent = kPedometerAvailable ?
                dailyProgress / Double(kGoalStepsPerDay) :
                dailyProgress / Double(recordToday.goal!)
            
            InactivityCoach.sharedInstance.setSharedInfo(kDayProgress, value: dayProgressPercent)
            InactivityCoach.sharedInstance.setSharedInfo(kLastActiveTime, value: NSDate())
        }
    }
    
    var notificationOn: Bool? {
        didSet {
            if let button = activateButton {
                if notificationOn! {
                    button.setBackgroundImage(UIImage(named: "main_button_sleep.png"), forState: UIControlState.Normal)
                    coachState = kCoachStateNormal
                    progress = kGoalStepsBetweenNotifications
                } else {
                    button.setBackgroundImage(UIImage(named: "main_button_active.png"), forState: UIControlState.Normal)
                    coachState = kCoachStateSleep
                }
            }
        }
    }
    
    var goalStepsBetweenSessions: Double {
        get {
            // calculate seconds since last notification
            let intervalHour = InactivityCoach.sharedInstance.getSharedInfo(kIntervalHour) as! Int
            let intervalMinute = InactivityCoach.sharedInstance.getSharedInfo(kIntervalMinute) as! Int
            
            let intervalInHours = intervalHour + (intervalMinute/60)
            
            // goal steps depend on notification intervals (i.e. longer interval means more goal steps)
            return Double(intervalInHours) * kGoalStepsPerHour
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        notificationOn = InactivityCoach.sharedInstance.getSharedInfo(kNotificationOn) as? Bool
        addProgressViews()
        
        // update progress views whenever app becomes active
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "processSensorData", name: UIApplicationDidBecomeActiveNotification, object: nil)
    }
    
    func addProgressViews() {
        var add = -arcWidth*4
        var width = coachImage.frame.width + add
        var height = coachImage.frame.height + add
        progressView = ProgressView(frame: CGRectMake(-add/2, -add/2, width, height))
        self.coachImage.addSubview(progressView)
        progressView.animateCircle(1.0)
        
        add = -arcWidth*2
        width = coachImage.frame.width + add
        height = coachImage.frame.height + add
        dayProgressView = DayProgressView(frame: CGRectMake(-add/2, -add/2, width, height))
        self.coachImage.addSubview(dayProgressView)
        dayProgressView.animateCircle(1.0)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "startWorkOut", name: "OPEN_FROM_NOTIFICATION", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "showNotification", name: "NOTIFY_WHILE_APP_IS_RUNNING", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "setNotification", name: "WATCH_SET_NOTIFICATION_ON", object: nil)
        
        animateCoachImage()
        
        let recordToday = Record.getRecordToday()
        dailyProgress = recordToday.progress as! Double
        
        // TEST PROGRESS IN SIMULATOR
        //        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(5.0 * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) { () -> Void in
        //            print("after 10 seconds")
        //            self.progress = 150.0
        //            self.dailyProgress = 150.0
        //
        //            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(5.0 * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) { () -> Void in
        //                print("after 10 seconds")
        //                self.progress = 500.0
        //                self.dailyProgress = 500.0
        //
        //                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(5.0 * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) { () -> Void in
        //                    print("after 10 seconds")
        //                    self.progress = 10.0
        //                    self.dailyProgress = 10.0
        //                }
        //            }
        //        }
        
        processSensorData()
        
        // FOR TESTING ONLY
        //InactivityCoach.sharedInstance.setSharedInfo(kLastStepCount, value: 0)
        //coachState = kCoachStateAngry
        //processSensorData()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        SOStepDetector.sharedInstance().stopDetection()
    }
    
    func startWorkOut() {
        if progress >= kGoalStepsBetweenNotifications {
            progress = 0
        }
        coachState = kCoachStateAngry
    }
    
    func processSensorData() {
        // if device has M7 co-processor, use it
        if (CMPedometer.isStepCountingAvailable() == true) {
            //            coachState = kCoachStateAngry
            handlePedometer()
        }
            // else use SOMotionDetector for older devices
        else {
            SOMotionDetector.sharedInstance().startDetection()
            // TODO: stop step detection when active goal or daily goal is reached
            handleStepDetector()
        }
    }
    
    func handlePedometer() {
        
        var status = ""
        
        if (CMPedometer.isStepCountingAvailable() == true) {
            
            // calculate seconds since last notification
            let intervalHour = InactivityCoach.sharedInstance.getSharedInfo(kIntervalHour) as! Int
            let intervalMinute = InactivityCoach.sharedInstance.getSharedInfo(kIntervalMinute) as! Int
            
            let secondsSinceLastNotification = Double((intervalHour * 60 * 60) + (intervalMinute * 60)) * -1
            
            let dateStart = NSDate(timeInterval:secondsSinceLastNotification, sinceDate: NSDate())
            
            // update session progress, get step count for the past few hours determined by notification interval
            pedometer.startPedometerUpdatesFromDate(dateStart) { (pedometerData, error) -> Void in
                
                if pedometerData != nil {
                    let steps: UInt = pedometerData!.numberOfSteps.unsignedLongValue
                    
                    // calculate steps while app is inactive, if it exceeds goal, change coach to happy and inform user
                    
                    let lastStoredSteps:UInt = InactivityCoach.sharedInstance.getSharedInfo(kLastStepCount) == nil ?
                        0 :
                        InactivityCoach.sharedInstance.getSharedInfo(kLastStepCount) as! UInt
                    
                    let stepsSinceLastNotification = Double(steps - lastStoredSteps)
                    
                    if(stepsSinceLastNotification >= self.goalStepsBetweenSessions) {
                        // max steps between notifications exceeded, make coach happy and store steps
                        self.progress = kGoalStepsBetweenNotifications
                        self.animateProgressTextImage(true)
                        
                        InactivityCoach.sharedInstance.setSharedInfo(kLastStepCount, value: steps)
                        
                    } else {
                        // max steps not exceeded, let user complete the remaining steps
                        self.progress = (Double(stepsSinceLastNotification) / self.goalStepsBetweenSessions) * kGoalStepsBetweenNotifications
                        
                        // / Double(kGoalStepsBetweenNotifications) * kGoalStepsBetweenNotifications
                        self.animateProgressTextImage(true)
                        
                    }
                    
                    //                    status = "stepsSinceLastNotification = \(stepsSinceLastNotification)"
                    status = "SESSION: \(Int(stepsSinceLastNotification))/\(Int(self.goalStepsBetweenSessions)) steps"
                    
                    dispatch_async(dispatch_get_main_queue(),{
                        //                        self.statsLabel.text = status
                        print("status = " + status)
                    })
                    
                }
            }
            
            // update daily progress
            
            // use this for daily progress, steps starting 6:00am will be counted towards daily progress
            let cal = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)
            let timeStart = cal?.dateBySettingHour(6, minute: 0, second: 0, ofDate: NSDate(), options: NSCalendarOptions())
            
            self.pedometer.queryPedometerDataFromDate(timeStart!, toDate: NSDate()) { (pedometerData, error) -> Void in
                
                if pedometerData != nil {
                    let steps: UInt = pedometerData!.numberOfSteps.unsignedLongValue
                    
                    status += "\nDAILY: \(steps)/\(Int(kGoalStepsPerDay)) steps"
                    
                    dispatch_async(dispatch_get_main_queue(),{
                        self.dailyProgress = Double(steps)
                        self.statsLabel.text = status
                        print("status = " + status)
                    })
                }
            }
            
        } else {
            status = "pedometer not available"
            self.statsLabel.text = status
        }
    }
    
    // Function use for non M7-processor devices
    func handleStepDetector() {
        SOStepDetector.sharedInstance().startDetectionWithUpdateBlock({
            (error: NSError!, isMoving: Bool) -> Void in
            
            if isMoving {
                if self.coachState == kCoachStateAngry {
                    self.progress += 1
                } else {
                    self.progress = kGoalStepsBetweenNotifications
                }
                self.dailyProgress += 1
            }
            
            if (isMoving != self.isMoving) {
                self.isMoving = isMoving
                self.animateProgressTextImage(isMoving)
            }
            
            if((error) != nil) {
                print("error = \(error.localizedDescription)")
            }
            
            dispatch_async(dispatch_get_main_queue(),{
                let recordToday = Record.getRecordToday()
                var string = "session = \(self.progress) / \(kGoalStepsBetweenNotifications)"
                string += "\n"
                string += "day = \(self.dailyProgress) / \(recordToday.goal!)"
                self.statsLabel.text = string
            })
        })
    }
    
    func showNotification() {
        startWorkOut()
        
        let alert = UIAlertController(title: "Inactivity Coach",
            message: "Move! Move! Move!",
            preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler:nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func clickedActivateButton(sender: AnyObject) {
        setNotification()
    }
    
    func animateCoachImage() {
        let selected = InactivityCoach.sharedInstance.getSharedInfo(kSelectedCoach) as! Int
        let selectedName = coachNames[selected]
        
        let img1 = "coach_\(selectedName)_\(coachState)1"
        let img2 = "coach_\(selectedName)_\(coachState)2"
        let animationImages: [UIImage] = [UIImage(named: img1)!, UIImage(named: img2)!]
        
        coachImage.animationImages = animationImages
        coachImage.animationDuration = 1.0
        coachImage.startAnimating()
    }
    
    func animateProgressTextImage(isShaking: Bool) {
        let empty = UIImage(named: "main_txt_empty")
        let keepgoing = UIImage(named: "main_txt_keepgoing")
        let almostthere = UIImage(named: "main_txt_almostthere")
        let done = UIImage(named:"main_txt_done")
        
        let animationImages1: [UIImage] = [empty!, keepgoing!]
        let animationImages2: [UIImage] = [empty!, almostthere!]
        
        if coachState == kCoachStateNormal || coachState == kCoachStateSleep {
            progressTextImage.stopAnimating()
            progressTextImage.image = empty
        } else {
            if progress < kGoalStepsBetweenNotifications {
                let stepsAlmostThere = kGoalStepsBetweenNotifications * 0.8
                if isShaking {
                    progressTextImage.stopAnimating()
                    progressTextImage.image = (progress < stepsAlmostThere) ? keepgoing : almostthere
                } else {
                    progressTextImage.animationImages = (progress < stepsAlmostThere) ? animationImages1 : animationImages2
                    progressTextImage.animationDuration = 0.7
                    progressTextImage.startAnimating()
                }
            } else {
                progressTextImage.stopAnimating()
                progressTextImage.image = done
            }
        }
    }
    
    func setNotification() {
        notificationOn = !notificationOn!
        
        InactivityCoach.sharedInstance.setSharedInfo(kNotificationOn, value: notificationOn!)
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            InactivityCoach.sharedInstance.turnOnNotification(self.notificationOn!)
        }
    }
    
}

