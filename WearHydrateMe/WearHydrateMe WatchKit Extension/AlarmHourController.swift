//
//  AlarmHourController.swift
//  WearHydrateMe
//
//  Created by Merielle Impreso on 5/21/15.
//  Copyright (c) 2015 Merielle Impreso. All rights reserved.
//

import WatchKit
import Foundation
import DataKit


class AlarmHourController: WKInterfaceController {
    @IBOutlet weak var hourButton: WKInterfaceButton!
    @IBOutlet weak var hourSlider: WKInterfaceSlider!
    
    var parentController: AlarmController? = nil
    var hour: Int = 0

    @IBAction func hourSliderValueChanged(value: Float) {
        hour = Int(value)
        hourButton.setTitle(Formatter.zeroPad(hour))
        hourSlider.setValue(value)
    }
    
    @IBAction func okButtonClicked() {
        parentController!.hour = hour
        DataAccess.sharedInstance.updateAlarmHour(hour)
        self.dismissController()
    }
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        hour = parentController!.hour
        hourButton.setTitle(Formatter.zeroPad(hour))
        hourSlider.setValue(Float(hour))
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
