//
//  InactivityCoach.swift
//  InactivityCoach
//
//  Created by ATserver on 10/22/15.
//  Copyright © 2015 Merielle Impreso. All rights reserved.
//
// Model for InactivityCoach, handles basic functions

import Foundation
import UIKit
import CoreMotion
import WatchConnectivity
import ClockKit	

let kNotificationOn = "notification_on"
let kIntervalHour = "interval_hour"
let kIntervalMinute = "interval_minute"
let kSelectedCoach = "selected_coach"
let kLastStepCount = "last_step_count"
let kLastActiveTime = "last_active_time"
let kSessionProgress = "session_progress"
let kDayProgress = "day_progress"
let kCoachState = "coach_state"
let kUnlockedMasterMaru = "com.aeustech.inactivitycoach.mastermaru"
let kUnlockedDrDoki = "com.aeustech.inactivitycoach.drdoki"
let kUnlockedPrinceWilly = "com.aeustech.inactivitycoach.princewilly"

let kCoachStateNormal = "normal"
let kCoachStateSleep = "sleep"
let kCoachStateAngry = "angry"
let kCoachStateHappy = "happy"

let kNotificationOnDefault = false
let kIntervalHourDefault = 0
let kIntervalMinuteDefault = 1

let kPudgyBirbIndex = 0
let kChipIndex = 1
let kMasterMaruIndex = 2
let kDrDokiIndex = 3
let kPrinceWillyIndex = 4

class WatchInactivityCoach: NSObject, WCSessionDelegate {
    
    // Variables for CoreMotion
    let dataProcessingQueue = NSOperationQueue()
    let activityManager = CMMotionActivityManager()
    
    var session : WCSession? = nil
    
    // Static instance for the model
    class var sharedInstance : WatchInactivityCoach {
        struct Static {
            static var onceToken : dispatch_once_t = 0
            static var instance : WatchInactivityCoach? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = WatchInactivityCoach()
            
            if WCSession.isSupported() {
                Static.instance?.session = WCSession.defaultSession()
                Static.instance?.session!.delegate = Static.instance
                Static.instance?.session!.activateSession()
            }
        }
        return Static.instance!
    }
    
    // Used in handling coach selection, filenames for images
    func getCoachNames() -> [String] {
        var coachNames: [String] = ["", "", "", "", ""]
        coachNames[kPudgyBirbIndex] = "pudgy"
        coachNames[kChipIndex] = "chip"
        coachNames[kMasterMaruIndex] = "maru"
        coachNames[kDrDokiIndex] = "doki"
        coachNames[kPrinceWillyIndex] = "willy"
        
        return coachNames
    }
    
    func getSharedInfo(key: String) -> AnyObject? {
        let sharedAppGroup: NSString = "group.com.aeustech.inactivitycoach"
        let defaults = NSUserDefaults(suiteName: sharedAppGroup as String)
        
        if key == kSelectedCoach || key == kIntervalHour || key == kIntervalMinute  {
            return defaults!.integerForKey(key)
        } else if key == kSessionProgress || key == kDayProgress {
            return defaults!.doubleForKey(key)
        } else if key == kNotificationOn || key == kUnlockedMasterMaru
            || key == kUnlockedPrinceWilly || key == kUnlockedDrDoki {
                return defaults!.boolForKey(key)
        } else if key == kLastActiveTime || key == kCoachState {
            return defaults?.objectForKey(key)
        } else {
            return nil
        }
    }
    
    func setSharedInfo(key: String, value: AnyObject) {
        let sharedAppGroup: NSString = "group.com.aeustech.inactivitycoach"
        let defaults = NSUserDefaults(suiteName: sharedAppGroup as String)
        
        if key == kSelectedCoach || key == kIntervalHour || key == kIntervalMinute  {
            defaults!.setInteger(value as! Int, forKey: key)
        } else if key == kSessionProgress || key == kDayProgress {
            defaults!.setDouble(value as! Double, forKey: key)
        } else if key == kNotificationOn || key == kUnlockedMasterMaru
            || key == kUnlockedPrinceWilly || key == kUnlockedDrDoki {
                defaults!.setBool(value as! Bool, forKey: key)
        } else {
            defaults!.setObject(value, forKey: key)
        }
        defaults!.synchronize()
    }

    
    /*func getSelectedCoach() -> Int {
        return NSUserDefaults.standardUserDefaults().integerForKey(kSelectedCoach)
    }
    
    func saveSelectedCoach(index: Int) {
        refreshComplications()
        NSUserDefaults.standardUserDefaults().setInteger(index, forKey: kSelectedCoach)
    }
    
    func getNotificationOn() -> Bool {
        return NSUserDefaults.standardUserDefaults().boolForKey(kNotificationOn)
    }
    
    func saveNotificationOn(notificationOn: Bool) {
        NSUserDefaults.standardUserDefaults().setBool(notificationOn, forKey: kNotificationOn)
        
        let data = [kNotificationOn: notificationOn]
        session!.sendMessage(data, replyHandler: nil, errorHandler: nil)
    }
    
    func getIntervalHour() -> Int {
        return NSUserDefaults.standardUserDefaults().integerForKey(kIntervalHour)
    }
    
    func saveIntervalHour(hour: Int) {
        NSUserDefaults.standardUserDefaults().setInteger(hour, forKey: kIntervalHour)
    }
    
    func getIntervalMinute() -> Int {
        return NSUserDefaults.standardUserDefaults().integerForKey(kIntervalMinute)
    }
    
    func saveIntervalMinute(minute: Int) {
        NSUserDefaults.standardUserDefaults().setInteger(minute, forKey: kIntervalMinute)
    }*/
    
    func getIntervalTimeDate() -> NSDate {
        var hour = getSharedInfo(kIntervalHour) as! Int
        var minute = getSharedInfo(kIntervalMinute) as! Int
        
        if hour == 0 && minute == 0 {
            hour = kIntervalHourDefault
            minute = kIntervalMinuteDefault
        }
        
        let cal = NSCalendar.currentCalendar()
        let comps = NSDateComponents()
        comps.hour = hour
        comps.minute = minute
        let date = cal.dateFromComponents(comps)
        
        return date!
    }
    
    // Prepare activity updates
    func checkActivity() {
        activityManager.startActivityUpdatesToQueue(dataProcessingQueue) {
            data in
            dispatch_async(dispatch_get_main_queue()) {
                if data!.running {
                    self.log("running")
                } else if data!.cycling {
                    self.log("cycling")
                } else if data!.walking {
                    self.log("walking")
                } else if data!.stationary {
                    self.log("stationary")
                } else {
                    
                }
            }
        }
    }
    
    func refreshComplications() {
        let complicationServer = CLKComplicationServer.sharedInstance()
        if complicationServer != nil {
            for complication in complicationServer.activeComplications {
                complicationServer.reloadTimelineForComplication(complication)
            }
        }
        
        
    }
    
    func log(string: String) {
        print(string)
    }
    
}