//
//  GlanceController.swift
//  WearHydrateMe
//
//  Created by Merielle Impreso on 6/11/15.
//  Copyright (c) 2015 Merielle Impreso. All rights reserved.
//

import WatchKit
import Foundation
import DataKit


class GlanceController: WKInterfaceController {
    @IBOutlet weak var drankLabel: WKInterfaceLabel!
    @IBOutlet weak var goalLabel: WKInterfaceLabel!
    @IBOutlet weak var lastEntryLabel: WKInterfaceLabel!
    @IBOutlet weak var lastEntryTitleLabel: WKInterfaceLabel!
    
    var allRecords: [Record] = []
    var allGoals: [Goal] = []
    var recordsToday: [Record] = []
    var goalToday: Goal?
    
    func getRecordsToday() -> [Record] {
        var recordsToday: [Record] = []
        let formatter = NSDateFormatter()
        formatter.dateStyle = .ShortStyle
        let dateToday = formatter.stringFromDate(NSDate())
        
        allRecords = DataAccess.sharedInstance.getAllRecords()
        for record in allRecords {
            let date = formatter.stringFromDate(record.timeStamp)
            if (date == dateToday) {
                recordsToday.append(record)
            }
        }
        return recordsToday
    }
    
    func getGoalToday() -> Goal? {
        var goalToday: Goal?
        allGoals = DataAccess.sharedInstance.getAllGoals()
        
        if (allGoals.count == 0) {
            goalToday = DataAccess.sharedInstance.insertGoal()
        } else {
            var latestGoal = allGoals.first!
            
            let formatter = NSDateFormatter()
            formatter.dateStyle = .ShortStyle
            let dateToday = formatter.stringFromDate(NSDate())
            let date = formatter.stringFromDate(latestGoal.date)
            
            if (date == dateToday) {
                goalToday = latestGoal
            } else {
                goalToday = DataAccess.sharedInstance.insertGoal()
            }
        }
        return goalToday
    }

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        recordsToday = getRecordsToday()
        goalToday = getGoalToday()
        
        drankLabel.setText("\(recordsToday.count)")
        goalLabel.setText("\(goalToday!.number)")
        
        if (recordsToday.count > 0) {
            let formatter = NSDateFormatter()
            formatter.timeStyle = .MediumStyle
            let record = recordsToday.first!
            let time = formatter.stringFromDate(record.timeStamp)
            lastEntryTitleLabel.setText("Last Entry")
            lastEntryLabel.setText("\(time)")
        } else {
            lastEntryTitleLabel.setText("")
            lastEntryLabel.setText("")
        }
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
