//
//  TodayViewController.swift
//  Hydrate Me
//
//  Created by Merielle Impreso on 6/16/15.
//  Copyright (c) 2015 Merielle Impreso. All rights reserved.
//

import UIKit
import NotificationCenter
import DataKit

class TodayViewController: UIViewController, NCWidgetProviding {
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var lastEntryLabel: UILabel!
    @IBOutlet weak var countProgressView: UIProgressView!
    
    var allRecords: [Record] = []
    var allGoals: [Goal] = []
    var recordsToday: [Record] = []
    var goalToday: Goal?
    
    @IBAction func drinkButtonClicked(sender: AnyObject) {
        let url =  NSURL(string:"WearHydrateMe://")
        
        self.extensionContext!.openURL(url!, completionHandler:{(success: Bool) -> Void in
            print("task done!")
        })
    }
    
    func getRecordsToday() -> [Record] {
        var recordsToday: [Record] = []
        let formatter = NSDateFormatter()
        formatter.dateStyle = .ShortStyle
        let dateToday = formatter.stringFromDate(NSDate())
        
        allRecords = DataAccess.sharedInstance.getAllRecords()
        for record in allRecords {
            let date = formatter.stringFromDate(record.timeStamp)
            if (date == dateToday) {
                recordsToday.append(record)
            }
        }
        return recordsToday
    }
    
    func getGoalToday() -> Goal? {
        var goalToday: Goal?
        allGoals = DataAccess.sharedInstance.getAllGoals()
        
        if (allGoals.count == 0) {
            goalToday = DataAccess.sharedInstance.insertGoal()
        } else {
            var latestGoal = allGoals.first!
            
            let formatter = NSDateFormatter()
            formatter.dateStyle = .ShortStyle
            let dateToday = formatter.stringFromDate(NSDate())
            let date = formatter.stringFromDate(latestGoal.date)
            
            if (date == dateToday) {
                goalToday = latestGoal
            } else {
                goalToday = DataAccess.sharedInstance.insertGoal()
            }
        }
        return goalToday
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view from its nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func widgetPerformUpdateWithCompletionHandler(completionHandler: ((NCUpdateResult) -> Void)) {
        // Perform any setup necessary in order to update the view.

        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
        
        recordsToday = getRecordsToday()
        goalToday = getGoalToday()
        countLabel.text = "\(recordsToday.count) out of \(goalToday!.number)"
        
        countProgressView.progress = Float(recordsToday.count) / Float(goalToday!.number)
        
        if (recordsToday.count > 0) {
            let formatter = NSDateFormatter()
            formatter.timeStyle = .MediumStyle
            let record = recordsToday.first!
            let time = formatter.stringFromDate(record.timeStamp)
            lastEntryLabel.text = "Last Entry \(time)"
        } else {
            lastEntryLabel.text = "No entry"
        }

        completionHandler(NCUpdateResult.NewData)
    }
    
    func widgetMarginInsetsForProposedMarginInsets(defaultMarginInsets: UIEdgeInsets) -> UIEdgeInsets {
        return UIEdgeInsetsZero
    }
}
