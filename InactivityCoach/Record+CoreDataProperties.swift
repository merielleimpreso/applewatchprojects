//
//  Record+CoreDataProperties.swift
//  InactivityCoach
//
//  Created by Merielle Impreso on 11/26/15.
//  Copyright © 2015 Merielle Impreso. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData
    
extension Record {

    @NSManaged var date: NSDate?
    @NSManaged var goal: NSNumber?
    @NSManaged var progress: NSNumber?

}
