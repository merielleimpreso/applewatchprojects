//
//  CoachSelectViewController.swift
//  InactivityCoach
//
//  Created by ATserver on 10/26/15.
//  Copyright © 2015 Merielle Impreso. All rights reserved.
//

import UIKit

class CoachSelectViewController: UIViewController, UIPageViewControllerDataSource {
    
    // MARK: - Variables
    private var pageViewController: UIPageViewController?
    private let coachNames = InactivityCoach.sharedInstance.getCoachNames()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupPageControl()
        createPageViewController()
    }
    
    private func setupPageControl() {
        let appearance = UIPageControl.appearance()
        appearance.pageIndicatorTintColor = colorize(0xCCCCCC)
        appearance.currentPageIndicatorTintColor = colorize(0x44B8E9)
        appearance.backgroundColor = colorize(0xF5F5F5)
        appearance.opaque = false
    }

    private func createPageViewController() {
        for view in self.view.subviews {
            view.removeFromSuperview()
        }
        
        let pageController = self.storyboard!.instantiateViewControllerWithIdentifier("PageController") as! UIPageViewController
        pageController.dataSource = self
        
        if coachNames.count > 0 {
            let selectedCoach = InactivityCoach.sharedInstance.getSharedInfo(kSelectedCoach) as! Int
            let controller = getItemController(selectedCoach)!
            let startingViewControllers: NSArray = [controller]
            pageController.setViewControllers(startingViewControllers as? [UIViewController], direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil)
        }
        
        pageViewController = pageController
        addChildViewController(pageViewController!)
        self.view.addSubview(pageViewController!.view)
        pageViewController!.didMoveToParentViewController(self)
    }
    
    func colorize (hex: Int, alpha: Double = 1.0) -> UIColor {
        let red = Double((hex & 0xFF0000) >> 16) / 255.0
        let green = Double((hex & 0xFF00) >> 8) / 255.0
        let blue = Double((hex & 0xFF)) / 255.0
        let color: UIColor = UIColor( red: CGFloat(red), green: CGFloat(green), blue: CGFloat(blue), alpha:CGFloat(alpha) )
        return color
    }
    
    // MARK: - UIPageViewControllerDataSource
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as! PageItemViewController
        
        if itemController.itemIndex > 0 {
            return getItemController(itemController.itemIndex-1)
        }
        
        return nil
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as! PageItemViewController
        
        if itemController.itemIndex+1 < coachNames.count {
            return getItemController(itemController.itemIndex+1)
        }
        
        return nil
    }
    
    private func getItemController(itemIndex: Int) -> PageItemViewController? {
        
        if itemIndex < coachNames.count {
            let pageItemController = self.storyboard!.instantiateViewControllerWithIdentifier("ItemController") as! PageItemViewController
            pageItemController.parentController = self
            pageItemController.itemIndex = itemIndex
            pageItemController.name = coachNames[itemIndex]
            return pageItemController
        }
        
        return nil
    }
    
    // MARK: - Page Indicator
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return coachNames.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        let selectedCoach = InactivityCoach.sharedInstance.getSharedInfo(kSelectedCoach) as! Int
        return selectedCoach
    }
    
}
