//
//  ActiveMinuteController.swift
//  WearHydrateMe
//
//  Created by Merielle Impreso on 5/21/15.
//  Copyright (c) 2015 Merielle Impreso. All rights reserved.
//

import WatchKit
import Foundation
import DataKit


class ActiveMinuteController: WKInterfaceController {
    @IBOutlet weak var titleLabel: WKInterfaceLabel!
    @IBOutlet weak var minuteButton: WKInterfaceButton!
    @IBOutlet weak var minuteSlider: WKInterfaceSlider!
    
    var parentController: ActiveController? = nil
    var segueIdentifier: String = ""
    var minute: Int = 0

    @IBAction func minuteSliderValueChanged(value: Float) {
        minute = Int(value)
        minuteButton.setTitle(Formatter.zeroPad(minute))
        minuteSlider.setValue(value)
    }
    
    @IBAction func okButtonClicked() {
        if segueIdentifier == "activeFromMinute" {
            parentController!.fromMinute = minute
            DataAccess.sharedInstance.updateActiveFromMinute(minute)
        } else {
            parentController!.toMinute = minute
            DataAccess.sharedInstance.updateActiveToMinute(minute)
        }
        self.dismissController()
    }
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        let data = context as! NSDictionary
        parentController = (data.objectForKey("parentController") as! ActiveController)
        segueIdentifier = data.objectForKey("segueIdentifier") as! String
        
        if segueIdentifier == "activeFromMinute" {
            minute = parentController!.fromMinute
            titleLabel.setText("Active From: MINUTE")
        } else {
            minute = parentController!.toMinute
            titleLabel.setText("Active To: MINUTE")
        }
        minuteButton.setTitle(Formatter.zeroPad(minute))
        minuteSlider.setValue(Float(minute))
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        

    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
