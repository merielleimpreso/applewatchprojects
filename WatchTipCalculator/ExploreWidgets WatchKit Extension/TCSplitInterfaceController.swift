//
//  InterfaceController.swift
//  ExploreWidgets WatchKit Extension
//
//  Created by Lito Ang on 1/22/15.
//  Copyright (c) 2015 Aeus. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity


class TCSplitInterfaceController: WKInterfaceController, InterfaceControllerDelegate, WCSessionDelegate {
    
    @IBOutlet weak var btnSplit: WKInterfaceButton!
    @IBOutlet weak var lblPay: WKInterfaceLabel!
    @IBOutlet weak var btnUnlock: WKInterfaceButton!
    @IBOutlet weak var label: WKInterfaceLabel!
    @IBOutlet weak var labelPeople: WKInterfaceLabel!
    
    @IBOutlet var grpSplitUI: WKInterfaceGroup!
    
    @IBOutlet var pckSplit: WKInterfacePicker!
    
    
    var pay:Float = 0
    var splitPay:Float = 0
    var numSplit:Int = 1

    var amountEntered:String = "0"
    var numPadSegue:String? = ""
    var data:String? = "15"

    let formatNumber = NSNumberFormatter()
    
    var delegate:InterfaceControllerDelegate?
    var controllerContext:ControllerContext?
    
    var session : WCSession!

    
    func log(message:String) {
//        println(message)
    }
    
    func storeSharedInfo(key:String, value:String) {
        
        let appGroupID = "group.com.aeustech.watchtipcalculator"
        let defaults = NSUserDefaults(suiteName: appGroupID)
        
        defaults!.setObject(value, forKey: key)
        
        defaults!.synchronize()
        
//        log("stored latest shared info")
        
    }
    
    func session(_ session: WCSession,
        didReceiveApplicationContext applicationContext: [String : AnyObject]) {
            let unlocked = applicationContext["split_unlocked"] as! NSString
            log("watchos - received app context = \(unlocked)")
            
            if(unlocked == "yes") {
                storeSharedInfo("split_unlocked", value:"yes")
                checkUnlock()
            }
            
    }
    
    func retrieveSharedInfo(key:String) -> String {
        
        let appGroupID = "group.com.aeustech.watchtipcalculator"
        let defaults = NSUserDefaults(suiteName: appGroupID)
        
        defaults!.synchronize()
        
        if let value = defaults!.objectForKey(key) as? String {
            return value
        } else {
            return ""
        }
        
    }
    
    @IBAction func pickerSplitAction(value: Int) {
        self.updateAmount(String(value+1))
    }
    
    @IBAction func splitButtonAction() {
        numSplit++
        self.updateAmount("\(numSplit)")
    }
    
    @IBAction func menuItem1Action() {
        numSplit = 3
        self.updateAmount("\(numSplit)")
        pckSplit.setSelectedItemIndex(numSplit-1)
    }
    
    @IBAction func menuItem2Action() {
        numSplit = 5
        self.updateAmount("\(numSplit)")
        pckSplit.setSelectedItemIndex(numSplit-1)
    }
    
    @IBAction func menuItem3Action() {
        numSplit = 1
        self.updateAmount("\(numSplit)")
        pckSplit.setSelectedItemIndex(numSplit-1)
    }
    
    
    override func contextForSegueWithIdentifier(_segueIdentifier: String) -> AnyObject? {
//        println(_segueIdentifier)
        
        self.numPadSegue = _segueIdentifier
        
        controllerContext?.delegate = self
        
        return controllerContext
    }
    
    private func showAlertControllerWithStyle(style: WKAlertControllerStyle!, title: String, message: String) {
        
        let defaultAction = WKAlertAction(
            title: "Dismiss",
            style: WKAlertActionStyle.Default) { () -> Void in
                
//                print("Dismiss")
        }
        
//        let cancelAction = WKAlertAction(
//            title: "Cancel",
//            style: WKAlertActionStyle.Cancel) { () -> Void in
//                
//                print("Cancel")
//        }
//        
//        let destructiveAction = WKAlertAction(
//            title: "Destructive",
//            style: WKAlertActionStyle.Destructive) { () -> Void in
//                
//                print("Destructive")
//        }
        
//        var actions = [defaultAction, destructiveAction]
        let actions = [defaultAction]

        
        // exactly two actions are needed for WKAlertControllerStyleSideBySideButtonsAlert
//        if style != WKAlertControllerStyle.SideBySideButtonsAlert {
//            actions.append(cancelAction)
//        }
        
        self.presentAlertControllerWithTitle(
            title,
            message: message,
            preferredStyle: style,
            actions: actions)
    }
    
    func didEnterAmount(amount:String) {
        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(0.6 * Double(NSEC_PER_SEC)))
        dispatch_after(time, dispatch_get_main_queue(), {
            self.updateAmount(amount)
        })
    }
    
    func updateAmount(amount:String) {
//        btnSplit.setTitle(amount)
        splitPay = pay / (amount as NSString).floatValue
        
        numSplit = (amount as NSString).integerValue

        log("splitPay = \(splitPay)")
        
        delegate?.data = amount
        
        lblPay.setText(formatNumber.stringFromNumber(splitPay))
        
        storeSharedInfo("num_split", value: amount)

    }
    
    func checkUnlock() {
        
//        let unlocked = retrieveSharedInfo("split_unlocked")
        
        // TEST ONLY
//        let unlocked = "yes"
     
        let unlocked = retrieveSharedInfo("split_unlocked")
        if unlocked == "yes" {
            self.lblPay.setHidden(false)
            self.btnUnlock.setHidden(true)
        } else {
            self.lblPay.setHidden(true)
            self.btnUnlock.setHidden(false)
        }
        
//        let dictionary = ["request":"unlock_split"]
//        WKInterfaceController.openParentApplication(dictionary) {
//            (replyInfo, error) -> Void in
//            
//            let replyValue = replyInfo["response"] as! String
//            
//            println("replyValue = \(replyValue)")
//            
//            if replyValue == "yes" {
//                self.lblPay.setHidden(false)
//                self.btnUnlock.setHidden(true)
//            }
//        }
    }
    
    
    @IBAction func btnUnlockAction() {
//        log("unlock split feature")
        self.showAlertControllerWithStyle(WKAlertControllerStyle.Alert, title:"",  message:"Unlock this feature from the phone app.")
    }
    

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        formatNumber.numberStyle = NSNumberFormatterStyle.CurrencyStyle
        //TEST ONLY
//        formatNumber.locale = NSLocale(localeIdentifier: "de_DE")
        
        let controllerContext = (context as! ControllerContext)
        delegate = controllerContext.delegate
        
        self.controllerContext = controllerContext
        
        pay = (delegate as! TCTipInterfaceController).pay

        log("pay total = \(pay)")
        
        checkUnlock()
        
//        self.updateAmount("\(numSplit)")
//        self.updateAmount(delegate!.data!)

        
//        let attributedText = NSAttributedString(
//            string: "EACH PAYS",
//            attributes: NSDictionary (object: UIFont(name: "Roboto-Thin", size: 8)!,
//                forKey: NSFontAttributeName) as [NSObject : AnyObject]
//        )
//        
//        label.setAttributedText(attributedText)
        
        
//        let attributedTextPeople = NSAttributedString(
//            string: "PEOPLE",
//            attributes: NSDictionary (object: UIFont(name: "Roboto-Thin", size: 8)!,
//                forKey: NSFontAttributeName) as [NSObject : AnyObject]
//        )
//        
//        labelPeople.setAttributedText(attributedTextPeople)

    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        var pickerSplitItems: [WKPickerItem]! = []
        
        
        for i in 1...30 {
            let pickerItem = WKPickerItem()
            pickerItem.title = "\(i)"
            pickerItem.caption = "\(i)"
            pickerSplitItems.append(pickerItem)
        }

        // create animated images and picker items
        var images: [UIImage]! = []
        for (var i=0; i<=15; i++) {
            let name = "progress_circle-\(i)"
            images.append(UIImage(named: name)!)
        }
        let progressImages = UIImage.animatedImageWithImages(images, duration: 0.0)

        grpSplitUI.setBackgroundImage(progressImages)
        
        pckSplit.setCoordinatedAnimations([grpSplitUI])
        pckSplit.setItems(pickerSplitItems)
        pckSplit.focus()
        
        let split = Int(retrieveSharedInfo("num_split"))
        numSplit = split == nil || split == 0 ? 0 : split! - 1
        
        pckSplit.setSelectedItemIndex(numSplit)
        
        self.updateAmount(String(numSplit+1))

        
        if (WCSession.isSupported()) {
            session = WCSession.defaultSession()
            session.delegate = self
            session.activateSession()
        }

//        resetColors()

    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    

    


}
