//
//  CoachPickerController.swift
//  InactivityCoach
//
//  Created by Merielle Impreso on 10/6/15.
//  Copyright © 2015 Merielle Impreso. All rights reserved.
//

import WatchKit
import Foundation


class CoachPickerController: WKInterfaceController {
    @IBOutlet var picker: WKInterfacePicker!
    var items: [String]! = nil
    
    var isUnlockedMasterMaru = false
    var isUnlockedDrDoki = false
    var isUnlockedPrinceWilly = false
    
    var coaches: [(String, String)] = [
        ("Pudgy Birb", "picker_birb.png"),
        ("Chip", "picker_chip.png"),
        ("Master Maru", "picker_maru.png"),
        ("Dr. Doki", "picker_doki.png"),
        ("Prince Willy", "picker_prince.png") ]
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        if !isUnlockedMasterMaru {
            coaches[kMasterMaruIndex] = ("Master Maru", "picker_maru_locked.png")
        }
        if !isUnlockedDrDoki {
            coaches[kDrDokiIndex] = ("Dr. Doki", "picker_doki_locked.png")
        }
        if !isUnlockedMasterMaru {
            coaches[kPrinceWillyIndex] = ("Prince Willy", "picker_prince_locked.png")
        }
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        
        let pickerItems: [WKPickerItem] = coaches.map {
            let pickerItem = WKPickerItem()
            pickerItem.caption = $0.0
            pickerItem.contentImage = WKImage(imageName: $0.1)
            return pickerItem
        }
        picker.setItems(pickerItems)
        picker.focus()
    }
    
    @IBAction func pickerSelectedItemChanged(value: Int) {
        if value == kMasterMaruIndex {
            if isUnlockedMasterMaru {
                WatchInactivityCoach.sharedInstance.setSharedInfo(kSelectedCoach, value: value)
            }
        } else if value == kDrDokiIndex {
            if isUnlockedDrDoki {
                WatchInactivityCoach.sharedInstance.setSharedInfo(kSelectedCoach, value: value)
            }
        } else if value == kPrinceWillyIndex {
            if isUnlockedPrinceWilly {
                WatchInactivityCoach.sharedInstance.setSharedInfo(kSelectedCoach, value: value)
            }
        } else {
            WatchInactivityCoach.sharedInstance.setSharedInfo(kSelectedCoach, value: value)
        }
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
