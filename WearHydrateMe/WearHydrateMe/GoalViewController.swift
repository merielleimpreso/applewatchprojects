//
//  GoalViewController.swift
//  WearHydrateMe
//
//  Created by Merielle Impreso on 5/27/15.
//  Copyright (c) 2015 Merielle Impreso. All rights reserved.
//

import UIKit
import DataKit

class GoalViewController: UIViewController {
    @IBOutlet weak var goalLabel: UILabel!
    
    var allGoals: [Goal] = []
    var goalToday: Goal?
    
    let minimumGoal = 3
    let maximumGoal = 25
    let recommendedGoal = 8
    
    @IBAction func subtractButtonClicked(sender: AnyObject) {
        var number = goalToday!.number as Int
        number -= 1
        
        if (number >= minimumGoal) {
            var g = goalToday
            g!.number = number
            g!.date = NSDate()
            
            //goalToday!.number = number as NSNumber
            goalLabel.text = "\(g!.number)"
            self.goalToday = DataAccess.sharedInstance.updateGoalToday(g!)
        }
    }
    
    @IBAction func addButtonClicked(sender: AnyObject) {
        var number = goalToday!.number as Int
        
        if (number == recommendedGoal) {
            var alert = UIAlertController(title: "WARNING",
                message: "Recommended intake is 8 glasses. Drinking more than the daily amount may lead to water intoxication. Continue?",
                preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler:nil))
            alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { action in
                number += 1
                
                if (number <= self.maximumGoal) {
                    var g = self.goalToday
                    g!.number = number
                    g!.date = NSDate()
                    
                    
                    //self.goalToday!.number = number as NSNumber
                    self.goalLabel.text = "\(g!.number)"
                    self.goalToday = DataAccess.sharedInstance.updateGoalToday(g!)
                }
            }))
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            number += 1
            
            if (number <= maximumGoal) {
                var g = goalToday
                g!.number = number
                g!.date = NSDate()
                
                //self.goalToday!.number = number as NSNumber
                self.goalLabel.text = "\(g!.number)"
                self.goalToday = DataAccess.sharedInstance.updateGoalToday(g!)
            }
        }
    }
    
    func getGoalToday() -> Goal? {
        var goalToday: Goal? = nil
        allGoals = DataAccess.sharedInstance.getAllGoals()
        
        let formatter = NSDateFormatter()
        formatter.dateStyle = .ShortStyle
        let dateToday = formatter.stringFromDate(NSDate())
        for goal in allGoals {
            let date = formatter.stringFromDate(goal.date)
            if (date == dateToday) {
                goalToday = goal
                break
            }
        }
        
        if (goalToday == nil) {
            goalToday = DataAccess.sharedInstance.insertGoal()
        }
        
        return goalToday
    }

    /*func getGoalToday() -> Goal? {
        var goalToday: Goal?
        allGoals = DataAccess.sharedInstance.getAllGoals()
        
        if (allGoals.count == 0) {
            goalToday = DataAccess.sharedInstance.insertGoal()
        } else {
            var latestGoal = allGoals.first!
            
            let formatter = NSDateFormatter()
            formatter.dateStyle = .ShortStyle
            let dateToday = formatter.stringFromDate(NSDate())
            let date = formatter.stringFromDate(latestGoal.date)
            
            if (date == dateToday) {
                goalToday = latestGoal
            } else {
                goalToday = DataAccess.sharedInstance.insertGoal()
            }
        }
        return goalToday
    }*/

    override func viewDidLoad() {
        super.viewDidLoad()

        goalToday = getGoalToday()
        goalLabel.text = "\(goalToday!.number)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
