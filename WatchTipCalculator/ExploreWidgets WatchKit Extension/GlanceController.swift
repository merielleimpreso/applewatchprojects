//
//  GlanceController.swift
//  ExploreWidgets WatchKit Extension
//
//  Created by Lito Ang on 1/22/15.
//  Copyright (c) 2015 Aeus. All rights reserved.
//

import WatchKit
import Foundation

// class Tip: NSObject {
//    var billAmount: String!
//    var tipPercentage: String!
//}

class GlanceController: WKInterfaceController {

    @IBOutlet weak var billAmount: WKInterfaceLabel!
    @IBOutlet weak var tipPercentage: WKInterfaceLabel!
    @IBOutlet weak var tipAmount: WKInterfaceLabel!
    @IBOutlet weak var totalAmount: WKInterfaceLabel!
    
    let formatNumber = NSNumberFormatter()
    
//    @IBOutlet weak var tipsTable: WKInterfaceTable!
//    
//    func reloadTable() {
//        var tips = [Tip]()
//        
//        var tip1 = Tip()
//        tip1.billAmount = "100"
//        tip1.tipPercentage = "20"
//        tips.append(tip1)
//        
//        var tip2 = Tip()
//        tip2.billAmount = "10"
//        tip2.tipPercentage = "22"
//        tips.append(tip2)
//        
//        var tip3 = Tip()
//        tip3.billAmount = "450"
//        tip3.tipPercentage = "18"
//        tips.append(tip3)
//        
//        tipsTable.setNumberOfRows(1, withRowType: "TipsRow")
//        
//        for(index, tip) in enumerate(tips) {
//            if let row = tipsTable.rowControllerAtIndex(index) as? TipsRow {
//                row.lblBillAmount.setText("Bill: " + tip.billAmount)
//                row.lblTipPercentage.setText("Tip %: " + tip.tipPercentage)
//                row.lblTipAmount.setText("Tip Amount: " + "0.00")
//                row.lblTotalAmount.setText("Total Amount: " + "0.00")
//                
//            }
//            
//            println(tip.billAmount)
//
//        }
//
//    }

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
//        reloadTable()
        
//        var defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        

    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        formatNumber.numberStyle = NSNumberFormatterStyle.CurrencyStyle
        //        formatNumber.locale = NSLocale(localeIdentifier: "en_US")
        
        let appGroupID = "group.com.aeustech.watchtipcalculator"
        let defaults = NSUserDefaults(suiteName: appGroupID)
        
        
        //        var billAmt = defaults.stringForKey("bill_amount")
        //        println("bill amount = \(billAmt)")
        
        
        if let billAmt = defaults!.stringForKey("bill_amount") {
            //            println("bill amount = \(billAmt)")
            billAmount.setText(formatNumber.stringFromNumber((billAmt as NSString).floatValue))
        }
        
        if let tipPercent = defaults!.stringForKey("tip_percent") {
            tipPercentage.setText(tipPercent)
        }
        
        if let tipAmt = defaults!.stringForKey("tip_amount") {
            tipAmount.setText(formatNumber.stringFromNumber((tipAmt as NSString).floatValue))
        }
        
        
        if let totalAmt = defaults!.stringForKey("total_amount") {
            totalAmount.setText(formatNumber.stringFromNumber((totalAmt as NSString).floatValue))
        }
        
        //        println("loaded standard user defaults")
        

        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
