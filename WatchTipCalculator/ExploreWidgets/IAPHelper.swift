//
//  IAPHelper.swift
//  WatchTipCalculator
//
//  Created by dev on 5/6/15.
//  Copyright (c) 2015 Aeus. All rights reserved.
//

import StoreKit

class IAPHelper : NSObject, SKPaymentTransactionObserver, SKProductsRequestDelegate {
    var product : SKProduct?
    var productID = "com.aeustech.iap.split1"
    
    var parentController:ViewController?
    
    
    func initIAP(controller:ViewController) {
        parentController = controller
        SKPaymentQueue.defaultQueue().addTransactionObserver(self)
    }
    
    func log(message:String) {
//        println(message)
    }
    
    func buyProduct(product: SKProduct){
        log("Sending the Payment Request to Apple")
        let payment = SKPayment(product: product)
        SKPaymentQueue.defaultQueue().addPayment(payment);
        
    }
    
    func restorePurchases(){
        log("restore purchases");
        SKPaymentQueue.defaultQueue().restoreCompletedTransactions()
    }
    
    func fetchProducts() {
        if SKPaymentQueue.canMakePayments() {
            let prodID:NSSet = NSSet(object: self.productID)
            let productsRequest:SKProductsRequest = SKProductsRequest(productIdentifiers: prodID as! Set<String>)
            productsRequest.delegate = self
            productsRequest.start()
            
            log("fetch products")
            
        } else {
            log("cannot make payments")
        }
    }
    
    func productsRequest(request: SKProductsRequest, didReceiveResponse response: SKProductsResponse) {
        log("didReceiveResponse")
        
        let count:Int = response.products.count
        if (count>0) {
            var validProducts = response.products
            let validProduct: SKProduct = response.products[0] 
            if (validProduct.productIdentifier == self.productID) {
                log(validProduct.localizedTitle)
                log(validProduct.localizedDescription)
                log("\(validProduct.price)")
                
                buyProduct(validProduct);
                
            } else {
                log(validProduct.productIdentifier)
            }
        } else {
            log("nothing")
        }
    }
    
    
    func paymentQueue(queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction])    {
//        println("Received Payment Transaction Response from Apple");
        
        for transaction:AnyObject in transactions {
            if let trans:SKPaymentTransaction = transaction as? SKPaymentTransaction{
                switch trans.transactionState {
                case .Purchased:
                    log("Product Purchased");
                    parentController?.iapCallBack("success")
                    SKPaymentQueue.defaultQueue().finishTransaction(transaction as! SKPaymentTransaction)
                    break;
                case .Failed:
                    log("Purchased Failed");
                    SKPaymentQueue.defaultQueue().finishTransaction(transaction as! SKPaymentTransaction)
                    parentController?.iapCallBack("failure")

                    break;
                case .Restored:
                    log("Product Restored");
                    parentController?.iapCallBack("success")
                    SKPaymentQueue.defaultQueue().finishTransaction(transaction as! SKPaymentTransaction)
                default:
                    break;
                }
            }
        }
    }
}