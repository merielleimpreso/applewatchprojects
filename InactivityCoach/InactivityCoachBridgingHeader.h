//
//  InactivityCoachBridgingHeader.h
//  InactivityCoach
//
//  Created by ATserver on 11/19/15.
//  Copyright © 2015 Merielle Impreso. All rights reserved.
//

#ifndef InactivityCoachBridgingHeader_h
#define InactivityCoachBridgingHeader_h

#import "SOMotionDetector.h"
#import "SOLocationManager.h"
#import "SOStepDetector.h"

#endif /* InactivityCoachBridgingHeader_h */
