//
//  InterfaceController.swift
//  ExploreWidgets WatchKit Extension
//
//  Created by Lito Ang on 1/22/15.
//  Copyright (c) 2015 Aeus. All rights reserved.
//

import WatchKit
import Foundation
import ClockKit

class TCTipInterfaceController: WKInterfaceController, InterfaceControllerDelegate {
    
    @IBOutlet weak var btnTipPercent: WKInterfaceButton!
    @IBOutlet weak var lblPay: WKInterfaceLabel!
    @IBOutlet weak var lblTipAmt: WKInterfaceLabel!
    @IBOutlet weak var label: WKInterfaceLabel!
    
    @IBOutlet var pckTip: WKInterfacePicker!
    
    @IBOutlet var grpTipUI: WKInterfaceGroup!

    
    var bill:Float = 0;
    var tipPercent:Float = 0
    var tipText:Int = 15
    var pay:Float = 0

    var amountEntered:String = "0"
    var numPadSegue:String? = ""
    var data:String? = "1"
    
    let formatNumber = NSNumberFormatter()
    var tipChanged = false
    
    var delegate:InterfaceControllerDelegate?
    var controllerContext:ControllerContext?
    
    @IBAction func menuItem1Action() {
        self.updateAmount("10")
        pckTip.setSelectedItemIndex(10)
    }
    
    @IBAction func menuItem2Action() {
        self.updateAmount("15")
        pckTip.setSelectedItemIndex(15)
    }
    
    @IBAction func menuItem3Action() {
        self.updateAmount("20")
        pckTip.setSelectedItemIndex(20)
    }
    
    @IBAction func menuItem4Action() {
        self.updateAmount("25")
        pckTip.setSelectedItemIndex(25)
    }
    
    @IBAction func incrementAmount() {
        tipText++
        self.updateAmount("\(tipText)")
    }
    
    func log(message:String) {
//        println(message)
    }
    
    @IBAction func pickerSelectedTipPercentage(value: Int) {
        storeSharedInfo("tip", value: String(value))
        storeSharedInfo("tip_percent", value: String(value))

        
        tipPercent = Float(value)
        
        updateAmount(String(tipPercent))
        
        tipChanged = true
        
//        calculateAmounts()
    }
    
    func storeSharedInfo(key:String, value:String) {
        
        let appGroupID = "group.com.aeustech.watchtipcalculator"
        let defaults = NSUserDefaults(suiteName: appGroupID)
        
        defaults!.setObject(value, forKey: key)
        
        defaults!.synchronize()
        
        log("stored latest shared info")
        
    }
    
    func retrieveSharedInfo(key:String) -> String {
        
        let appGroupID = "group.com.aeustech.watchtipcalculator"
        let defaults = NSUserDefaults(suiteName: appGroupID)
        
        defaults!.synchronize()
        
        if let value = defaults!.objectForKey(key) as? String {
            return value
        } else {
            return ""
        }
        
    }
    
    override func contextForSegueWithIdentifier(_segueIdentifier: String) -> AnyObject? {
//        log(_segueIdentifier)
        
        self.numPadSegue
            = _segueIdentifier
        
        controllerContext?.delegate = self
        
        return controllerContext
    }
    
    func didEnterAmount(amount:String) {
//        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(0.6 * Double(NSEC_PER_SEC)))
//        dispatch_after(time, dispatch_get_main_queue(), {
//            self.updateAmount(amount)
//        })
    }
    
    func updateAmount(amount:String) {
//        btnTipPercent.setTitle(amount)
        tipText = (amount as NSString).integerValue
        tipPercent = Float(tipText) / 100
        pay = bill + (bill * tipPercent)
        
        log("bill = \(bill)")
        log("tipPercent = \(tipPercent)")
        log("pay = \(pay)")
        
        lblPay.setText(formatNumber.stringFromNumber(pay))
        lblTipAmt.setText(formatNumber.stringFromNumber((bill * tipPercent)))
        
        delegate?.data = amount
    
        storeTipInfo()
        
    }

    func storeTipInfo() {
//        var defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        let appGroupID = "group.com.aeustech.watchtipcalculator"
        let defaults = NSUserDefaults(suiteName: appGroupID)
        
        defaults!.setObject(bill, forKey: "bill_amount")
        defaults!.setObject(tipPercent * 100, forKey: "tip_percent")
        defaults!.setObject(bill * tipPercent, forKey: "tip_amount")
        defaults!.setObject(pay, forKey: "total_amount")

        defaults!.synchronize()
        
        log("stored latest tip info")
        
//        let billAmt = defaults.stringForKey("bill_amount")
//        log("stored bill amount = \(billAmt)")
    }
    
    @IBAction func splitOpenAction() {
        controllerContext?.delegate = self
        
        pushControllerWithName("Split", context:controllerContext)
    }
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        formatNumber.numberStyle = NSNumberFormatterStyle.CurrencyStyle
//        formatNumber.locale = NSLocale(localeIdentifier: "en_US")
        
        //TEST ONLY
//        formatNumber.locale = NSLocale(localeIdentifier: "de_DE")

        
        let controllerContext = (context as! ControllerContext)
        delegate = controllerContext.delegate
        
        self.controllerContext = controllerContext
        
        bill = ((delegate as! TCBillInterfaceController).amountEntered as NSString).floatValue
        
        log("bill page = \((delegate as! TCBillInterfaceController).amountEntered)")
        log("bill = \(bill)")
        
        pay = bill
        lblPay.setText(formatNumber.stringFromNumber(pay))
        
//        self.updateAmount("\(tipText)")
        
        self.updateAmount(delegate!.data!)
        
//        let attributedText = NSAttributedString(
//            string: "TOTAL AMOUNT",
//            attributes: NSDictionary (object: UIFont(name: "Roboto-Thin", size: 8)!,
//                forKey: NSFontAttributeName) as [NSObject : AnyObject]
//        )
//        
//        label.setAttributedText(attributedText)
        
        

    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        // set items
        var pickerTipItems: [WKPickerItem]! = []

        for i in 0...50 {
            let pickerItem = WKPickerItem()
            pickerItem.title = "\(i)"
            pickerItem.caption = "\(i)"
            pickerTipItems.append(pickerItem)
        }
        
        // create animated images and picker items
        var images: [UIImage]! = []
        for (var i=0; i<=15; i++) {
            let name = "progress_circle-\(i)"
            images.append(UIImage(named: name)!)
        }
        let progressImages = UIImage.animatedImageWithImages(images, duration: 0.0)
        grpTipUI.setBackgroundImage(progressImages)

        
        pckTip.setCoordinatedAnimations([grpTipUI])
        pckTip.setItems(pickerTipItems)
        pckTip.focus()
        
        let tip = Int(retrieveSharedInfo("tip"))        
        pckTip.setSelectedItemIndex(tip == nil ? 15 : tip!)
        
        tipChanged = false


//        resetColors()

    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
        
        // update complication if tip % changed
        if tipChanged {
            let complicationServer = CLKComplicationServer.sharedInstance()
            for complication in complicationServer.activeComplications {
                complicationServer.reloadTimelineForComplication(complication)
            }
            
            tipChanged = false
            
//            print("watchapp - update complications")
        }
    }
    
    func resetColors() {
        btnTipPercent.setBackgroundColor(UIColor.clearColor())

    }
    


}
