//
//  PageItemViewController.swift
//  InactivityCoach
//
//  Created by ATserver on 10/26/15.
//  Copyright © 2015 Merielle Impreso. All rights reserved.
//

import UIKit

class PageItemViewController: UIViewController {
    var parentController: CoachSelectViewController?
    var itemIndex: Int = 0
    var name: String = "" {
        didSet {
            if let bg = imgBg {
                bg.image = UIImage(named: "coach_\(name)_bg.jpg")
            }
            if let desc = imgDesc {
                desc.image = UIImage(named: "coach_\(name)_desc.png")
            }
            if let top = imgTop {
                top.image = UIImage(named: "coach_\(name)_top.png")
            }
        }
    }
    
    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var imgTop: UIImageView!
    @IBOutlet weak var imgDesc: UIImageView!
    @IBOutlet weak var buttonSelect: UIButton!
    
    var isUnlockedMasterMaru = false
    var isUnlockedDrDoki = false
    var isUnlockedPrinceWilly = false

    override func viewDidLoad() {
        super.viewDidLoad()
    
        imgBg.image = UIImage(named: "coach_\(name)_bg.jpg")
        imgDesc.image = UIImage(named: "coach_\(name)_desc.png")
        imgTop.image = UIImage(named: "coach_\(name)_top.png")
        
        let selectedCoach = InactivityCoach.sharedInstance.getSharedInfo(kSelectedCoach) as! Int
        if selectedCoach == itemIndex {
            buttonSelect.setBackgroundImage(UIImage(named: "coach_button_selected.png"), forState: UIControlState.Normal)
        }
        
        isUnlockedMasterMaru = InactivityCoach.sharedInstance.getSharedInfo(kUnlockedMasterMaru) as! Bool
        isUnlockedDrDoki = InactivityCoach.sharedInstance.getSharedInfo(kUnlockedDrDoki) as! Bool
        isUnlockedPrinceWilly = InactivityCoach.sharedInstance.getSharedInfo(kUnlockedPrinceWilly) as! Bool
        
        if (itemIndex == kMasterMaruIndex) {
            if !isUnlockedMasterMaru {
                buttonSelect.setBackgroundImage(UIImage(named: "coach_button_locked.png"), forState: UIControlState.Normal)
            }
        }
        
        if (itemIndex == kDrDokiIndex) {
            if !isUnlockedDrDoki {
                buttonSelect.setBackgroundImage(UIImage(named: "coach_button_locked.png"), forState: UIControlState.Normal)
            }
        }
        
        if (itemIndex == kPrinceWillyIndex) {
            if !isUnlockedPrinceWilly {
                buttonSelect.setBackgroundImage(UIImage(named: "coach_button_locked.png"), forState: UIControlState.Normal)
            }
        }
    }
    
    @IBAction func clickedSelectButton(sender: AnyObject) {
        var canBeSelected = false
        
        if (itemIndex == kMasterMaruIndex) {
            if !isUnlockedMasterMaru {
                // TO DO: Implement app store for master maru
                print("buying master maru")
            } else {
                canBeSelected = true
            }
        }
        else if (itemIndex == kDrDokiIndex) {
            if !isUnlockedDrDoki {
                // TO DO: Implement app store for dr doki
                print("buying dr doki")
            } else {
                canBeSelected = true
            }
        }
        else if (itemIndex == kPrinceWillyIndex) {
            if !isUnlockedPrinceWilly {
                // TO DO: Implement app store for prince willy
                print("buying prince willy")
            } else {
                canBeSelected = true
            }
        } else {
            canBeSelected = true
        }
        
        if canBeSelected {
            InactivityCoach.sharedInstance.setSharedInfo(kSelectedCoach, value: itemIndex)
            parentController!.navigationController?.popToRootViewControllerAnimated(true)
        }

    }
    
}
