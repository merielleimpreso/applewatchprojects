//
//  ProgressView.swift
//  InactivityCoach
//
//  Created by ATserver on 10/29/15.
//  Copyright © 2015 Merielle Impreso. All rights reserved.
//

import UIKit
import Foundation

let PI:CGFloat = CGFloat(M_PI)
let arcWidth: CGFloat = 5

class ProgressView: UIView {
    var progress: Double =  0 {
        didSet {
            animateCircle(1.0)
        }
    }
    
    var circleLayer: CAShapeLayer!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clearColor()
        let bgColor = colorize(0x999999)
        
        let startAngleDegrees: Double = 90
        let endAngleDegrees: Double = 450
        
        let center = CGPoint(x:frame.width/2, y: frame.height/2)
        let radius: CGFloat = max(frame.width, frame.height)
        
        let startAngle: CGFloat =  CGFloat(degreesToRadians(startAngleDegrees))
        let endAngle: CGFloat = CGFloat(degreesToRadians(endAngleDegrees))
        
        let path = UIBezierPath(arcCenter: center,
            radius: radius/2 - arcWidth/2,
            startAngle: startAngle,
            endAngle: endAngle,
            clockwise: true)
        path.lineWidth = arcWidth
        
        let bgLayer = CAShapeLayer()
        bgLayer.path = path.CGPath
        bgLayer.lineWidth = arcWidth
        bgLayer.fillColor = UIColor.clearColor().CGColor
        bgLayer.strokeColor = bgColor.CGColor
        bgLayer.strokeEnd = 1.0
        layer.addSublayer(bgLayer)
        
        circleLayer = CAShapeLayer()
        circleLayer.path = path.CGPath
        circleLayer.lineWidth = arcWidth
        circleLayer.fillColor = UIColor.clearColor().CGColor
        circleLayer.strokeColor = UIColor.whiteColor().CGColor
        circleLayer.strokeEnd = 0.0
        layer.addSublayer(circleLayer)
    }
    
    func animateCircle(duration: NSTimeInterval) {
        // We want to animate the strokeEnd property of the circleLayer
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.duration = duration
        
        // Animate from 0 (no circle) to 1 (full circle)
        animation.fromValue = circleLayer.strokeEnd
        animation.toValue = CGFloat(progress)
        
        // Do a linear animation (i.e. the speed of the animation stays the same)
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        
        // Set the circleLayer's strokeEnd property to 1.0 now so that it's the
        // right value when the animation ends.
        circleLayer.strokeEnd = CGFloat(progress)
        
        // Do the actual animation
        circleLayer.addAnimation(animation, forKey: "animateCircle")
    }
    
    func colorize (hex: Int, alpha: Double = 1.0) -> UIColor {
        let red = Double((hex & 0xFF0000) >> 16) / 255.0
        let green = Double((hex & 0xFF00) >> 8) / 255.0
        let blue = Double((hex & 0xFF)) / 255.0
        let color: UIColor = UIColor( red: CGFloat(red), green: CGFloat(green), blue: CGFloat(blue), alpha:CGFloat(alpha) )
        return color
    }
    
    func degreesToRadians (value:Double) -> Double {
        return value * M_PI / 180.0
    }
    
    func radiansToDegrees (value:Double) -> Double {
        return value * 180.0 / M_PI
    }
}


