//
//  GoalWarningController.swift
//  WearHydrateMe
//
//  Created by Merielle Impreso on 6/10/15.
//  Copyright (c) 2015 Merielle Impreso. All rights reserved.
//

import WatchKit
import Foundation
import DataKit


class GoalWarningController: WKInterfaceController {
    var allGoals: [Goal] = []
    var goalToday: Goal?

    @IBAction func okButtonClicked() {
        var number = goalToday!.number as Int
        number += 1
        
        var g = self.goalToday
        g!.number = number
        g!.date = NSDate()
        
        goalToday = DataAccess.sharedInstance.updateGoalToday(g!)
        self.dismissController()
    }
    
    func getGoalToday() -> Goal? {
        var goalToday: Goal?
        allGoals = DataAccess.sharedInstance.getAllGoals()
        
        if (allGoals.count == 0) {
            goalToday = DataAccess.sharedInstance.insertGoal()
        } else {
            var latestGoal = allGoals.first!
            
            let formatter = NSDateFormatter()
            formatter.dateStyle = .ShortStyle
            let dateToday = formatter.stringFromDate(NSDate())
            let date = formatter.stringFromDate(latestGoal.date)
            
            if (date == dateToday) {
                goalToday = latestGoal
            } else {
                goalToday = DataAccess.sharedInstance.insertGoal()
            }
        }
        return goalToday
    }

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        super.willActivate()
        goalToday = getGoalToday()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
