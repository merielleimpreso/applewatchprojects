//
//  TrackDetailDeleteController.swift
//  WearHydrateMe
//
//  Created by Merielle Impreso on 5/26/15.
//  Copyright (c) 2015 Merielle Impreso. All rights reserved.
//

import WatchKit
import Foundation
import DataKit


class TrackDetailDeleteController: WKInterfaceController {
    @IBOutlet weak var dateLabel: WKInterfaceLabel!
    @IBOutlet weak var timeLabel: WKInterfaceLabel!
    
    var parentController: TrackDetailController? = nil
    var record: Record?
    
    @IBAction func deleteButtonClicked() {
        parentController!.shouldDelete = true
        self.dismissController()
    }
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        parentController = (context as! TrackDetailController)
        record = parentController!.selectedRecord
        
        let format = NSDateFormatter.dateFormatFromTemplate("dd MMM yyyy, EEE", options: 0, locale: NSLocale(localeIdentifier: "en-US"))
        let formatter = NSDateFormatter()
        formatter.dateFormat = format
        let date = formatter.stringFromDate(record!.timeStamp)
        dateLabel.setText(date)
        
        formatter.dateFormat = nil
        formatter.timeStyle = .MediumStyle
        let time = formatter.stringFromDate(record!.timeStamp)
        timeLabel.setText(time)
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
