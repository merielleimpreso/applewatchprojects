//
//  Formatter.swift
//  WearHydrateMe
//
//  Created by Merielle Impreso on 5/21/15.
//  Copyright (c) 2015 Merielle Impreso. All rights reserved.
//

import Foundation

class Formatter {
    
    class func zeroPad(number: Int) -> String {
        return (number < 10) ? "0\(number)" : "\(number)"
    }
}