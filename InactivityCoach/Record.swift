//
//  Record.swift
//  InactivityCoach
//
//  Created by Merielle Impreso on 11/26/15.
//  Copyright © 2015 Merielle Impreso. All rights reserved.
//

import Foundation
import CoreData
import UIKit


class Record: NSManagedObject {
    
    // Insert code here to add functionality to your managed object subclass
    
    class func getAllRecords() -> [Record] {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext!
        
        let fetchRecords = NSFetchRequest(entityName: "Record")
        fetchRecords.returnsObjectsAsFaults = false
        
        let sortDescriptor = NSSortDescriptor(key: "date", ascending: false)
        let sortDescriptors = [sortDescriptor]
        fetchRecords.sortDescriptors = sortDescriptors
        
        return ((try? managedContext.executeFetchRequest(fetchRecords)) as? [Record])!
    }
    
    
    class func insertRecord() -> Record {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext!
        
        let record =  NSEntityDescription.insertNewObjectForEntityForName("Record", inManagedObjectContext:managedContext) as! Record
        record.date = NSDate()
        record.progress = 0
        record.goal = getGoalStepsForToday()
        
        save(managedContext)
        return record
    }
    
    class func insertRecordWithData(recordData: Record) -> Record {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext!
        let record =  NSEntityDescription.insertNewObjectForEntityForName("Record", inManagedObjectContext:managedContext) as! Record
        record.date = NSDate()
        record.goal = recordData.goal
        record.progress = recordData.progress
        
        save(managedContext)
        return record
    }
    
    class func getGoalStepsForToday() -> Double {
        var intervalHour = InactivityCoach.sharedInstance.getSharedInfo(kIntervalHour) as! Int
        var intervalMinute = InactivityCoach.sharedInstance.getSharedInfo(kIntervalMinute) as! Int
        if intervalHour == 0 {
            if intervalMinute < kIntervalMinuteDefault {
                intervalHour = kIntervalHourDefault
                intervalMinute = kIntervalMinuteDefault
            }
        }
        let intervalTime = (intervalHour * 60) + intervalMinute
        let goal = Double((kMinutesForDailyProgress / intervalTime)) * kGoalStepsBetweenNotifications
        return goal
    }
    
    class func updateRecordToday(record: Record) -> Record {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext!
        
        let allRecords = getAllRecords()
        var recordToday: Record?
        
        let formatter = NSDateFormatter()
        formatter.dateStyle = .ShortStyle
        let dateToday = formatter.stringFromDate(NSDate())
        for r in allRecords {
            let date = formatter.stringFromDate(r.date!)
            if (date == dateToday) {
                managedContext.deleteObject(r)
                recordToday = insertRecordWithData(record)
                save(managedContext)
            }
        }
        
        return recordToday!
    }
    
    class func getRecordToday() -> Record {
        var recordToday: Record? = nil
        
        let allRecords = Record.getAllRecords()
        let formatter = NSDateFormatter()
        formatter.dateStyle = .ShortStyle
        let dateToday = formatter.stringFromDate(NSDate())
        for record in allRecords {
            let date = formatter.stringFromDate(record.date!)
            if (date == dateToday) {
                recordToday = record
                break
            }
        }
        if (recordToday == nil) {
            recordToday = Record.insertRecord()
        }
        
        return recordToday!
    }
    
    class func save(managedContext: NSManagedObjectContext) {
        do {
            try managedContext.save()
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
}
