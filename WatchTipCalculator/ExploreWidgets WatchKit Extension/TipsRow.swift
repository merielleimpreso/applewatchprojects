//
//  TipsRow.swift
//  WatchTipCalculator
//
//  Created by dev on 4/13/15.
//  Copyright (c) 2015 Aeus. All rights reserved.
//

import WatchKit

class TipsRow: NSObject {
    @IBOutlet weak var lblBillAmount: WKInterfaceLabel!
    @IBOutlet weak var lblTipPercentage: WKInterfaceLabel!
    @IBOutlet weak var lblTipAmount: WKInterfaceLabel!
    @IBOutlet weak var lblTotalAmount: WKInterfaceLabel!
    
}