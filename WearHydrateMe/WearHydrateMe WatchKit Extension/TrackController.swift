//
//  TrackController.swift
//  WearHydrateMe
//
//  Created by Merielle Impreso on 5/25/15.
//  Copyright (c) 2015 Merielle Impreso. All rights reserved.
//

import WatchKit
import Foundation
import DataKit


class TrackController: WKInterfaceController {
    @IBOutlet weak var table: WKInterfaceTable!
    
    let daysToBeDisplayed = 7
    
    var allRecords: [Record] = []
    var allGoals: [Goal] = []
    var allTracks: [Track] = []
    var selectedTrack: Track?
    var context: AnyObject?
    
    func loadTableData() {
        table.setNumberOfRows(daysToBeDisplayed, withRowType: "trackRow")
        
        allTracks = []
        
        for daysAgo in 0...daysToBeDisplayed-1 {
            let format = NSDateFormatter.dateFormatFromTemplate("MMM-dd", options: 0, locale: NSLocale(localeIdentifier: "en-US"))
            let formatter = NSDateFormatter()
            formatter.dateFormat = format

            let date = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: -daysAgo, toDate: NSDate(), options: [])!
            let records = getRecordsWithDate(date)
            let goal = getGoalWithDate(date)
            let dateString = formatter.stringFromDate(date)
            
            let row = table.rowControllerAtIndex(daysAgo) as! TrackRow
            row.dateLabel.setText(dateString)
            if (goal == nil) {
                row.trackLabel.setText("-/-")
            } else {
                row.trackLabel.setText("\(records.count)/\(goal!.number)")
            }
            
            let track = Track(records: records, goal: goal, date: date)
            allTracks.append(track)
        }
    }
    
    func getRecordsWithDate(specificDate: NSDate) -> [Record] {
        var records: [Record] = []
        let formatter = NSDateFormatter()
        formatter.dateStyle = .ShortStyle
        
        let selectedDate = formatter.stringFromDate(specificDate)
        for record in allRecords {
            let date = formatter.stringFromDate(record.timeStamp)
            if (date == selectedDate) {
                records.append(record)
            }
        }
        return records
    }
    
    func getGoalWithDate(specificDate: NSDate) -> Goal? {
        let formatter = NSDateFormatter()
        formatter.dateStyle = .ShortStyle
        
        let selectedDate = formatter.stringFromDate(specificDate)
        for g in allGoals {
            let date = formatter.stringFromDate(g.date)
            if (date == selectedDate) {
                return g
            }
        }
        return nil
    }
    
    override func table(table: WKInterfaceTable, didSelectRowAtIndex rowIndex: Int) {
        selectedTrack = allTracks[rowIndex]
        
        if (selectedTrack!.records!.count > 0) {
            presentControllerWithName("TrackDetail", context: self)
        }
    }
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        self.context = context
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        allRecords = DataAccess.sharedInstance.getAllRecords()
        allGoals = DataAccess.sharedInstance.getAllGoals()
        loadTableData()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
