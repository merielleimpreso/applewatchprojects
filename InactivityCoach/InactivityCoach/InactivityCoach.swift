//
//  InactivityCoach.swift
//  InactivityCoach
//
//  Created by ATserver on 10/22/15.
//  Copyright © 2015 Merielle Impreso. All rights reserved.
//
// Model for InactivityCoach, handles basic functions

import Foundation
import UIKit
import CoreMotion

let kCoachStateNormal = "normal"
let kCoachStateSleep = "sleep"
let kCoachStateAngry = "angry"
let kCoachStateHappy = "happy"

let kNotificationOn = "notification_on"
let kIntervalHour = "interval_hour"
let kIntervalMinute = "interval_minute"
let kSelectedCoach = "selected_coach"
let kLastStepCount = "last_step_count"
let kLastActiveTime = "last_active_time"
let kSessionProgress = "session_progress"
let kDayProgress = "day_progress"
let kCoachState = "coach_state"
let kUnlockedMasterMaru = "com.aeustech.inactivitycoach.mastermaru"
let kUnlockedDrDoki = "com.aeustech.inactivitycoach.drdoki"
let kUnlockedPrinceWilly = "com.aeustech.inactivitycoach.princewilly"

let kNotificationOnDefault = false
let kIntervalHourDefault = 0
let kIntervalMinuteDefault = 5

let kPedometerAvailable = CMPedometer.isStepCountingAvailable()
let kGoalStepsBetweenNotifications:Double = kPedometerAvailable ? goalStepsBetweenSessions : 500
let kGoalStepsPerDay:Double = 3000.0 // recommended goal is 10,000 steps over 12 hours
let kGoalStepsPerHour:Double = kGoalStepsPerDay / 12

let kMinutesForDailyProgress = 360

let kPudgyBirbIndex = 0
let kChipIndex = 1
let kMasterMaruIndex = 2
let kDrDokiIndex = 3
let kPrinceWillyIndex = 4

var goalStepsBetweenSessions: Double {
    get {
        // calculate seconds since last notification
        let intervalHour = InactivityCoach.sharedInstance.getSharedInfo(kIntervalHour) as! Int
        let intervalMinute = InactivityCoach.sharedInstance.getSharedInfo(kIntervalMinute) as! Int
        
        let intervalInHours = intervalHour + (intervalMinute/60)
        
        // goal steps depend on notification intervals (i.e. longer interval means more goal steps)
        return Double(intervalInHours) * kGoalStepsPerHour
    }
}


class InactivityCoach: NSObject, SOMotionDetectorDelegate {
    
    // Variables for CoreMotion
    let motionMgr = CMMotionManager()
    let pedometer = CMPedometer()
    let activityManager = CMMotionActivityManager()
        
    var status = ""
    var progress: Double = 0
    
    
    // Static instance for the model
    class var sharedInstance : InactivityCoach {
        struct Static {
            static var onceToken : dispatch_once_t = 0
            static var instance : InactivityCoach? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = InactivityCoach()
            
        }
        return Static.instance!
    }
    
    // Used in handling coach selection, filenames for images
    func getCoachNames() -> [String] {
        var coachNames: [String] = ["", "", "", "", ""]
        coachNames[kPudgyBirbIndex] = "pudgy"
        coachNames[kChipIndex] = "chip"
        coachNames[kMasterMaruIndex] = "maru"
        coachNames[kDrDokiIndex] = "doki"
        coachNames[kPrinceWillyIndex] = "willy"
        
        return coachNames
    }
    
    func getSharedInfo(key: String) -> AnyObject? {
        let sharedAppGroup: NSString = "group.com.aeustech.inactivitycoach"
        let defaults = NSUserDefaults(suiteName: sharedAppGroup as String)
        
        if key == kSelectedCoach || key == kIntervalHour || key == kIntervalMinute  {
            return defaults!.integerForKey(key)
        } else if key == kSessionProgress || key == kDayProgress {
            return defaults!.doubleForKey(key)
        } else if key == kNotificationOn || key == kUnlockedMasterMaru
            || key == kUnlockedPrinceWilly || key == kUnlockedDrDoki {
            return defaults!.boolForKey(key)
        } else if key == kLastActiveTime || key == kCoachState {
            return defaults?.objectForKey(key)
        } else {
            return nil
        }
    }
    
    func setSharedInfo(key: String, value: AnyObject) {
        let sharedAppGroup: NSString = "group.com.aeustech.inactivitycoach"
        let defaults = NSUserDefaults(suiteName: sharedAppGroup as String)
        
        if key == kSelectedCoach || key == kIntervalHour || key == kIntervalMinute  {
            defaults!.setInteger(value as! Int, forKey: key)
        } else if key == kSessionProgress || key == kDayProgress {
            defaults!.setDouble(value as! Double, forKey: key)
        } else if key == kNotificationOn || key == kUnlockedMasterMaru
            || key == kUnlockedPrinceWilly || key == kUnlockedDrDoki {
            defaults!.setBool(value as! Bool, forKey: key)
        } else {
            defaults!.setObject(value, forKey: key)
        }
        defaults!.synchronize()
    }
    
    func getIntervalTimeDate() -> NSDate {
        var hour = getSharedInfo(kIntervalHour) as! Int
        var minute = getSharedInfo(kIntervalMinute) as! Int
        
        if hour == 0 {
            if minute < kIntervalMinuteDefault {
                hour = kIntervalHourDefault
                minute = kIntervalMinuteDefault
            }
        }
        
        let cal = NSCalendar.currentCalendar()
        let comps = NSDateComponents()
        comps.hour = hour
        comps.minute = minute
        let date = cal.dateFromComponents(comps)
        
        return date!
    }
    
    func getMinimumTimeDate() -> NSDate {
        let cal = NSCalendar.currentCalendar()
        let comps = NSDateComponents()
        comps.hour = 0
        comps.minute = kIntervalMinuteDefault
        let date = cal.dateFromComponents(comps)
        
        return date!
    }
    
    func turnOnNotification(notification_on: Bool) {
        if notification_on {
            let calendar = NSCalendar.currentCalendar()
            let components = NSDateComponents()
            components.day = 1
            
            let date1 = NSDate()
            let date2 = calendar.dateByAddingComponents(components, toDate: date1, options: [])
            
            let intervalOfDates = Int(date2!.timeIntervalSinceDate(date1))
            
            var intervalHour = getSharedInfo(kIntervalHour) as! Int
            var intervalMinute = getSharedInfo(kIntervalMinute) as! Int
            if intervalHour == 0 {
                if intervalMinute < kIntervalMinuteDefault {
                    intervalHour = kIntervalHourDefault
                    intervalMinute = kIntervalMinuteDefault
                }
            }
            
            let intervalOfNotification = (intervalHour * 3600) + (intervalMinute * 60)
            
            let alarmCount = intervalOfDates / intervalOfNotification
            
            for i in 1...alarmCount {
                let secondsToAdd = i * intervalOfNotification
                let components = NSDateComponents()
                components.second = secondsToAdd
                let date = calendar.dateByAddingComponents(components, toDate: date1, options: [])
                
                let notification = UILocalNotification()
                notification.fireDate = date
                notification.alertBody = "Move! Move! Move!"
                notification.timeZone = NSTimeZone.defaultTimeZone()
                notification.applicationIconBadgeNumber = 1
                
                notification.repeatInterval = .Day
                notification.category = "INVITE_CATEGORY"
                notification.repeatInterval = .Day
                
                UIApplication.sharedApplication().scheduleLocalNotification(notification)
                
                let formatter = NSDateFormatter()
                formatter.dateStyle = .MediumStyle
                formatter.timeStyle = .MediumStyle
                log("\(i) = \(formatter.stringFromDate(date!))")
            }
        } else {
            UIApplication.sharedApplication().cancelAllLocalNotifications()
        }
    }
    
    func log(string: String) {
        print(string)
        
    }
    
}