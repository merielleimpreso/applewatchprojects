//
//  AlarmViewController.swift
//  WearHydrateMe
//
//  Created by Merielle Impreso on 6/2/15.
//  Copyright (c) 2015 Merielle Impreso. All rights reserved.
//

import UIKit
import DataKit

class AlarmViewController: UITableViewController {
    @IBOutlet weak var alarmIntervalLabel: UILabel!
    @IBOutlet weak var activeFromLabel: UILabel!
    @IBOutlet weak var activeToLabel: UILabel!
    @IBOutlet weak var alarmToggle: UISwitch!
    
    var alarmOn: Bool = false
    var alarmHour: Int = 0
    var alarmMinute: Int = 30
    var activeFromHour: Int = 0
    var activeFromMinute: Int = 0
    var activeFromMiridiem: String = ""
    var activeToHour: Int = 0
    var activeToMinute: Int = 0
    var activeToMiridiem: String = ""

    @IBAction func alarmToggled(sender: AnyObject) {
        var toggle = sender as! UISwitch
        if (toggle.on == true) {
            let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
            dispatch_async(dispatch_get_global_queue(priority, 0)) {
                DataAccess.sharedInstance.sendNotification()
            }
        } else {
            DataAccess.sharedInstance.cancelNotification()
        }
        DataAccess.sharedInstance.updateAlarmOn(toggle.on)
    }
    
    func getAlarmIntervalLabelText() -> String {
        var label = ""
        if (alarmHour > 0) {
            label = "\(alarmHour)"
            if (alarmHour > 1) {
                label += "hrs"
            } else {
                label += "hr"
            }
            
            if (alarmMinute > 0) {
                label += ","
            }
        }
        if (alarmMinute > 0) {
            label += "\(alarmMinute)"
            if (alarmMinute > 1) {
                label += "mins"
            } else {
                label += "min"
            }
        }
        return label
    }
    
    func getDate(hour: Int, minute: Int) -> NSDate {
        let cal = NSCalendar.currentCalendar()
        let comps = NSDateComponents()
        comps.hour = hour
        comps.minute = minute
        let date = cal.dateFromComponents(comps)
        
        return date!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        alarmOn = DataAccess.sharedInstance.getAlarmOn()
        alarmToggle.setOn(alarmOn, animated: true)
        
        alarmHour = DataAccess.sharedInstance.getAlarmHour()
        alarmMinute = DataAccess.sharedInstance.getAlarmMinute()
        if (alarmHour == 0 && alarmMinute == 0) {
            alarmMinute = 30
            DataAccess.sharedInstance.updateAlarmMinute(30)
        }
        alarmIntervalLabel.text = getAlarmIntervalLabelText()
        
        
        let formatter = NSDateFormatter()
        formatter.timeStyle = .ShortStyle
        
        activeFromHour = DataAccess.sharedInstance.getActiveFromHour()
        activeFromMinute = DataAccess.sharedInstance.getActiveFromMinute()
        
        var date = getDate(activeFromHour, minute: activeFromMinute)
        activeFromLabel.text = formatter.stringFromDate(date)
        
        activeToHour = DataAccess.sharedInstance.getActiveToHour()
        activeToMinute = DataAccess.sharedInstance.getActiveToMinute()
        
        date = getDate(activeToHour, minute: activeToMinute)
        activeToLabel.text = formatter.stringFromDate(date)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "AlarmInterval" {
            let vc = segue.destinationViewController as! AlarmIntervalViewController
            let date = getDate(alarmHour, minute: alarmMinute)
            vc.currentIntervalDate = date
            vc.parentController = self
        } else {
            let vc = segue.destinationViewController as! ActiveViewController
    
            if segue.identifier == "ActiveFrom" {
                let date = getDate(activeFromHour, minute: activeFromMinute)
                vc.currentTime = date
                vc.title = "Active From"
            } else {
                let date = getDate(activeToHour, minute: activeToMinute)
                vc.currentTime = date
                vc.title = "Active To"
            }
            vc.type = segue.identifier!
            vc.parentController = self
        }
    }
}
