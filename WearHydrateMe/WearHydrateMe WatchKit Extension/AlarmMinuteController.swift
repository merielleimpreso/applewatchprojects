//
//  AlarmMinuteController.swift
//  WearHydrateMe
//
//  Created by Merielle Impreso on 5/21/15.
//  Copyright (c) 2015 Merielle Impreso. All rights reserved.
//

import WatchKit
import Foundation
import DataKit


class AlarmMinuteController: WKInterfaceController {
    @IBOutlet weak var minuteButton: WKInterfaceButton!
    @IBOutlet weak var minuteSlider: WKInterfaceSlider!
    
    var parentController: AlarmController? = nil
    var minute: Int = 0

    @IBAction func minuteSliderValueChanged(value: Float) {
        minute = Int(value)
        minuteButton.setTitle(Formatter.zeroPad(minute))
        minuteSlider.setValue(value)
    }
    
    @IBAction func okButtonClicked() {
        parentController!.minute = minute
        DataAccess.sharedInstance.updateAlarmMinute(minute)
        self.dismissController()
    }
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        parentController = (context as! AlarmController)
        minute = parentController!.minute
        minuteButton.setTitle(Formatter.zeroPad(minute))
        minuteSlider.setValue(Float(minute))
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
