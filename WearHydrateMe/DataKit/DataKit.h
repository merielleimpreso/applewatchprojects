//
//  DataKit.h
//  DataKit
//
//  Created by Merielle Impreso on 5/20/15.
//  Copyright (c) 2015 Merielle Impreso. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for DataKit.
FOUNDATION_EXPORT double DataKitVersionNumber;

//! Project version string for DataKit.
FOUNDATION_EXPORT const unsigned char DataKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <DataKit/PublicHeader.h>


