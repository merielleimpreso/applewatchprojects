//
//  TrackDetailViewController.swift
//  WearHydrateMe
//
//  Created by Merielle Impreso on 5/27/15.
//  Copyright (c) 2015 Merielle Impreso. All rights reserved.
//

import UIKit
import DataKit

class TrackDetailViewController: UITableViewController {
    var track: Track?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.setEditing(true, animated:true)

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return track!.records!.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) 

        let r = track?.records![indexPath.row]
        let formatter = NSDateFormatter()
        formatter.timeStyle = .MediumStyle
        let date = formatter.stringFromDate(r!.timeStamp)
        
        cell.textLabel?.text = date
        return cell
    }
    
    override func setEditing(editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            let r = track?.records![indexPath.row]
            DataAccess.sharedInstance.deleteRecord(r!)
            
            track?.records?.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
        }
    }
}
