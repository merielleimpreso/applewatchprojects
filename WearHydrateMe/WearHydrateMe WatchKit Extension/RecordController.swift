//
//  InterfaceController.swift
//  WearHydrateMe WatchKit Extension
//
//  Created by Merielle Impreso on 5/20/15.
//  Copyright (c) 2015 Merielle Impreso. All rights reserved.
//

import WatchKit
import Foundation
import DataKit


class RecordController: WKInterfaceController {

    @IBOutlet weak var recordButton: WKInterfaceButton!
    
    var allRecords: [Record] = []
    var allGoals: [Goal] = []
    var recordsToday: [Record] = []
    var goalToday: Goal?
    
    @IBAction func recordButtonClicked() {
        var record = DataAccess.sharedInstance.insertRecord()
        allRecords.append(record)
        recordsToday.append(record)
        recordButton.setTitle("\(recordsToday.count)")
        
        if (recordsToday.count >= (goalToday!.number as Int)) {
            let image = UIImage(named:"main_drink_button_finished")
            recordButton.setBackgroundImage(image)
        } else {
            let image = UIImage(named:"main_drink_button")
            recordButton.setBackgroundImage(image)
        }
    }
    
    func getRecordsToday() -> [Record] {
        var recordsToday: [Record] = []
        let formatter = NSDateFormatter()
        formatter.dateStyle = .ShortStyle
        let dateToday = formatter.stringFromDate(NSDate())
        
        allRecords = DataAccess.sharedInstance.getAllRecords()
        for record in allRecords {
            let date = formatter.stringFromDate(record.timeStamp)
            if (date == dateToday) {
                recordsToday.append(record)
            }
        }
        return recordsToday
    }
    
    func getGoalToday() -> Goal? {
        var goalToday: Goal?
        allGoals = DataAccess.sharedInstance.getAllGoals()
        
        if (allGoals.count == 0) {
            goalToday = DataAccess.sharedInstance.insertGoal()
        } else {
            var latestGoal = allGoals.first!
            
            let formatter = NSDateFormatter()
            formatter.dateStyle = .ShortStyle
            let dateToday = formatter.stringFromDate(NSDate())
            let date = formatter.stringFromDate(latestGoal.date)
            
            if (date == dateToday) {
                goalToday = latestGoal
            } else {
                goalToday = DataAccess.sharedInstance.insertGoal()
            }
        }
        return goalToday
    }
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        recordsToday = getRecordsToday()
        goalToday = getGoalToday()
        
        recordButton.setTitle("\(recordsToday.count)")
        if (recordsToday.count >= (goalToday!.number as Int)) {
            let image = UIImage(named:"main_drink_button_finished")
            recordButton.setBackgroundImage(image)
        } else {
            let image = UIImage(named:"main_drink_button")
            recordButton.setBackgroundImage(image)
        }
    }
    
    override func handleActionWithIdentifier(identifier: String?, forLocalNotification localNotification: UILocalNotification) {
        if identifier == "Drink" {
            recordsToday = getRecordsToday()
            goalToday = getGoalToday()
            
            var record = DataAccess.sharedInstance.insertRecord()
            allRecords.append(record)
            recordsToday.append(record)
            recordButton.setTitle("\(recordsToday.count)")
            
            if (recordsToday.count >= (goalToday!.number as Int)) {
                let image = UIImage(named:"main_drink_button_finished")
                recordButton.setBackgroundImage(image)
            } else {
                let image = UIImage(named:"main_drink_button")
                recordButton.setBackgroundImage(image)
            }
        }
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
