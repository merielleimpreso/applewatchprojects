//
//  GlanceController.swift
//  InactivitityCoach Extension
//
//  Created by Merielle Impreso on 10/1/15.
//  Copyright © 2015 Merielle Impreso. All rights reserved.
//

import WatchKit
import Foundation


class GlanceInterfaceController: WKInterfaceController {
    @IBOutlet var coachImage: WKInterfaceImage!
    @IBOutlet var progressImage: WKInterfaceImage!
    @IBOutlet var dayProgressImage: WKInterfaceImage!
    
    let coachNames = WatchInactivityCoach.sharedInstance.getCoachNames()
    
    //TO DO: Change value
    //let sessionGoal = 100.0
    //let dayGoal = 100.0
    
    var coachState: String? {
        didSet {
            animateCoachImage(coachState!)
        }
    }
    
    var notificationOn: Bool? {
        didSet {
            if notificationOn! {
                coachState = kCoachStateNormal
            } else {
                coachState = kCoachStateSleep
            }
        }
    }
    
    var progress: Double = 0 {
        didSet {
            if let image = progressImage {
                if (progress) > 0.8 {
                    image.setImageNamed("meter_5")
                } else if (progress) > 0.6 {
                    image.setImageNamed("meter_4")
                } else if (progress) > 0.4 {
                    image.setImageNamed("meter_3")
                } else if (progress) > 0.2 {
                    image.setImageNamed("meter_2")
                } else if (progress) > 0 {
                    image.setImageNamed("meter_1")
                } else {
                    image.setImageNamed("meter_0")
                }
            }
        }
    }
    
    var dayProgress: Double = 0 {
        didSet {
            if let image = dayProgressImage {
                if (dayProgress) > 0.8 {
                    image.setImageNamed("day_meter_5")
                } else if (dayProgress) > 0.6 {
                    image.setImageNamed("day_meter_4")
                } else if (dayProgress) > 0.4 {
                    image.setImageNamed("day_meter_3")
                } else if (dayProgress) > 0.2 {
                    image.setImageNamed("day_meter_2")
                } else if (dayProgress) > 0 {
                    image.setImageNamed("day_meter_1")
                } else {
                    image.setImageNamed("day_meter_0")
                }
            }
        }
    }

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        super.willActivate()
        
        progress = WatchInactivityCoach.sharedInstance.getSharedInfo(kSessionProgress) as! Double
        dayProgress = WatchInactivityCoach.sharedInstance.getSharedInfo(kDayProgress) as! Double
        
        if let state = WatchInactivityCoach.sharedInstance.getSharedInfo(kCoachState) as? String {
            coachState = state
        } else {
            coachState = kCoachStateNormal
        }
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    func animateCoachImage(state: String) {
        let selected = WatchInactivityCoach.sharedInstance.getSharedInfo(kSelectedCoach) as! Int
        let selectedName = coachNames[selected]
        let imageName = "w_coach_\(selectedName)_\(state)"
        
        coachImage.setImageNamed(imageName)
        coachImage.startAnimatingWithImagesInRange(NSRange(location: 0, length: 2), duration: 1, repeatCount: Int.max)
    }
}
