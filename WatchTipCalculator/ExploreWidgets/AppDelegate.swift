//
//  AppDelegate.swift
//  ExploreWidgets
//
//  Created by Lito Ang on 12/3/14.
//  Copyright (c) 2014 Aeus. All rights reserved.
//


import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func processRequest(request:NSDictionary) -> NSDictionary {
        var dictionary: NSDictionary

        let requestValue = request["request"] as! String
//        print("request = \(requestValue)")
        
        dictionary = ["response":"no"]

        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        if let unlocked_split = defaults.objectForKey("unlocked_split") as? Bool
        {
//            print("unlocked_split = \(unlocked_split)")
            
            if unlocked_split {
                dictionary = ["response":"yes"]
            }
        }
        
        return dictionary
    }
    
    func application(application: UIApplication, handleWatchKitExtensionRequest userInfo: [NSObject : AnyObject]?, reply: (([NSObject : AnyObject]?) -> Void)) {
        reply(processRequest(userInfo!) as [NSObject : AnyObject])
        
        
//        var rootViewController = self.window!.rootViewController
//        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        var setViewController = mainStoryboard.instantiateViewControllerWithIdentifier("Main") as ViewController
//        
//        rootViewController?.navigationController?.popToViewController(setViewController, animated: false)
        
//        rootViewController.navigationController.popToViewController(setViewController, animated: false)
//        setViewController.reloadData()
        
//        UIApplication().openURL(NSURL(string:"myappscheme://open")!)
    }


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

