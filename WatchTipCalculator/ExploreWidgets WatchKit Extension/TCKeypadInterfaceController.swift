//
//  InterfaceController.swift
//  ExploreWidgets WatchKit Extension
//
//  Created by Lito Ang on 1/22/15.
//  Copyright (c) 2015 Aeus. All rights reserved.
//

import WatchKit
import Foundation


class TCKeypadInterfaceController: WKInterfaceController {
    
    @IBOutlet weak var btnTipPercent: WKInterfaceButton!
    @IBOutlet weak var lblPay: WKInterfaceLabel!
    @IBOutlet weak var lblTipAmt: WKInterfaceLabel!
    @IBOutlet weak var mainGroup: WKInterfaceGroup!
    @IBOutlet weak var keypadGroup: WKInterfaceGroup!
    @IBOutlet weak var lblAmount: WKInterfaceLabel!
    @IBOutlet weak var btnBill: WKInterfaceButton!
    @IBOutlet weak var lblInputType: WKInterfaceLabel!
    @IBOutlet weak var grpMain: WKInterfaceGroup!
    
    @IBOutlet weak var btnDecimal: WKInterfaceButton!
    
    var billDollar:Int = 0
    var billCents:Int = 0
    
    var bill:Float = 0
    var tipPercent:Float = 0
    var amountEntered:String = "0"
    var separator:String = "."
    
    let formatNumber = NSNumberFormatter()
    
    var delegate:InterfaceControllerDelegate?
//    var model:ContextModel!
    
    func handleButton(button:String) {
//        println("button \(button)")
        
        if amountEntered == "0" {
            amountEntered = ""
        }
        
        // don't accept decimal point twice
        if button == separator && amountEntered.rangeOfString(separator) != nil {
            return
        }
        
        // allow only up to 6 digits so text won't overflow
        if (amountEntered as NSString).length > 5 {
            return
        }
        
        // allow only up to 3 digits before the decimal point
        if button != separator && (amountEntered as NSString).length > 2 && amountEntered.rangeOfString(separator) == nil {
            return
        }
        
        if button != "ok" {
            amountEntered += button
            
            lblAmount.setText(amountEntered)
        }
    }
    
    @IBAction func btnAllClearAction() {
        lblAmount.setText("0")
        amountEntered = "0"
    }
    

    @IBAction func btnClearAction() {
        
        let range = Range<String.Index>(start: amountEntered.startIndex, end: amountEntered.startIndex.advancedBy(amountEntered.characters.count-1))
        let newString = amountEntered.substringWithRange(range)
        
        lblAmount.setText(newString)
        amountEntered = newString
    }
    
    @IBAction func btnOneAction() {
        handleButton("1")
    }
    
    @IBAction func btnTwoAction() {
        handleButton("2")
    }
    
    @IBAction func btnThreeAction() {
        handleButton("3")
    }
    
    @IBAction func btnFourAction() {
        handleButton("4")
    }
    
    @IBAction func btnFiveAction() {
        handleButton("5")
    }
    
    @IBAction func btnSixAction() {
        handleButton("6")
    }
    
    @IBAction func btnSevenAction() {
        handleButton("7")
    }
    
    @IBAction func btnEightAction() {
        handleButton("8")
    }
    
    @IBAction func btnNineAction() {
        handleButton("9")
    }
    
    @IBAction func btnZeroAction() {
        handleButton("0")
    }
    
    @IBAction func btnPeriodAction() {
        handleButton(separator)
    }
    
    @IBAction func btnOkAction() {
//        println("ok pressed")
        
        
//        self.bill = (self.amountEntered as NSString).floatValue
        
        delegate?.didEnterAmount(self.amountEntered.stringByReplacingOccurrencesOfString(",", withString: ".", options: NSStringCompareOptions.LiteralSearch, range: nil))
        
        dismissController()

        
//        self.controller.amountEntered = self.formatNumber.stringFromNumber(self.bill)!
//        self.controller.updateBill(self.controller.amountEntered)
//        println(self.controller.amountEntered)
        
//        if selectedButton == btnBill {
//            bill = (amountEntered as NSString).floatValue
//            selectedButton.setTitle(formatNumber.stringFromNumber(bill))
//            
//        } else if selectedButton == btnTipPercent {
//            tipPercent = (amountEntered as NSString).floatValue
//            selectedButton.setTitle(amountEntered)
//            
//        }
//        
//        
//        let tip = bill * (tipPercent/100)
//        let pay = bill + tip
//        
//        
//        println("bill = \(bill)")
//        println("tip = \(tip)")
//        println("pay = \(pay)")
//        
//        lblPay.setText(formatNumber.stringFromNumber(pay))
//        lblTipAmt.setText(formatNumber.stringFromNumber(tip))
//        
//        mainGroup.setHidden(false);
//        keypadGroup.setHidden(true);
    }
    
    @IBAction func billOpenKeypadAction() {
        selectedButton = btnBill
        
        lblAmount.setText("0")
        amountEntered = ""
        
        mainGroup.setHidden(true);
        keypadGroup.setHidden(false);
    }
    
    @IBAction func tipOpenKeypadAction() {
        selectedButton = btnTipPercent
        
        lblAmount.setText("0")
        amountEntered = ""
        
        mainGroup.setHidden(true);
        keypadGroup.setHidden(false);
    }
    
    var selectedButton:WKInterfaceButton!
    
    func storeSharedInfo(key:String, value:String) {
        
        let appGroupID = "group.com.aeustech.watchtipcalculator"
        let defaults = NSUserDefaults(suiteName: appGroupID)
        
        defaults!.setObject(value, forKey: key)
        
        defaults!.synchronize()
        
    }
    
    func retrieveSharedInfo(key:String) -> String {
        
        let appGroupID = "group.com.aeustech.watchtipcalculator"
        let defaults = NSUserDefaults(suiteName: appGroupID)
        
        defaults!.synchronize()
        
        if let value = defaults!.objectForKey(key) as? String {
            return value
        } else {
            return ""
        }
        
    }

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
//        mainGroup.setHidden(false)
//        keypadGroup.setHidden(true)
        
        
        let controllerContext = (context as! ControllerContext)
        delegate = controllerContext.delegate
//        model = controllerContext.object
        
//        println("numpad segue = \(delegate?.numPadSegue)")
        
//        if delegate?.numPadSegue == "seguebill" {
////            lblInputType.setText("BILL AMOUNT")
//            keypadGroup.setBackgroundImage(UIImage(named: "bg_kp_bill"))
//        } else if delegate?.numPadSegue == "seguetip" {
////            lblInputType.setText("TIP PERCENTAGE")
//            keypadGroup.setBackgroundImage(UIImage(named: "bg_kp_tip"))
//        } else if delegate?.numPadSegue == "seguesplit" {
////            lblInputType.setText("PEOPLE")
//            keypadGroup.setBackgroundImage(UIImage(named: "bg_kp_split"))
//        }
        
        formatNumber.numberStyle = NSNumberFormatterStyle.DecimalStyle
        
        let locale: AnyObject = NSLocale.currentLocale()
        separator = locale.objectForKey(NSLocaleDecimalSeparator) as! String
        btnDecimal.setTitle(separator)
        
//        formatNumber.locale = NSLocale(localeIdentifier: "en_US")
//        btnBill.setTitle(formatNumber.stringFromNumber(0))
//        btnTipPercent.setTitle("0")
//        lblPay.setText(formatNumber.stringFromNumber(0))
//        lblTipAmt.setText(formatNumber.stringFromNumber(0))
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        //TEST ONLY
//        separator = ","
        
//        resetColors()

    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    func resetColors() {
        btnTipPercent.setBackgroundColor(UIColor.clearColor())

    }
    

    


}
