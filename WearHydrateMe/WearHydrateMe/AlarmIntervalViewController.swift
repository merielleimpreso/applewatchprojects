//
//  ViewController.swift
//  WearHydrateMe
//
//  Created by Merielle Impreso on 5/20/15.
//  Copyright (c) 2015 Merielle Impreso. All rights reserved.
//

import UIKit
import DataKit

class AlarmIntervalViewController: UIViewController {
    @IBOutlet weak var picker: UIDatePicker!
    var currentIntervalDate: NSDate?
    var parentController: AlarmViewController?
    
    func saveChangeSettings() {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "HH:mm"
        let stringFromDate = formatter.stringFromDate(self.picker.date)
        let stringFtapertromDateArr = stringFromDate.componentsSeparatedByString(":")
        let hour = stringFromDateArr[0].toInt()
        let min = stringFromDateArr[1].toInt()
        
        DataAccess.sharedInstance.updateAlarmHour(hour!)
        DataAccess.sharedInstance.updateAlarmMinute(min!)
        
        DataAccess.sharedInstance.updateAlarmOn(false)
        DataAccess.sharedInstance.cancelNotification()
        
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    @IBAction func saveButtonClicked(sender: AnyObject) {
        let alarm = DataAccess.sharedInstance.getAlarmOn()
        if (alarm) {
            var alert = UIAlertController(title: "Hydrate Me",
                message: "Alarm will be turned off because your settings was changed. Continue?",
                preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler:nil))
            alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { action in
                self.saveChangeSettings()
            }))
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            saveChangeSettings()
        }
    }
    
    @IBAction func cancelButtonClicked(sender: AnyObject) {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.setDate(currentIntervalDate!, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

