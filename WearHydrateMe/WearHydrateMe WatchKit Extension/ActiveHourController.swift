//
//  ActiveHourController.swift
//  WearHydrateMe
//
//  Created by Merielle Impreso on 5/21/15.
//  Copyright (c) 2015 Merielle Impreso. All rights reserved.
//

import WatchKit
import Foundation
import DataKit


class ActiveHourController: WKInterfaceController {
    @IBOutlet weak var titleLabel: WKInterfaceLabel!
    @IBOutlet weak var hourButton: WKInterfaceButton!
    @IBOutlet weak var hourSlider: WKInterfaceSlider!
    
    var parentController: ActiveController? = nil
    var segueIdentifier: String = ""
    var hour: Int = 0

    @IBAction func hourSliderValueChanged(value: Float) {
        hour = Int(value)
        hourButton.setTitle(Formatter.zeroPad(hour))
        hourSlider.setValue(value)
    }
    
    @IBAction func okButtonClicked() {
        if segueIdentifier == "activeFromHour" {
            parentController!.fromHour = hour
            DataAccess.sharedInstance.updateActiveFromHour(hour)
        } else {
            parentController!.toHour = hour
            DataAccess.sharedInstance.updateActiveToHour(hour)
        }
        self.dismissController()
    }
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        let data = context as! NSDictionary
        parentController = (data.objectForKey("parentController") as! ActiveController)
        segueIdentifier = data.objectForKey("segueIdentifier") as! String
        
        if segueIdentifier == "activeFromHour" {
            hour = parentController!.fromHour
            titleLabel.setText("Active From: HOUR")
        } else {
            hour = parentController!.toHour
            titleLabel.setText("Active To: HOUR")
        }
        hourButton.setTitle(Formatter.zeroPad(hour))
        hourSlider.setValue(Float(hour))
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
