//
//  ComplicationController.swift
//  InactivitityCoach Extension
//
//  Created by Merielle Impreso on 10/1/15.
//  Copyright © 2015 Merielle Impreso. All rights reserved.
//

import ClockKit


class ComplicationController: NSObject, CLKComplicationDataSource {
    
    // MARK: - Timeline Configuration
    
    func getSupportedTimeTravelDirectionsForComplication(complication: CLKComplication, withHandler handler: (CLKComplicationTimeTravelDirections) -> Void) {
        handler([.Forward, .Backward])
    }
    
    func getTimelineStartDateForComplication(complication: CLKComplication, withHandler handler: (NSDate?) -> Void) {
        handler(nil)
    }
    
    func getTimelineEndDateForComplication(complication: CLKComplication, withHandler handler: (NSDate?) -> Void) {
        handler(nil)
    }
    
    func getPrivacyBehaviorForComplication(complication: CLKComplication, withHandler handler: (CLKComplicationPrivacyBehavior) -> Void) {
        handler(.ShowOnLockScreen)
    }
    
    // MARK: - Timeline Population
    
    func getCurrentTimelineEntryForComplication(complication: CLKComplication, withHandler handler: ((CLKComplicationTimelineEntry?) -> Void)) {

        var entry = CLKComplicationTimelineEntry?()
        
        switch complication.family {
            case .ModularSmall:
                let progress = "80%"
                let template = CLKComplicationTemplateModularSmallStackImage()
                template.line1ImageProvider = CLKImageProvider(onePieceImage: getCoachImage())
                template.line2TextProvider = CLKSimpleTextProvider(text: progress)
                entry = CLKComplicationTimelineEntry(date: NSDate(), complicationTemplate: template)
                break
            
            case .ModularLarge:
                let active = "Active @ 12:59PM"
                let progress = "Progress: 100%"
                let template = CLKComplicationTemplateModularLargeStandardBody()
                template.headerTextProvider = CLKSimpleTextProvider(text: "Inactivity Coach")
                template.body1TextProvider = CLKSimpleTextProvider(text: active)
                template.body2TextProvider = CLKSimpleTextProvider(text: progress)
                entry = CLKComplicationTimelineEntry(date: NSDate(), complicationTemplate: template)
                break
            
            case .UtilitarianSmall:
                let progress = "80%"
                let template = CLKComplicationTemplateUtilitarianSmallFlat()
                template.imageProvider = CLKImageProvider(onePieceImage: getCoachImage())
                template.textProvider = CLKSimpleTextProvider(text: progress)
                entry = CLKComplicationTimelineEntry(date: NSDate(), complicationTemplate: template)
                break
            
            case .UtilitarianLarge:
                let active = "100% ACTIVE @ 12:59PM"
                let template = CLKComplicationTemplateUtilitarianLargeFlat()
                template.textProvider = CLKSimpleTextProvider(text: active)
                entry = CLKComplicationTimelineEntry(date: NSDate(), complicationTemplate: template)
                break
            
            case .CircularSmall:
                let progress = Float(8) / 10.0
                let template = CLKComplicationTemplateCircularSmallRingImage()
                template.imageProvider = CLKImageProvider(onePieceImage: getCoachImage())
                template.fillFraction = progress
                template.ringStyle = CLKComplicationRingStyle.Closed
                entry = CLKComplicationTimelineEntry(date: NSDate(), complicationTemplate: template)
            
                break
        }
        handler(entry)
    }
    
    func getTimelineEntriesForComplication(complication: CLKComplication, beforeDate date: NSDate, limit: Int, withHandler handler: (([CLKComplicationTimelineEntry]?) -> Void)) {
        // Call the handler with the timeline entries prior to the given date
        handler(nil)
    }
    
    func getTimelineEntriesForComplication(complication: CLKComplication, afterDate date: NSDate, limit: Int, withHandler handler: (([CLKComplicationTimelineEntry]?) -> Void)) {
        // Call the handler with the timeline entries after to the given date
        handler(nil)
    }
    
    // MARK: - Update Scheduling
    
    func getNextRequestedUpdateDateWithHandler(handler: (NSDate?) -> Void) {
        // Call the handler with the date when you would next like to be given the opportunity to update your complication content
        // Update hourly
        handler(NSDate(timeIntervalSinceNow: 60*60))
    }
    
    // MARK: - Placeholder Templates
    
    func getPlaceholderTemplateForComplication(complication: CLKComplication, withHandler handler: (CLKComplicationTemplate?) -> Void) {
        var template: CLKComplicationTemplate? = nil
        switch complication.family {
        case .ModularSmall:
            let modularTemplate = CLKComplicationTemplateModularSmallStackImage()
            modularTemplate.line1ImageProvider = CLKImageProvider(onePieceImage: getCoachImage())
            modularTemplate.line2TextProvider = CLKSimpleTextProvider(text: "0%")
            template = modularTemplate
            break
            
        case .ModularLarge:
            let modularTemplate = CLKComplicationTemplateModularLargeStandardBody()
            modularTemplate.headerImageProvider = CLKImageProvider(onePieceImage: getCoachImage())
            modularTemplate.headerTextProvider = CLKSimpleTextProvider(text: "Inactivity Coach")
            modularTemplate.body1TextProvider = CLKSimpleTextProvider(text: "No session")
            modularTemplate.body2TextProvider = CLKSimpleTextProvider(text: "0%")
            template = modularTemplate
            break
            
        case .UtilitarianSmall:
            let progress = "80%"
            let modularTemplate = CLKComplicationTemplateUtilitarianSmallFlat()
            modularTemplate.imageProvider = CLKImageProvider(onePieceImage: getCoachImage())
            modularTemplate.textProvider = CLKSimpleTextProvider(text: progress)
            template = modularTemplate
            break
            
        case .UtilitarianLarge:
            let modularTemplate = CLKComplicationTemplateUtilitarianLargeFlat()
            modularTemplate.textProvider = CLKSimpleTextProvider(text: "0% No session")
            template = modularTemplate
            break
            
        case .CircularSmall:
            let modularTemplate = CLKComplicationTemplateCircularSmallRingImage()
            modularTemplate.imageProvider = CLKImageProvider(onePieceImage: getCoachImage())
            modularTemplate.fillFraction = 0
            modularTemplate.ringStyle = CLKComplicationRingStyle.Closed
            template = modularTemplate
            break
        }
        
        handler(template)
    }
    
    func getCoachImage() -> UIImage {
        let coachNames = WatchInactivityCoach.sharedInstance.getCoachNames()
        let selected = WatchInactivityCoach.sharedInstance.getSharedInfo(kSelectedCoach) as! Int
        let selectedName = coachNames[selected]
        let imgName = "complications_\(selectedName)"
        return UIImage(named: imgName)!
    }
    
}
