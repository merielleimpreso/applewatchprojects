//
//  WatchNotificationController.swift
//  InactivityCoach
//
//  Created by ATserver on 11/9/15.
//  Copyright © 2015 Merielle Impreso. All rights reserved.
//

import WatchKit
import Foundation


class WatchNotificationController: WKInterfaceController {
    @IBOutlet var hourButton: WKInterfaceButton!
    @IBOutlet var minuteButton: WKInterfaceButton!

    var hour: Int = 0 {
        didSet {
            if let button = hourButton {
                button.setTitle(Formatter.zeroPad(hour))
            }
        }
    }
    var minute: Int = 0 {
        didSet {
            if let button = minuteButton {
                button.setTitle(Formatter.zeroPad(minute))
            }
        }
    }
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        hour = WatchInactivityCoach.sharedInstance.getSharedInfo(kIntervalHour) as! Int
        minute = WatchInactivityCoach.sharedInstance.getSharedInfo(kIntervalMinute) as! Int
        
        let totalTime = hour + minute
        if totalTime == 0 {
            minute = 1
            WatchInactivityCoach.sharedInstance.setSharedInfo(kIntervalMinute, value: minute)
        }
    }
    
    override func contextForSegueWithIdentifier(segueIdentifier: String) -> AnyObject? {
        return self
    }
    
}
