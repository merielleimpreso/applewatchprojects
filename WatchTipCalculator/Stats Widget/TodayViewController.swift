//
//  TodayViewController.swift
//  Stats Widget
//
//  Created by Lito Ang on 12/3/14.
//  Copyright (c) 2014 Aeus. All rights reserved.
//

import UIKit
import NotificationCenter

class TodayViewController: UIViewController, NCWidgetProviding {
    @IBOutlet weak var lblDisplay: UILabel!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view from its nib.
        
        getStats()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func widgetPerformUpdateWithCompletionHandler(completionHandler: ((NCUpdateResult) -> Void)!) {
        // Perform any setup necessary in order to update the view.

        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData

        completionHandler(NCUpdateResult.NewData)
    }
    
    func getStats() {
//        lblDisplay.text = "storage left: 128GB"
        
        let documentDirectoryPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        if let systemAttributes = NSFileManager.defaultManager().attributesOfFileSystemForPath(documentDirectoryPath.last as! String, error: nil) {
            if let freeSize = systemAttributes[NSFileSystemFreeSize] as? NSNumber {
                // 1000000000
                // 1073741824
//                lblDisplay.text = "storage left: " + String(freeSize.longLongValue/1000000000) + " GB"
                lblDisplay.text = "storage left: " + String(freeSize.longLongValue/1000000) + " MB"

            }
        }
    }
    
}
