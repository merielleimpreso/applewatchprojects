//
//  InterfaceController.swift
//  ExploreWidgets WatchKit Extension
//
//  Created by Lito Ang on 1/22/15.
//  Copyright (c) 2015 Aeus. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

protocol Context {
    typealias DelType
//    typealias ObjType
    var delegate: DelType? { get set }
//    var object: ObjType? { get set }
}

class ControllerContext: Context {
    typealias DelType = InterfaceControllerDelegate
//    typealias ObjType = ContextModel
    var delegate: DelType?
//    weak var object: ObjType?
}

//class ContextModel {
//    var data : String;
//    init(data : String) {
//        self.data = data
//    }
//}

protocol InterfaceControllerDelegate  {
    var numPadSegue:String? { get set }
    var data:String? { get set }

    func didEnterAmount(amount:String)
}

func log(message:String) {
//     print(message)
}

class TCBillInterfaceController: WKInterfaceController, InterfaceControllerDelegate, WCSessionDelegate {
    
    @IBOutlet weak var btnTipPercent: WKInterfaceButton!
    @IBOutlet weak var lblPay: WKInterfaceLabel!
    @IBOutlet weak var lblTipAmt: WKInterfaceLabel!
    @IBOutlet weak var mainGroup: WKInterfaceGroup!
    @IBOutlet weak var keypadGroup: WKInterfaceGroup!
    @IBOutlet weak var lblAmount: WKInterfaceLabel!
    @IBOutlet weak var btnBill: WKInterfaceButton!
    
    
    @IBOutlet var pckAmount: WKInterfacePicker!
    @IBOutlet var pckCents: WKInterfacePicker!
    
    
    @IBOutlet var grpBillUI: WKInterfaceGroup!
    
    @IBOutlet var lblSeparator: WKInterfaceLabel!
    
    var billDollar:Int = 0
    var billCents:Int = 0
    
    var bill:Float = 0;
    var tipPercent:Float = 0
    var amountEntered:String = "0"
    var numPadSegue:String? = ""
    
    var data:String? = "15"
    
    var separator:String = "."
    
    let formatNumber = NSNumberFormatter()
    
    var billDollarStr = "0"
    var billCentsStr = "0"
    
    var pickerCentItems: [WKPickerItem]! = []
    
    var session : WCSession!
    
    @IBAction func jumpToNumpad() {
        self.numPadSegue = "seguebill"
        
        let context = ControllerContext()
        context.delegate = self
        
        //        let contextModel = ContextModel(data: "source data")
        //        contextModel.data = "source data"
        //        context.object = contextModel
        
        presentControllerWithName("Numpad", context:context)
    }
    
    @IBAction func jumpToCompactMode() {
        pushControllerWithName("Compact", context:nil)
    }
    
    @IBAction func pickerSelectedAmount(value: Int) {
        
        billDollarStr = "\(value)"
        amountEntered = "\(billDollarStr).\(billCentsStr)"
        
        storeSharedInfo("bill_dollaramount", value: billDollarStr)

        
//        print("bill=\(amountEntered)")
        
    }
    
    @IBAction func pickerSelectedCents(value: Int) {
//        billCentsStr = pickerCentItems[value].title!
        
        billCentsStr = pickerCentItems[value].title!.stringByReplacingOccurrencesOfString(separator, withString: "")

        amountEntered = "\(billDollarStr)\(separator)\(billCentsStr)"
        
        storeSharedInfo("bill_cents", value: billCentsStr)

        
//        print("bill=\(amountEntered)")
        
    }
    
    override func contextForSegueWithIdentifier(_segueIdentifier: String) -> AnyObject? {
        log(_segueIdentifier)
        
        self.numPadSegue = _segueIdentifier
        
        let context = ControllerContext()
//        let contextModel = ContextModel(data: "source data")
//        contextModel.data = "source data"
//        context.object = contextModel
        context.delegate = self
        
        return context
        
//        if (_segueIdentifier == "Load View") {
//            // pass data to next view
//        }
    }
    
    func session(_ session: WCSession,
        didReceiveApplicationContext applicationContext: [String : AnyObject]) {
            let unlocked = applicationContext["split_unlocked"] as! NSString
            log("watchos - received app context = \(unlocked)")
            
            if(unlocked == "yes") {
                clearAllMenuItems()
                addMenuItemWithImageNamed("numpad", title: "Number Pad", action: "jumpToNumpad")
                addMenuItemWithImageNamed("compact_mode", title: "Compact Mode", action: "jumpToCompactMode")
//                addMenuItemWithItemIcon(WKMenuItemIcon.Accept, title: "Compact Mode", action: "jumpToCompactMode")
                
                storeSharedInfo("split_unlocked", value:"yes")
            }

    }
    
    func retrieveSharedInfo(key:String) -> String {
        
        let appGroupID = "group.com.aeustech.watchtipcalculator"
        let defaults = NSUserDefaults(suiteName: appGroupID)
        
        defaults!.synchronize()
        
        if let value = defaults!.objectForKey(key) as? String {
            
//            print("got \(key) = \(value)")
            
            return value
        } else {
            return ""
        }
        
    }
    
    func storeSharedInfo(key:String, value:String) {
        
        let appGroupID = "group.com.aeustech.watchtipcalculator"
        let defaults = NSUserDefaults(suiteName: appGroupID)
        
        defaults!.setObject(value, forKey: key)
        
        defaults!.synchronize()
        
//        print("stored \(key) = \(value)")
        
    }
    
    func didEnterAmount(var amount:String) {
        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(0.6 * Double(NSEC_PER_SEC)))
        dispatch_after(time, dispatch_get_main_queue(), {
//            self.updateBill(amount)
            log("bill amount = \(amount)")
            self.amountEntered = amount
            
            let rounded = Double(round(Double(amount)! * 100)/100)
            amount = "\(rounded)"
            
            self.billDollarStr = amount.componentsSeparatedByString(".")[0]
            self.billCentsStr = amount.componentsSeparatedByString(".")[1]
            
            if(self.billCentsStr.characters.count == 1) {
                self.billCentsStr += "0"
            }
            
            self.pckAmount.setSelectedItemIndex(Int(self.billDollarStr)!)
            self.pckCents.setSelectedItemIndex(Int(self.billCentsStr)!)
            
            self.storeSharedInfo("bill_dollaramount", value: self.billDollarStr)
            self.storeSharedInfo("bill_cents", value: self.billCentsStr)
            


        })
    }
    
//    func updateBill(amount:String) {
//        amountEntered = amount
////        let amountDisplayed = formatNumber.stringFromNumber((amount as NSString).floatValue)!
//        
////        btnBill.setTitle(amountDisplayed)
//        log("bill amount = \(amountEntered)")
//    }
    
    func handleButton(button:String) {
//        log("button \(button)")
        
        if button != "ok" {
            amountEntered += button
            lblAmount.setText(amountEntered)
        }
    }

    
    @IBAction func btnOkAction() {

        
        if selectedButton == btnBill {
            bill = (amountEntered as NSString).floatValue
            selectedButton.setTitle(formatNumber.stringFromNumber(bill))
            
        } else if selectedButton == btnTipPercent {
            tipPercent = (amountEntered as NSString).floatValue
            selectedButton.setTitle(amountEntered)
            
        }
        
        
        let tip = bill * (tipPercent/100)
        let pay = bill + tip
        
        
        log("bill = \(bill)")
        log("tip = \(tip)")
        log("pay = \(pay)")
        
        lblPay.setText(formatNumber.stringFromNumber(pay))
        lblTipAmt.setText(formatNumber.stringFromNumber(tip))
        
        mainGroup.setHidden(false);
        keypadGroup.setHidden(true);
    }
    
    @IBAction func billOpenKeypadAction() {
        
//        updateBill(amountEntered)
        
        
//        selectedButton = btnBill
//        
//        lblAmount.setText("0")
//        amountEntered = ""
//        
//        mainGroup.setHidden(true);
//        keypadGroup.setHidden(false);
    }
    
    @IBAction func tipOpenAction() {
        let context = ControllerContext()
//        let contextModel = ContextModel(data: amountEntered)
//        contextModel.data = amountEntered
//        context.object = contextModel
        context.delegate = self
        
        pushControllerWithName("Tip", context:context)
    }
    
    var selectedButton:WKInterfaceButton!
    
    func printFonts() {
        let fontFamilyNames = UIFont.familyNames()
        for familyName in fontFamilyNames {
            log("family name = \(familyName)")
            let names = UIFont.fontNamesForFamilyName(familyName )
            log("font names = \(names)")
        }
    }

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
//        mainGroup.setHidden(false)
//        keypadGroup.setHidden(true)
        
//        var locale: AnyObject = NSLocale.preferredLanguages()[0]
        
        let locale: AnyObject = NSLocale.currentLocale()
        separator = locale.objectForKey(NSLocaleDecimalSeparator) as! String
        
        //TEST ONLY
//        separator = ","
//        formatNumber.locale = NSLocale(localeIdentifier: "de_DE")

        
        log("watch locale decimal separator = " + separator)
        
//        lblSeparator.setText(separator)

        
        formatNumber.numberStyle = NSNumberFormatterStyle.CurrencyStyle
//        formatNumber.locale = NSLocale(localeIdentifier: "de_DE")
        btnBill.setTitle(formatNumber.stringFromNumber(0))
        
//        printFonts()
        
//        let attributedText = NSAttributedString(
//            string: "BILL AMOUNT",
//            attributes: NSDictionary (object: UIFont(name: "Roboto-Thin", size: 8)!,
//                forKey: NSFontAttributeName) as [NSObject : AnyObject]
//        )
//        
//        lblAmount.setAttributedText(attributedText)
//
        
//        btnTipPercent.setTitle("0")
//        lblPay.setText(formatNumber.stringFromNumber(0))
//        lblTipAmt.setText(formatNumber.stringFromNumber(0))
        
        
        // TEST ONLY
//        let unlocked = "yes"

//        let unlocked = retrieveSharedInfo("split_unlocked")
//        if(unlocked == "yes") {
//            addMenuItemWithItemIcon(WKMenuItemIcon.Accept, title: "Compact Mode", action: "jumpToCompactMode")
//        }
    }

    func isWatch38mm()->Bool {
        let rect = WKInterfaceDevice.currentDevice().screenBounds
        if (rect.size.height == 195.0) {
            // Apple Watch 42mm
        } else if (rect.size.height == 170.0){
            // Apple Watch 38mm
            return true
        }
        return false
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()

        if isWatch38mm() {
            self.setTitle("Tip Calc")
        }
        
        var pickerItems: [WKPickerItem]! = []
        
        for i in 0...999 {
            let pickerItem = WKPickerItem()
            pickerItem.title = "\(i)"
            pickerItem.caption = "\(i)"
            pickerItems.append(pickerItem)
        }
        
        for i in 0...99 {
            let pickerItem = WKPickerItem()
            
//            pickerItem.title = "\(i)"
            pickerItem.title = String(format: "\(separator)%02d", i)
            
//            pickerItem.caption = "\(i)"
            pickerItem.caption = String(format: "%02d", i)

            
            pickerCentItems.append(pickerItem)
        }
        
        // create animated images and picker items
        var images: [UIImage]! = []
        for (var i=0; i<=15; i++) {
            let name = "progress_circle-\(i)"
            images.append(UIImage(named: name)!)
        }
        let progressImages = UIImage.animatedImageWithImages(images, duration: 0.0)

        grpBillUI.setBackgroundImage(progressImages)
        pckAmount.setCoordinatedAnimations([grpBillUI])

        pckAmount.setItems(pickerItems)
        pckAmount.focus()
        
        pckCents.setItems(pickerCentItems)
        
        let bill_amount = Int(retrieveSharedInfo("bill_dollaramount"))
        let bill_cents = Int(retrieveSharedInfo("bill_cents"))
        pckAmount.setSelectedItemIndex(bill_amount == nil ? 0 : bill_amount!)
        pckCents.setSelectedItemIndex(bill_cents == nil ? 0 : bill_cents!)

//        print("bill - will activate")
        
        if (WCSession.isSupported()) {
            session = WCSession.defaultSession()
            session.delegate = self
            session.activateSession()
        }
        
        let unlocked = retrieveSharedInfo("split_unlocked")
        if(unlocked == "yes") {
            clearAllMenuItems()
            addMenuItemWithImageNamed("numpad", title: "Number Pad", action: "jumpToNumpad")
            addMenuItemWithImageNamed("compact_mode", title: "Compact Mode", action: "jumpToCompactMode")
        } else {
            clearAllMenuItems()
            addMenuItemWithImageNamed("numpad", title: "Number Pad", action: "jumpToNumpad")
        }

//        resetColors()

    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    func resetColors() {
        btnTipPercent.setBackgroundColor(UIColor.clearColor())

    }
    


}
