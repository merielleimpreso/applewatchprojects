//
//  RecordViewController.swift
//  WearHydrateMe
//
//  Created by Merielle Impreso on 5/27/15.
//  Copyright (c) 2015 Merielle Impreso. All rights reserved.
//

import UIKit
import DataKit

class RecordViewController: UIViewController {
    
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var waterImageConstraint: NSLayoutConstraint!
    
    var allRecords: [Record] = []
    var allGoals: [Goal] = []
    var recordsToday: [Record] = []
    var goalToday: Goal?
    
    @IBAction func recordButtonClicked(sender: AnyObject) {
        drink()
    }
    
    func getRecordsToday() -> [Record] {
        var recordsToday: [Record] = []
        let formatter = NSDateFormatter()
        formatter.dateStyle = .ShortStyle
        let dateToday = formatter.stringFromDate(NSDate())
        
        allRecords = DataAccess.sharedInstance.getAllRecords()
        for record in allRecords {
            let date = formatter.stringFromDate(record.timeStamp)
            if (date == dateToday) {
                recordsToday.append(record)
            }
        }
        return recordsToday
    }
    
    func getGoalToday() -> Goal? {
        var goalToday: Goal? = nil
        allGoals = DataAccess.sharedInstance.getAllGoals()
        
        let formatter = NSDateFormatter()
        formatter.dateStyle = .ShortStyle
        let dateToday = formatter.stringFromDate(NSDate())
        for goal in allGoals {
            let date = formatter.stringFromDate(goal.date)
            if (date == dateToday) {
                goalToday = goal
                break
            }
        }
        
        if (goalToday == nil) {
            goalToday = DataAccess.sharedInstance.insertGoal()
        }
        
        return goalToday
    }
    
    /*func getGoalToday() -> Goal? {
        var goalToday: Goal?
        allGoals = DataAccess.sharedInstance.getAllGoals()
        
        if (allGoals.count == 0) {
            goalToday = DataAccess.sharedInstance.insertGoal()
        } else {
            var latestGoal = allGoals.first!
            
            let formatter = NSDateFormatter()
            formatter.dateStyle = .ShortStyle
            let dateToday = formatter.stringFromDate(NSDate())
            let date = formatter.stringFromDate(latestGoal.date)
            
            if (date == dateToday) {
                goalToday = latestGoal
            } else {
                goalToday = DataAccess.sharedInstance.insertGoal()
            }
        }
        return goalToday
    }*/
    
    func animateImage(prevGlassNumber: Int, currentGlassNumber: Int) {
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        let height = screenSize.height
        let goal: CGFloat = goalToday!.number as CGFloat
        let duration: Double = Double(currentGlassNumber-prevGlassNumber)
        
        let prev: Double = Double(prevGlassNumber)
        let prevDistance = (height/goal) * CGFloat(prev)
        self.waterImageConstraint.constant = prevDistance
        
        UIView.animateWithDuration(duration, animations: {
            let distance = (height/goal) * CGFloat(duration)
            self.waterImageConstraint.constant += distance
            self.view.layoutIfNeeded()
        })
    }
    
    func drink() {
        var record = DataAccess.sharedInstance.insertRecord()
        allRecords.append(record)
        recordsToday.append(record)
        recordButton.setTitle("\(recordsToday.count)", forState: [])
        
        animateImage(recordsToday.count-1, currentGlassNumber: recordsToday.count)
        if (recordsToday.count >= (goalToday!.number as Int)) {
            let image = UIImage(named:"main_drink_button_finished")
            recordButton.setBackgroundImage(image, forState: [])
        } else {
            let image = UIImage(named:"main_drink_button")
            recordButton.setBackgroundImage(image, forState: [])
        }
    }
    
    func drinkFromNotification() {
        var record = DataAccess.sharedInstance.insertRecord()
        
        recordsToday = getRecordsToday()
        goalToday = getGoalToday()
        
        animateImage(0, currentGlassNumber: recordsToday.count)
        recordButton.setTitle("\(recordsToday.count)", forState: [])
        if (recordsToday.count >= (goalToday!.number as Int)) {
            let image = UIImage(named:"main_drink_button_finished")
            recordButton.setBackgroundImage(image, forState: [])
        } else {
            let image = UIImage(named:"main_drink_button")
            recordButton.setBackgroundImage(image, forState: [])
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "drinkFromNotification", name: "drink", object: nil)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        recordsToday = getRecordsToday()
        goalToday = getGoalToday()
        
        animateImage(0, currentGlassNumber: recordsToday.count)
        recordButton.setTitle("\(recordsToday.count)", forState: [])
        if (recordsToday.count >= (goalToday!.number as Int)) {
            let image = UIImage(named:"main_drink_button_finished")
            recordButton.setBackgroundImage(image, forState: [])
        } else {
            let image = UIImage(named:"main_drink_button")
            recordButton.setBackgroundImage(image, forState: [])
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
