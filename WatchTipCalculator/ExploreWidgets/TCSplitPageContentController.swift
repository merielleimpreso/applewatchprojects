//
//  TCSplitPageContentController.swift
//  WatchTipCalculator
//
//  Created by dev on 4/27/15.
//  Copyright (c) 2015 Aeus. All rights reserved.
//

import UIKit

class TCSplitPageContentController : TCPageContentController {
    @IBOutlet weak var btn_split_num: UIButton!
    @IBOutlet weak var lbl_share_amount: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}
