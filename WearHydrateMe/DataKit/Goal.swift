//
//  Goal.swift
//  WearHydrateMe
//
//  Created by Merielle Impreso on 5/22/15.
//  Copyright (c) 2015 Merielle Impreso. All rights reserved.
//

import Foundation
import CoreData

@objc(Goal)
public class Goal: NSManagedObject {

    @NSManaged public var date: NSDate
    @NSManaged public var number: NSNumber
    
    public class func getAllGoals() -> [Goal] {
        let fetchResults = NSFetchRequest(entityName: "Goal")
        fetchResults.returnsObjectsAsFaults = false
        
        let sortDescriptor = NSSortDescriptor(key: "date", ascending: false)
        let sortDescriptors = [sortDescriptor]
        fetchResults.sortDescriptors = sortDescriptors
        
        return ((try? managedContext.executeFetchRequest(fetchResults)) as? [Goal])!
    }
    
    public class func insertGoal(managedContext: NSManagedObjectContext) -> Goal {
        let allGoals = getAllGoals(managedContext)
        
        let goal =  NSEntityDescription.insertNewObjectForEntityForName("Goal", inManagedObjectContext:managedContext) as! Goal
        goal.date = NSDate()
        if (allGoals.count == 0) {
            goal.number = 8
        } else {
            let latestGoal = allGoals.first!
            goal.number = latestGoal.number
        }
        
        save(managedContext)
        return goal
    }
    
    public class func insertGoalWithData(managedContext: NSManagedObjectContext, goalData: Goal) -> Goal {
        let goal =  NSEntityDescription.insertNewObjectForEntityForName("Goal", inManagedObjectContext:managedContext) as! Goal
        goal.date = NSDate()
        goal.number = goalData.number
        
        save(managedContext)
        return goal
    }
    
    public class func updateGoalToday(managedContext: NSManagedObjectContext, goal: Goal) -> Goal {
        let allGoals = getAllGoals(managedContext)
        var goalToday: Goal?
        
        let formatter = NSDateFormatter()
        formatter.dateStyle = .ShortStyle
        let dateToday = formatter.stringFromDate(NSDate())
        for g in allGoals {
            let date = formatter.stringFromDate(g.date)
            if (date == dateToday) {
                managedContext.deleteObject(g)
                goalToday = insertGoalWithData(managedContext, goalData: goal)
                save(managedContext)
            }
        }
        
        return goalToday!
    }
    
    public class func save(managedContext: NSManagedObjectContext) {
        var error : NSError?
        if(managedContext.save() ) {
            //println(error?.localizedDescription)
        }
    }

}
