//
//  InterfaceController.swift
//  InactivitityCoach Extension
//
//  Created by Merielle Impreso on 10/1/15.
//  Copyright © 2015 Merielle Impreso. All rights reserved.
//

import WatchKit
import Foundation
import CoreMotion


class WatchMainController: WKInterfaceController {
    @IBOutlet var activateButton: WKInterfaceButton!
    @IBOutlet var coachImage: WKInterfaceImage!
    @IBOutlet var progressImage: WKInterfaceImage!
    @IBOutlet var dayProgressImage: WKInterfaceImage!
    
    @IBOutlet var lblDebug: WKInterfaceLabel!
    
    let coachNames = WatchInactivityCoach.sharedInstance.getCoachNames()
    let motionMgr = CMMotionManager()
    let pedometer = CMPedometer()
    let activityMgr = CMMotionActivityManager()
    
    //TO DO: Change value
    let sessionGoal = 100.0
    let dayGoal = 100.0
    
    var coachState: String? {
        didSet {
            animateCoachImage(coachState!)
        }
    }
    
    var notificationOn: Bool? {
        didSet {
            if let button = activateButton {
                if notificationOn! {
                    button.setBackgroundImageNamed("button_sleep")
                    coachState = kCoachStateNormal
                } else {
                    button.setBackgroundImageNamed("button_active")
                    coachState = kCoachStateSleep
                }
            }
        }
    }
    
    var progress: Double = 0 {
        didSet {
            if let image = progressImage {
                if (progress / sessionGoal) > 0.8 {
                    image.setImageNamed("meter_5")
                } else if (progress / sessionGoal) > 0.6 {
                    image.setImageNamed("meter_4")
                } else if (progress / sessionGoal) > 0.4 {
                    image.setImageNamed("meter_3")
                } else if (progress / sessionGoal) > 0.2 {
                    image.setImageNamed("meter_2")
                } else if (progress / sessionGoal) > 0 {
                    image.setImageNamed("meter_1")
                } else {
                    image.setImageNamed("meter_0")
                }
            }
        }
    }
    
    var dayProgress: Double = 0 {
        didSet {
            if let image = dayProgressImage {
                if (dayProgress / dayGoal) > 0.8 {
                    image.setImageNamed("day_meter_5")
                } else if (dayProgress / dayGoal) > 0.6 {
                    image.setImageNamed("day_meter_4")
                } else if (dayProgress / dayGoal) > 0.4 {
                    image.setImageNamed("day_meter_3")
                } else if (dayProgress / dayGoal) > 0.2 {
                    image.setImageNamed("day_meter_2")
                } else if (dayProgress / dayGoal) > 0 {
                    image.setImageNamed("day_meter_1")
                } else {
                    image.setImageNamed("day_meter_0")
                }
            }
        }
    }
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        notificationOn = WatchInactivityCoach.sharedInstance.getSharedInfo(kNotificationOn) as? Bool
    }

    override func willActivate() {
        super.willActivate()
        self.lblDebug.setText("steps")

        handlePedometer()
        animateCoachImage(coachState!)
    }
    
    @IBAction func activateButtonClicked() {
        notificationOn = !notificationOn!
        WatchInactivityCoach.sharedInstance.setSharedInfo(kNotificationOn, value: notificationOn!)
    }
    
    func animateCoachImage(state: String) {
        let selected = WatchInactivityCoach.sharedInstance.getSharedInfo(kSelectedCoach) as! Int
        let selectedName = coachNames[selected]
        let imageName = "w_coach_\(selectedName)_\(state)"
        
        coachImage.setImageNamed(imageName)
        coachImage.startAnimatingWithImagesInRange(NSRange(location: 0, length: 2), duration: 1, repeatCount: Int.max)
    }
    
    func handlePedometer() {
        
        var status = ""
        
        if (CMPedometer.isStepCountingAvailable() == true) {
            
            // TODO: use this for daily progress, steps starting 6:30am will be counted towards daily progress
            
            //            let cal = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)
            //            let dateStart = cal?.dateBySettingHour(6, minute: 30, second: 0, ofDate: NSDate(), options: NSCalendarOptions())
            
            
            // calculate seconds since last notification
//            let intervalHour = InactivityCoach.sharedInstance.getSharedInfo(kIntervalHour) as! Int
//            let intervalMinute = InactivityCoach.sharedInstance.getSharedInfo(kIntervalMinute) as! Int
            
            // test only - get step count from 5 minutes ago
            let intervalHour = 0
            let intervalMinute = 5
            
            let secondsSinceLastNotification = Double((intervalHour * 60 * 60) + (intervalMinute * 60)) * -1
            
            let dateStart = NSDate(timeInterval:secondsSinceLastNotification, sinceDate: NSDate())
            
            // get step count since last notification
            pedometer.startPedometerUpdatesFromDate(dateStart) { (pedometerData, error) -> Void in
                
                if pedometerData != nil {
                    let steps: UInt = pedometerData!.numberOfSteps.unsignedLongValue
                    
                    self.progress = Double(steps)
                    print("progress = \(self.progress)% steps = \(steps)")
                    
                    // calculate steps while app is inactive, if it exceeds threshold, change coach to happy and inform user
                    
//                    let lastStoredSteps:UInt = InactivityCoach.sharedInstance.getSharedInfo(kLastStepCount) == nil ?
//                        0 :
//                        InactivityCoach.sharedInstance.getSharedInfo(kLastStepCount) as! UInt
                    
//                    let stepsSinceLastNotification = Double(steps - lastStoredSteps)
                    
//                    if(stepsSinceLastNotification >= kGoalStepsBetweenNotifications) {
//                        // max steps between notifications exceeded, make coach happy and store steps
//                        self.progress = kGoalStepsBetweenNotifications
//                        self.animateProgressTextImage(true)
//                        
//                        InactivityCoach.sharedInstance.setSharedInfo(kLastStepCount, value: steps)
//                        
//                    } else {
//                        // max steps not exceeded, let user complete the remaining steps
//                        self.progress = Double(stepsSinceLastNotification) / Double(kGoalStepsBetweenNotifications) * kGoalStepsBetweenNotifications
//                        self.animateProgressTextImage(true)
//                        
//                    }
                    
                    //                    status = "stepsSinceLastNotification = \(stepsSinceLastNotification)"
//                    status = "\(self.progress)% \(stepsSinceLastNotification) steps"
                    
                    dispatch_async(dispatch_get_main_queue(),{
//                        self.statsLabel.text = status
//                        print("status = " + status)
                        
                        self.lblDebug.setText("progress = \(self.progress)%")
                    })
                }
            }
        } else {
            status = "pedometer not available"
            self.lblDebug.setText(status)
        }
    }
    

    
}
