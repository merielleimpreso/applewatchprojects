//
//  AlarmController.swift
//  WearHydrateMe
//
//  Created by Merielle Impreso on 5/21/15.
//  Copyright (c) 2015 Merielle Impreso. All rights reserved.
//

import WatchKit
import Foundation
import DataKit


class AlarmController: WKInterfaceController {
    @IBOutlet weak var hourButton: WKInterfaceButton!
    @IBOutlet weak var minuteButton: WKInterfaceButton!
    @IBOutlet weak var alarmSwitch: WKInterfaceSwitch!
    
    var alarmOn = false
    var hour = 0
    var minute = 30

    @IBAction func alarmValueChanged(alarmOn: Bool) {
        if alarmOn {
            if hour == 0 && minute == 0 {
                minute = 30
                minuteButton.setTitle("\(minute)")
            }
        }
        self.alarmOn = alarmOn
        DataAccess.sharedInstance.updateAlarmOn(alarmOn)
    }
        
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        alarmOn = DataAccess.sharedInstance.getAlarmOn()
        hour = DataAccess.sharedInstance.getAlarmHour()
        minute = DataAccess.sharedInstance.getAlarmMinute()
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        alarmSwitch.setOn(alarmOn)
        hourButton.setTitle(Formatter.zeroPad(hour))
        minuteButton.setTitle(Formatter.zeroPad(minute))
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    override func contextForSegueWithIdentifier(segueIdentifier: String) -> AnyObject? {
        return self
    }
}
