//
//  TodayViewController.swift
//  InactivityCoach Widget
//
//  Created by ATserver on 12/11/15.
//  Copyright © 2015 Merielle Impreso. All rights reserved.
//

import UIKit
import NotificationCenter
import CoreMotion


let kCoachStateNormal = "normal"
let kCoachStateSleep = "sleep"
let kCoachStateAngry = "angry"
let kCoachStateHappy = "happy"

let kNotificationOn = "notification_on"
let kIntervalHour = "interval_hour"
let kIntervalMinute = "interval_minute"
let kSelectedCoach = "selected_coach"
let kLastStepCount = "last_step_count"
let kLastActiveTime = "last_active_time"
let kSessionProgress = "session_progress"
let kDayProgress = "day_progress"
let kCoachState = "coach_state"

let kPudgyBirbIndex = 0
let kChipIndex = 1
let kMasterMaruIndex = 2
let kDrDokiIndex = 3
let kPrinceWillyIndex = 4

class TodayViewController: UIViewController, NCWidgetProviding {
    
    @IBOutlet weak var coachImage: UIImageView!
    @IBOutlet weak var activeTimeLabel: UILabel!
    var progressView: ProgressView!
    var dayProgressView: DayProgressView!
    
    var notificationOn: Bool? {
        didSet {
            coachState = (notificationOn == true) ? kCoachStateNormal : kCoachStateSleep
        }
    }
    
    var coachState: String = kCoachStateNormal {
        didSet {
            animateCoachImage()
        }
    }
    
    var progress: Double = 0 {
        didSet {
            if let view = progressView {
                view.progress = progress
            }
        }
    }
    
    var dayProgress: Double = 0 {
        didSet {
            if let view = dayProgressView {
                view.progress = dayProgress
            }
        }
    }
    
    var activeTime: NSDate? {
        didSet {
            if let label = activeTimeLabel {
                let formatter = NSDateFormatter()
                formatter.dateStyle = .ShortStyle
                let dateToday = formatter.stringFromDate(NSDate())
                let dateActive = formatter.stringFromDate(activeTime!)
                if (dateActive == dateToday) {
                    formatter.dateStyle = .NoStyle
                    formatter.timeStyle = .ShortStyle
                    label.text = "Last active at \(formatter.stringFromDate(activeTime!))"
                } else {
                    label.text = "No session today yet"
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addProgressViews()
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: "openApp")
        self.view.addGestureRecognizer(gestureRecognizer)
    }
    
    func  widgetPerformUpdateWithCompletionHandler(completionHandler: ((NCUpdateResult) -> Void)) {
        updateData()
        completionHandler(NCUpdateResult.NewData)
    }
    
    func widgetMarginInsetsForProposedMarginInsets(defaultMarginInsets: UIEdgeInsets) -> UIEdgeInsets {
        return UIEdgeInsetsZero
    }
    
    func addProgressViews() {
        var add = -arcWidth*4
        var width = coachImage.frame.width + add
        var height = coachImage.frame.height + add
        progressView = ProgressView(frame: CGRectMake(-add/2, -add/2, width, height))
        self.coachImage.addSubview(progressView)
        progressView.animateCircle(1.0)
        
        add = -arcWidth*2
        width = coachImage.frame.width + add
        height = coachImage.frame.height + add
        dayProgressView = DayProgressView(frame: CGRectMake(-add/2, -add/2, width, height))
        self.coachImage.addSubview(dayProgressView)
        dayProgressView.animateCircle(1.0)
        
        updateData()
    }
    
    func updateData() {
        notificationOn = getSharedInfo(kNotificationOn) as? Bool
        progress = getSharedInfo(kSessionProgress) as! Double
        dayProgress = getSharedInfo(kDayProgress) as! Double
        activeTime = getSharedInfo(kLastActiveTime) as? NSDate
        //        coachState = getSharedInfo(kCoachState) as! String
        //
        //        print("coach state: \(coachState)")
        
        /*if notificationOn != self.notificationOn {
        self.notificationOn = notificationOn
        }
        if progress != self.progress {
        self.progress = progress
        }
        if dayProgress != self.dayProgress {
        self.dayProgress = dayProgress
        }
        if activeTime != self.activeTime {
        self.activeTime = activeTime
        }*/
        
        
        // TEST ONLY
        //        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(5.0 * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) { () -> Void in
        //            print("after 5 seconds")
        //            self.progressView.progress = 0.2
        //
        //            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(5.0 * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) { () -> Void in
        //                print("after 5 seconds")
        //                self.progressView.progress = 0.8
        //
        //                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(5.0 * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) { () -> Void in
        //                    print("after 5 seconds")
        //                    self.progressView.progress = 0.4
        //                }
        //            }
        //        }
    }
    
    
    func animateCoachImage() {
        let selected = getSharedInfo(kSelectedCoach) as! Int
        let selectedName = getCoachNames()[selected]
        
        let img1 = "today_\(selectedName)_\(coachState)1"
        coachImage.image = UIImage(named: img1)
        //let img2 = "today_\(selectedName)_\(coachState)2"
        //let animationImages: [UIImage] = [UIImage(named: img1)!, UIImage(named: img2)!]
        
        //coachImage.animationImages = animationImages
        //coachImage.animationDuration = 1.0
        //coachImage.startAnimating()
    }
    
    func getCoachNames() -> [String] {
        var coachNames: [String] = ["", "", "", "", ""]
        coachNames[kPudgyBirbIndex] = "pudgy"
        coachNames[kChipIndex] = "chip"
        coachNames[kMasterMaruIndex] = "maru"
        coachNames[kDrDokiIndex] = "doki"
        coachNames[kPrinceWillyIndex] = "willy"
        
        return coachNames
    }
    
    func getSharedInfo(key: String) -> AnyObject? {
        let sharedAppGroup: NSString = "group.com.aeustech.inactivitycoach"
        let defaults = NSUserDefaults(suiteName: sharedAppGroup as String)
        
        if key == kSelectedCoach || key == kIntervalHour || key == kIntervalMinute  {
            return defaults!.integerForKey(key)
        } else if key == kSessionProgress || key == kDayProgress {
            return defaults!.doubleForKey(key)
        } else if key == kNotificationOn {
            return defaults!.boolForKey(key)
        } else if key == kLastActiveTime || key == kCoachState {
            return defaults!.objectForKey(key)
        } else {
            return nil
        }
    }
    
    func openApp() {
        let url =  NSURL(string:"InactivityCoach://")
        self.extensionContext!.openURL(url!, completionHandler:{(success: Bool) -> Void in
            print("task done!")
        })
    }
}
