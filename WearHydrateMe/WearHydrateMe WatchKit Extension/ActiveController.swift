//
//  ActiveController.swift
//  WearHydrateMe
//
//  Created by Merielle Impreso on 5/21/15.
//  Copyright (c) 2015 Merielle Impreso. All rights reserved.
//

import WatchKit
import Foundation
import DataKit

class ActiveController: WKInterfaceController {

    @IBOutlet weak var fromHourButton: WKInterfaceButton!
    @IBOutlet weak var fromMinuteButton: WKInterfaceButton!
    @IBOutlet weak var fromMiridiemButton: WKInterfaceButton!
    @IBOutlet weak var toHourButton: WKInterfaceButton!
    @IBOutlet weak var toMinuteButton: WKInterfaceButton!
    @IBOutlet weak var toMiridiemButton: WKInterfaceButton!
    
    var fromHour: Int = 8
    var fromMinute: Int = 0
    var fromMiridiem: String = "AM"
    var toHour: Int = 8
    var toMinute: Int = 0
    var toMiridiem: String = "PM"
    
    @IBAction func fromMiridiemButtonClicked() {
        if fromMiridiem == "AM" {
            fromMiridiem = "PM"
        } else {
            fromMiridiem = "AM"
        }
        fromMiridiemButton.setTitle(fromMiridiem)
        DataAccess.sharedInstance.updateActiveFromMiridiem(fromMiridiem)
    }
    
    @IBAction func toMiridiemButtonClicked() {
        if toMiridiem == "AM" {
            toMiridiem = "PM"
        } else {
            toMiridiem = "AM"
        }
        toMiridiemButton.setTitle(toMiridiem)
        DataAccess.sharedInstance.updateActiveFromMiridiem(toMiridiem)
    }
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        fromHour = DataAccess.sharedInstance.getActiveFromHour()
        fromMinute = DataAccess.sharedInstance.getActiveFromMinute()
        fromMiridiem = DataAccess.sharedInstance.getActiveFromMiridiem()
        toHour = DataAccess.sharedInstance.getActiveToHour()
        toMinute = DataAccess.sharedInstance.getActiveToMinute()
        toMiridiem = DataAccess.sharedInstance.getActiveToMiridiem()
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        fromHourButton.setTitle(Formatter.zeroPad(fromHour))
        fromMinuteButton.setTitle(Formatter.zeroPad(fromMinute))
        fromMiridiemButton.setTitle("\(fromMiridiem)")
        
        toHourButton.setTitle(Formatter.zeroPad(toHour))
        toMinuteButton.setTitle(Formatter.zeroPad(toMinute))
        toMiridiemButton.setTitle(toMiridiem)
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    override func contextForSegueWithIdentifier(segueIdentifier: String) -> AnyObject? {
        return ["parentController" : self,
            "segueIdentifier" : segueIdentifier]
    }

}
