//
//  TrackDetailController.swift
//  WearHydrateMe
//
//  Created by Merielle Impreso on 5/26/15.
//  Copyright (c) 2015 Merielle Impreso. All rights reserved.
//

import WatchKit
import Foundation
import DataKit


class TrackDetailController: WKInterfaceController {
    @IBOutlet weak var table: WKInterfaceTable!
    @IBOutlet weak var dateLabel: WKInterfaceLabel!
    
    var parentController: TrackController? = nil
    var track: Track?
    var selectedRecord: Record?
    var selectedRowIndex: Int = 0
    var shouldDelete: Bool = false
    
    func loadTableData() {
        let records = track!.records! as [Record]
        table.setNumberOfRows(records.count, withRowType: "trackDetailRow")
        
        for i in 0...records.count-1 {
            var r = records[i]
            
            let formatter = NSDateFormatter()
            formatter.timeStyle = .MediumStyle
        
            let row = table.rowControllerAtIndex(i) as! TrackDetailRow
            let date = formatter.stringFromDate(r.timeStamp)
            row.timeLabel.setText(date)
        }
        
        var r = records.first!
        let format = NSDateFormatter.dateFormatFromTemplate("dd MMM yyyy, EEE", options: 0, locale: NSLocale(localeIdentifier: "en-US"))
        let formatter = NSDateFormatter()
        formatter.dateFormat = format
        let date = formatter.stringFromDate(r.timeStamp)
        dateLabel.setText(date)
    }
    
    override func table(table: WKInterfaceTable, didSelectRowAtIndex rowIndex: Int) {
        let records = track!.records
        selectedRecord = records![rowIndex]
        selectedRowIndex = rowIndex
        
        let formatter = NSDateFormatter()
        formatter.dateStyle = .ShortStyle
        let dateToday = formatter.stringFromDate(NSDate())
        let date = formatter.stringFromDate(selectedRecord!.timeStamp)

        if (date == dateToday) {
            presentControllerWithName("TrackDetailDelete", context: self)
        }
    }

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        parentController = (context as! TrackController)
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        track = parentController!.selectedTrack
        if (shouldDelete) {
            let r = track!.records![selectedRowIndex]
            track!.records?.removeAtIndex(selectedRowIndex)
            DataAccess.sharedInstance.deleteRecord(r)
        }
        let records = track!.records
        if (records!.count == 0) {
            self.dismissController()
        } else {
            loadTableData()
        }
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
}
