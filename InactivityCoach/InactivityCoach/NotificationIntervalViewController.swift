//
//  NotificationIntervalViewController.swift
//  InactivityCoach
//
//  Created by ATserver on 10/22/15.
//  Copyright © 2015 Merielle Impreso. All rights reserved.
//

import UIKit

class NotificationIntervalViewController: UIViewController {
    @IBOutlet weak var picker: UIDatePicker!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let intervalTimeDate = InactivityCoach.sharedInstance.getIntervalTimeDate()
        picker.setDate(intervalTimeDate, animated: true)
    }
    
    func saveSettings() {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "HH:mm"
        let stringFromDate = formatter.stringFromDate(self.picker.date)
        let stringFromDateArr = stringFromDate.componentsSeparatedByString(":")
        let hour = Int(stringFromDateArr[0])!
        let minute = Int(stringFromDateArr[1])!
        
        InactivityCoach.sharedInstance.setSharedInfo(kIntervalHour, value: hour)
        InactivityCoach.sharedInstance.setSharedInfo(kIntervalMinute, value: minute)
        
        let notificationOn = InactivityCoach.sharedInstance.getSharedInfo(kNotificationOn) as! Bool
        if notificationOn {
            InactivityCoach.sharedInstance.turnOnNotification(false)
            InactivityCoach.sharedInstance.turnOnNotification(true)
        }
        
        let record = Record.getRecordToday()
        record.goal = Record.getGoalStepsForToday()
        Record.updateRecordToday(record)
        
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    @IBAction func saveButtonClicked(sender: AnyObject) {
        if isMoreThanMinimumTime() {
            saveSettings()
        } else {
            let alert = UIAlertController(title: "Failed to Set Interval Time",
                message: "Minimum time is \(kIntervalMinuteDefault) minutes.",
                preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler:nil))
            self.presentViewController(alert, animated: true, completion: nil)
            
            let intervalTimeDate = InactivityCoach.sharedInstance.getIntervalTimeDate()
            picker.setDate(intervalTimeDate, animated: true)
        }
    }
    
    @IBAction func cancelButtonClicked(sender: AnyObject) {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    func isMoreThanMinimumTime() -> Bool {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "HH:mm"
        let stringFromDate = formatter.stringFromDate(self.picker.date)
        let stringFromDateArr = stringFromDate.componentsSeparatedByString(":")
        let hour = Int(stringFromDateArr[0])!
        let minute = Int(stringFromDateArr[1])!
        
        if hour == 0 {
            return (minute >= kIntervalMinuteDefault)
        } else {
            return true
        }
    }
    
    func getMinimumIntervalDate() -> NSDate {
        let cal = NSCalendar.currentCalendar()
        let comps = NSDateComponents()
        comps.hour = 0
        comps.minute = 20
        let date = cal.dateFromComponents(comps)
        
        return date!
    }
}
