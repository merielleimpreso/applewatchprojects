//
//  Record.swift
//  WearHydrateMe
//
//  Created by Merielle Impreso on 5/22/15.
//  Copyright (c) 2015 Merielle Impreso. All rights reserved.
//

import Foundation
import CoreData

@objc(Record)
public class Record: NSManagedObject {

    @NSManaged public var timeStamp: NSDate
    
    public class func getAllRecords(managedContext: NSManagedObjectContext) -> [Record] {
        let fetchRecords = NSFetchRequest(entityName: "Record")
        fetchRecords.returnsObjectsAsFaults = false
        
        let sortDescriptor = NSSortDescriptor(key: "timeStamp", ascending: false)
        let sortDescriptors = [sortDescriptor]
        fetchRecords.sortDescriptors = sortDescriptors
        
        return ((try? managedContext.executeFetchRequest(fetchRecords)) as? [Record])!
    }
    
    public class func insertRecord(managedContext: NSManagedObjectContext) -> Record {
        let date = NSDate()
        let record =  NSEntityDescription.insertNewObjectForEntityForName("Record", inManagedObjectContext:managedContext) as! Record
        record.timeStamp = date
        
        save(managedContext)
        return record
    }
    
    public class func deleteRecord(managedContext: NSManagedObjectContext, record: Record) {
        managedContext.deleteObject(record)
        save(managedContext)
    }
    
    public class func save(managedContext: NSManagedObjectContext) {
        var error : NSError?
        if(managedContext.save() ) {
            //println(error?.localizedDescription)
        }
    }

}
