//
//  InterfaceController.swift
//  ProtoWatchOSTwo WatchKit Extension
//
//  Created by dev on 10/3/15.
//  Copyright © 2015 Aeus Tech, Inc. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {
    @IBOutlet var picker: WKInterfacePicker!
    @IBOutlet var pickerCents: WKInterfacePicker!
    @IBOutlet var pickerTipPercentage: WKInterfacePicker!
    @IBOutlet var lblTip: WKInterfaceLabel!
    @IBOutlet var lblTotal: WKInterfaceLabel!
    
    @IBOutlet var pickerSplit: WKInterfacePicker!
    
    @IBOutlet var lblSplit: WKInterfaceLabel!
    
    
    var billAmount = "0"
    var billCents = "0"
    var bill = "0"
    var tipPercentage = 0
    var amount:Float = 0
    var tipAmount:Float = 0
    var split = 0
    var separator:String = "."
    
    var pickerCentItems: [WKPickerItem]! = []
    
    let formatNumber = NSNumberFormatter()
    
    
    @IBAction func pickerSplitAction(value: Int) {
        storeSharedInfo("num_split", value: String(value + 1))

        split = value
        let splitAmount = (amount + tipAmount) / Float(value + 1)
        
        let formatNumber = NSNumberFormatter()
        formatNumber.numberStyle = NSNumberFormatterStyle.CurrencyStyle
        let formattedSplitAmount = formatNumber.stringFromNumber(splitAmount)!
        lblSplit.setText(formattedSplitAmount)
        
    }
    
    
    @IBAction func pickerSelectedAmount(value: Int) {
        
        billAmount = "\(value)"
        
        storeSharedInfo("bill_dollaramount", value: billAmount)

        bill = "\(billAmount).\(billCents)"
        
//        print("bill=\(bill)")
        
        calculateAmounts()
    }
    
    @IBAction func pickerSelectedCents(value: Int) {
        billCents = pickerCentItems[value].title!.stringByReplacingOccurrencesOfString(separator, withString: "")
        
        storeSharedInfo("bill_cents", value: billCents)

        bill = "\(billAmount).\(billCents)"
        
//        print("bill=\(bill)")
        
        calculateAmounts()
    }
    
    @IBAction func pickerSelectedTipPercentage(value: Int) {
        tipPercentage = value
        
        storeSharedInfo("tip", value: String(tipPercentage))

        
        calculateAmounts()
    }
    
    func storeSharedInfo(key:String, value:String) {
        
        let appGroupID = "group.com.aeustech.watchtipcalculator"
        let defaults = NSUserDefaults(suiteName: appGroupID)
        
        defaults!.setObject(value, forKey: key)
        
        defaults!.synchronize()
        
//        print("stored \(key) = \(value)")
        
    }
    
    
    func retrieveSharedInfo(key:String) -> String {
        
        let appGroupID = "group.com.aeustech.watchtipcalculator"
        let defaults = NSUserDefaults(suiteName: appGroupID)
        
        defaults!.synchronize()
        
        if let value = defaults!.objectForKey(key) as? String {
            
//            print("got \(key) = \(value)")

            return value
        } else {
            return ""
        }
        
    }
    
    func storeTipInfo() {
        
//        var bill_amount = Int(retrieveSharedInfo("bill_dollaramount"))
//        var bill_cents = Int(retrieveSharedInfo("bill_cents"))
//        bill_amount = bill_amount == nil ? 0 : bill_amount!
//        bill_cents = bill_cents == nil ? 0 : bill_cents!
        
//        var tip = Int(retrieveSharedInfo("tip"))
//        tip = tip == nil ? 15 : tip!
//        
//        var split = Int(retrieveSharedInfo("num_split"))
//        split = split == nil ? 0 : split! - 1
        
//        let bill = ("\(bill_amount).\(bill_cents)" as NSString).doubleValue
//        let tip_amount = bill * Double(tip!/100)
        
        let appGroupID = "group.com.aeustech.watchtipcalculator"
        let defaults = NSUserDefaults(suiteName: appGroupID)
        
        defaults!.setObject(bill, forKey: "bill_amount")
        defaults!.setObject(tipPercentage, forKey: "tip_percent")
        defaults!.setObject(tipAmount, forKey: "tip_amount")
        
        let pay = Float(bill)! + tipAmount
        defaults!.setObject(pay, forKey: "total_amount")
        
        defaults!.synchronize()
        
        log("stored latest tip info")
        
        //        let billAmt = defaults.stringForKey("bill_amount")
        //        log("stored bill amount = \(billAmt)")
    }
    
    
    func calculateAmounts() {
        amount = (bill as NSString).floatValue
        tipAmount = Float(tipPercentage)/100 * amount
        
//        print("amount=\(amount)")
//        print("tipAmount=\(tipAmount)")
        
//        let formatNumber = NSNumberFormatter()
//        let locale = NSLocale.currentLocale()
//        let separator = locale.objectForKey(NSLocaleDecimalSeparator) as! String
        
//        log("watch locale decimal separator = " + separator)
        
        
        formatNumber.numberStyle = NSNumberFormatterStyle.CurrencyStyle
        let formattedAmount = formatNumber.stringFromNumber(amount + tipAmount)!
        let formattedTipAmount = formatNumber.stringFromNumber(tipAmount)!

        let splitAmount = (amount + tipAmount) / Float(split + 1)
        let formattedSplitAmount = formatNumber.stringFromNumber(splitAmount)!
        
        lblSplit.setText(formattedSplitAmount)
        
        lblTip.setText("\(formattedTipAmount)")
        lblTotal.setText("\(formattedAmount)")
        
        storeTipInfo()
    }
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
        let locale: AnyObject = NSLocale.currentLocale()
        
        separator = locale.objectForKey(NSLocaleDecimalSeparator) as! String
        
        formatNumber.numberStyle = NSNumberFormatterStyle.CurrencyStyle
        
        //TEST ONLY
//        separator = ","
//        formatNumber.locale = NSLocale(localeIdentifier: "de_DE")
        

    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        
        var pickerItems: [WKPickerItem]! = []
        var pickerTipItems: [WKPickerItem]! = []
        var pickerSplitItems: [WKPickerItem]! = []

        
        for i in 0...999 {
            let pickerItem = WKPickerItem()
            pickerItem.title = "\(i)"
            pickerItem.caption = "\(i)"
            pickerItems.append(pickerItem)
        }
        
        for i in 0...99 {
            let pickerItem = WKPickerItem()
            
//            pickerItem.title = "\(i)"
//            pickerItem.caption = "\(i)"
            
            pickerItem.title = String(format: "\(separator)%02d", i)
            pickerItem.caption = String(format: "%02d", i)
            
            pickerCentItems.append(pickerItem)
        }
        
        for i in 0...50 {
            let pickerItem = WKPickerItem()
            pickerItem.title = "\(i)"
            pickerItem.caption = "\(i)"
            pickerTipItems.append(pickerItem)
        }
        
        for i in 1...30 {
            let pickerItem = WKPickerItem()
            pickerItem.title = "\(i)"
            pickerItem.caption = "\(i)"
            pickerSplitItems.append(pickerItem)
        }
        
        picker.setItems(pickerItems)
        picker.focus()
        
        pickerCents.setItems(pickerCentItems)
        pickerTipPercentage.setItems(pickerTipItems)
        pickerSplit.setItems(pickerSplitItems)
        
        let bill_amount = Int(retrieveSharedInfo("bill_dollaramount"))
        let bill_cents = Int(retrieveSharedInfo("bill_cents"))
        picker.setSelectedItemIndex(bill_amount == nil ? 0 : bill_amount!)
        pickerCents.setSelectedItemIndex(bill_cents == nil ? 0 : bill_cents!)

        let tip = Int(retrieveSharedInfo("tip"))
        pickerTipPercentage.setSelectedItemIndex(tip == nil ? 15 : tip!)
        
        let split = Int(retrieveSharedInfo("num_split"))
        pickerSplit.setSelectedItemIndex(split == nil ? 0 : split! - 1)
        
        
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
}
