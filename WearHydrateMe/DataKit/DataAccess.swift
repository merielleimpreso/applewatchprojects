//
//  DataAccess.swift
//  WearHydrateMe
//
//  Created by Merielle Impreso on 5/20/15.
//  Copyright (c) 2015 Merielle Impreso. All rights reserved.
//

import UIKit
import CoreData

public class DataAccess: NSObject {
    public var sharedAppGroup: NSString = "group.com.aeustech.wearhydrateme"
    public var unlock30days = "unlock30days"
    public var alarmOnString = "alarmOn"
    public var alarmHourString = "alarmHour"
    public var alarmMinuteString = "alarmMinute"
    public var activeFromHourString = "activeFromHour"
    public var activeFromMinuteString = "activeFromMinute"
    public var activeFromMiridiemString = "activeFromMiridiem"
    public var activeToHourString = "activeToHour"
    public var activeToMinuteString = "activeToMinute"
    public var activeToMiridiemString = "activeToMiridiem"
    
    
    public class var sharedInstance : DataAccess {
        struct Static {
            static var onceToken : dispatch_once_t = 0
            static var instance : DataAccess? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = DataAccess()
        }
        
        return Static.instance!
    }
    
    public func cancelNotification() {
        UIApplication.sharedApplication().cancelAllLocalNotifications()
    }
    
    public func sendNotification() {
        let activeFromHour = getActiveFromHour()
        let activeFromMinute = getActiveFromMinute()
        let activeFromMiridiem = getActiveFromMiridiem()
        
        let activeToHour = getActiveToHour()
        let activeToMinute = getActiveToMinute()
        let activeToMiridiem = getActiveToMiridiem()
        
        let alarmHour = getAlarmHour()
        let alarmMinute = getAlarmMinute()
        
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Year, .Month, .Day, .Hour, .Minute], fromDate: NSDate())
        let comps = NSDateComponents()
        comps.year = components.year
        comps.month = components.month
        comps.day = components.day
        
        comps.hour = activeFromHour
        comps.minute = activeFromMinute
        let date1 = calendar.dateFromComponents(comps)
        
        comps.hour = activeToHour
        comps.minute = activeToMinute
        var date2 = calendar.dateFromComponents(comps)
        
        var intervalOfDates = Int(date2!.timeIntervalSinceDate(date1!))
        if (intervalOfDates < 0) {
            let components = NSDateComponents()
            components.day = 1
            date2 = calendar.dateByAddingComponents(components, toDate: date2!, options: [])
        }
        intervalOfDates = Int(date2!.timeIntervalSinceDate(date1!))
        
        let intervalOfAlarm = (alarmHour * 3600) + (alarmMinute * 60)
        let alarmCount = intervalOfDates / intervalOfAlarm
        
        for i in 0...alarmCount {
            let secondsToAdd = i * intervalOfAlarm
            let components = NSDateComponents()
            components.second = secondsToAdd
            let date = calendar.dateByAddingComponents(components, toDate: date1!, options: [])
            

            let notification = UILocalNotification()
            notification.fireDate = date
            notification.alertBody = "Drink some water!"
            notification.timeZone = NSTimeZone.defaultTimeZone()
            notification.applicationIconBadgeNumber = 1

            notification.repeatInterval = .Day
            notification.category = "INVITE_CATEGORY"
            notification.repeatInterval = .Day

            UIApplication.sharedApplication().scheduleLocalNotification(notification)
            
            let formatter = NSDateFormatter()
            formatter.dateStyle = .MediumStyle
            formatter.timeStyle = .MediumStyle
            print("\(i) = \(formatter.stringFromDate(date!))")
        }
    }
    
    public func getAllRecords() -> [Record] {
        return Record.getAllRecords(DataAccess.sharedInstance.managedObjectContext!)
    }
    
    public func insertRecord() -> Record {
        return Record.insertRecord(DataAccess.sharedInstance.managedObjectContext!)
    }
    
    public func deleteRecord(record: Record) {
        return Record.deleteRecord(DataAccess.sharedInstance.managedObjectContext!, record: record)
    }
    
    public func getAllGoals() -> [Goal] {
        return Goal.getAllGoals(DataAccess.sharedInstance.managedObjectContext!)
    }
    
    public func insertGoal() -> Goal {
        return Goal.insertGoal(DataAccess.sharedInstance.managedObjectContext!)
    }
    
    public func updateGoalToday(goal: Goal) -> Goal {
        return Goal.updateGoalToday(DataAccess.sharedInstance.managedObjectContext!, goal: goal)
    }
    
    public func getUnlock30Days() -> Bool {
        let defaults = NSUserDefaults(suiteName: sharedAppGroup as String)
        let alarmOn = defaults?.boolForKey(unlock30days)
        
        if (alarmOn == nil) {
            return false
        } else {
            return alarmOn!
        }
    }
    
    public func updateUnlock30Days(unlocked: Bool) {
        let defaults = NSUserDefaults(suiteName: sharedAppGroup as String)
        defaults?.setBool(unlocked, forKey: unlock30days)
    }
    
    public func getAlarmOn() -> Bool {
        let defaults = NSUserDefaults(suiteName: sharedAppGroup as String)
        let alarmOn = defaults?.boolForKey(alarmOnString)
        
        if (alarmOn == nil) {
            return false
        } else {
            return alarmOn!
        }
    }
    
    public func updateAlarmOn(alarmOn: Bool) {
        let defaults = NSUserDefaults(suiteName: sharedAppGroup as String)
        defaults?.setBool(alarmOn, forKey: alarmOnString)
    }
    
    public func getAlarmHour() -> Int {
        let defaults = NSUserDefaults(suiteName: sharedAppGroup as String)
        let hour = defaults?.integerForKey(alarmHourString)
        
        if (hour == nil) {
            return 0
        } else {
            return hour!
        }
    }
    
    public func updateAlarmHour(hour: Int) {
        let defaults = NSUserDefaults(suiteName: sharedAppGroup as String)
        defaults?.setInteger(hour, forKey: alarmHourString)
    }
    
    public func getAlarmMinute() -> Int {
        let defaults = NSUserDefaults(suiteName: sharedAppGroup as String)
        let minute = defaults?.integerForKey(alarmMinuteString)
        
        if (minute == nil) {
            return 30
        } else {
            return minute!
        }
    }
    
    public func updateAlarmMinute(minute: Int) {
        let defaults = NSUserDefaults(suiteName: sharedAppGroup as String)
        defaults?.setInteger(minute, forKey: alarmMinuteString)
    }
    
    public func getActiveFromHour() -> Int {
        let defaults = NSUserDefaults(suiteName: sharedAppGroup as String)
        let hour = defaults?.integerForKey(activeFromHourString)
        
        if (hour == nil) {
            return 8
        } else {
            return hour!
        }
    }
    
    public func updateActiveFromHour(hour: Int) {
        let defaults = NSUserDefaults(suiteName: sharedAppGroup as String)
        defaults?.setInteger(hour, forKey: activeFromHourString)
    }
    
    public func getActiveFromMinute() -> Int {
        let defaults = NSUserDefaults(suiteName: sharedAppGroup as String)
        let minute = defaults?.integerForKey(activeFromMinuteString)
        
        if (minute == nil) {
            return 0
        } else {
            return minute!
        }
    }
    
    public func updateActiveFromMinute(hour: Int) {
        let defaults = NSUserDefaults(suiteName: sharedAppGroup as String)
        defaults?.setInteger(hour, forKey: activeFromMinuteString)
    }
    
    public func getActiveToHour() -> Int {
        let defaults = NSUserDefaults(suiteName: sharedAppGroup as String)
        let hour = defaults?.integerForKey(activeToHourString)
        
        if (hour == nil) {
            return 20
        } else {
            return hour!
        }
    }
    
    public func updateActiveToHour(hour: Int) {
        let defaults = NSUserDefaults(suiteName: sharedAppGroup as String)
        defaults?.setInteger(hour, forKey: activeToHourString)
    }
    
    public func getActiveToMinute() -> Int {
        let defaults = NSUserDefaults(suiteName: sharedAppGroup as String)
        let minute = defaults?.integerForKey(activeToMinuteString)
        
        if (minute == nil) {
            return 0
        } else {
            return minute!
        }
    }
    
    public func updateActiveToMinute(hour: Int) {
        let defaults = NSUserDefaults(suiteName: sharedAppGroup as String)
        defaults?.setInteger(hour, forKey: activeToMinuteString)
    }
    
    public func getActiveFromMiridiem() -> String {
        let defaults = NSUserDefaults(suiteName: sharedAppGroup as String)
        let miridiem = defaults?.stringForKey(activeFromMiridiemString)
        
        if (miridiem == nil) {
            return "AM"
        } else {
            return miridiem!
        }
    }
    
    public func updateActiveFromMiridiem(miridiem: String) {
        let defaults = NSUserDefaults(suiteName: sharedAppGroup as String)
        defaults?.setObject(miridiem, forKey: activeFromMiridiemString)
    }
    
    public func getActiveToMiridiem() -> String {
        let defaults = NSUserDefaults(suiteName: sharedAppGroup as String)
        let miridiem = defaults?.stringForKey(activeToMiridiemString)
        
        if (miridiem == nil) {
            return "PM"
        } else {
            return miridiem!
        }
    }
    
    public func updateActiveToMiridiem(miridiem: String) {
        let defaults = NSUserDefaults(suiteName: sharedAppGroup as String)
        defaults?.setObject(miridiem, forKey: activeToMiridiemString)
    }
    
    // MARK: - Core Data stack
    
    public lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.projectds.CoreDataWatch" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1] 
        }()
    
    public lazy var managedObjectModel: NSManagedObjectModel = {
        
        let proxyBundle = NSBundle(identifier: "com.aeustech.DataKit")
        let modelURL = proxyBundle?.URLForResource("DataModel", withExtension: "momd")!
        
        return NSManagedObjectModel(contentsOfURL: modelURL!)!
        }()
    
    public lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
        
        let containerPath = NSFileManager.defaultManager().containerURLForSecurityApplicationGroupIdentifier(self.sharedAppGroup as String)?.path
        let sqlitePath = NSString(format: "%@/%@", containerPath!, "DataModel")
        let url = NSURL(fileURLWithPath: sqlitePath as String);
        
        let  model = self.managedObjectModel;
        
        var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: model)
        var error: NSError? = nil
        
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator!.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch var error1 as NSError {
            error = error1
            coordinator = nil
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            dict[NSUnderlyingErrorKey] = error
            error = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            
            NSLog("Unresolved error \(error), \(error!.userInfo)")
            abort()
        } catch {
            fatalError()
        }
        
        return coordinator
        }()
    
    public lazy var managedObjectContext: NSManagedObjectContext? = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        if coordinator == nil {
            return nil
        }
        var managedObjectContext = NSManagedObjectContext()
        managedObjectContext.persistentStoreCoordinator = coordinator
        managedObjectContext.stalenessInterval = 1
        return managedObjectContext
        }()
    
    // MARK: - Core Data Saving support
    
    public func saveContext () {
        if let moc = self.managedObjectContext {
            var error: NSError? = nil
            if moc.hasChanges {
                do {
                    try moc.save()
                } catch let error1 as NSError {
                    error = error1
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    NSLog("Unresolved error \(error), \(error!.userInfo)")
                    abort()
                }
            }
        }
    }
}