//
//  WatchNotificationHourController.swift
//  InactivityCoach
//
//  Created by ATserver on 11/9/15.
//  Copyright © 2015 Merielle Impreso. All rights reserved.
//

import WatchKit
import Foundation


class WatchNotificationHourController: WKInterfaceController {
    @IBOutlet var hourButton: WKInterfaceButton!
    @IBOutlet var hourSlider: WKInterfaceSlider!
    
    var parentController: WatchNotificationController? = nil
    var hour: Int = 0
        
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        parentController = (context as! WatchNotificationController)
        hour = parentController!.hour
        hourButton.setTitle(Formatter.zeroPad(hour))
        hourSlider.setValue(Float(hour))
    }
    
    @IBAction func hourSliderValueChanged(value: Float) {
        hour = Int(value)
        hourButton.setTitle(Formatter.zeroPad(hour))
        hourSlider.setValue(value)
    }

    @IBAction func okButtonClicked() {
        parentController!.hour = hour
        WatchInactivityCoach.sharedInstance.setSharedInfo(kIntervalHour, value: hour)
        self.dismissController()
    }
}
