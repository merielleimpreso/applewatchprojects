//
//  TCPageContentController.swift
//  WatchTipCalculator
//
//  Created by dev on 4/19/15.
//  Copyright (c) 2015 Aeus. All rights reserved.
//

import UIKit

class TCPageContentController : UIViewController {
    @IBOutlet var contentImageView : UIImageView?
    
    var mainController:ViewController = ViewController()
    
    var pageIndex: Int = 0
    var imageName: String = "" {
        didSet {
            if let imageView = contentImageView {
                imageView.image = UIImage(named: imageName)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contentImageView!.image = UIImage(named: imageName)
    }
    
}
