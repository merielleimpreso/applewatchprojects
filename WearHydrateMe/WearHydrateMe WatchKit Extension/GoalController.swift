//
//  GoalController.swift
//  WearHydrateMe
//
//  Created by Merielle Impreso on 5/21/15.
//  Copyright (c) 2015 Merielle Impreso. All rights reserved.
//

import WatchKit
import Foundation
import DataKit


class GoalController: WKInterfaceController {
    @IBOutlet weak var goalLabel: WKInterfaceLabel!
    //@IBOutlet weak var goalSlider: WKInterfaceSlider!
    
    var allGoals: [Goal] = []
    var goalToday: Goal?
    
    let recommendedGoal = 8
    
    @IBAction func subtractButtonClicked(sender: AnyObject) {
        var number = goalToday!.number as Int
        number -= 1
        
        var g = self.goalToday
        g!.number = number
        g!.date = NSDate()
        
        goalLabel.setText("\(g!.number)")
        goalToday = DataAccess.sharedInstance.updateGoalToday(g!)
    }
    
    @IBAction func addButtonClicked(sender: AnyObject) {
        var number = goalToday!.number as Int
        
        if (number == recommendedGoal) {
            presentControllerWithName("GoalWarning",
                context: ["segue": "hierarchical",
                "data":"Passed through hierarchical navigation"])
        } else {
            number += 1
            
            var g = self.goalToday
            g!.number = number
            g!.date = NSDate()

            goalLabel.setText("\(g!.number)")
            goalToday = DataAccess.sharedInstance.updateGoalToday(g!)
        }
    }
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
    }
    
    func getGoalToday() -> Goal? {
        var goalToday: Goal? = nil
        allGoals = DataAccess.sharedInstance.getAllGoals()
        
        let formatter = NSDateFormatter()
        formatter.dateStyle = .ShortStyle
        let dateToday = formatter.stringFromDate(NSDate())
        for goal in allGoals {
            let date = formatter.stringFromDate(goal.date)
            if (date == dateToday) {
                goalToday = goal
                break
            }
        }
        
        if (goalToday == nil) {
            goalToday = DataAccess.sharedInstance.insertGoal()
        }
        
        return goalToday
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        goalToday = getGoalToday()
        goalLabel.setText("\(goalToday!.number)")
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
