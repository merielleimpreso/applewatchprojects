//
//  WatchNotificationMinuteController.swift
//  InactivityCoach
//
//  Created by ATserver on 11/9/15.
//  Copyright © 2015 Merielle Impreso. All rights reserved.
//

import WatchKit
import Foundation


class WatchNotificationMinuteController: WKInterfaceController {
    @IBOutlet var minuteButton: WKInterfaceButton!
    @IBOutlet var minuteSlider: WKInterfaceSlider!
    
    var parentController: WatchNotificationController? = nil
    var minute: Int = 0
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        parentController = (context as! WatchNotificationController)
        minute = parentController!.minute
        minuteButton.setTitle(Formatter.zeroPad(minute))
        minuteSlider.setValue(Float(minute))
    }
    
    @IBAction func sliderValueChanged(value: Float) {
        minute = Int(value)
        minuteButton.setTitle(Formatter.zeroPad(minute))
        minuteSlider.setValue(value)
    }
    
    @IBAction func okButtonClicked() {
        parentController!.minute = minute
        WatchInactivityCoach.sharedInstance.setSharedInfo(kIntervalMinute, value: minute)
        self.dismissController()
    }
    
}
