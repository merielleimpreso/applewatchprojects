//
//  TCBillPageContent.swift
//  WatchTipCalculator
//
//  Created by dev on 4/20/15.
//  Copyright (c) 2015 Aeus. All rights reserved.
//

import UIKit

class TCBillPageContentController : TCPageContentController {
    @IBOutlet weak var btn_bill_amt: UIButton!
    
    
    @IBAction func buttonAction(sender: AnyObject) {
        mainController.childMessage("jump_to_tip")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
}